package main

import (
	"bufio"
	"bytes"
	"container/list"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

type grid [][]byte

type vec struct {
	x int
	y int
}

var dirs = [4]vec{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func (v vec) add(o vec) vec {
	return vec{v.x + o.x, v.y + o.y}
}

func (g grid) valid(p vec) bool {
	return 0 <= p.y && p.y < len(g) && 0 <= p.x && p.x < len(g[p.y])
}

func (g grid) neighbors(p vec, fn func(vec)) {
	for _, dir := range dirs {
		if n := p.add(dir); g.valid(n) && g[n.y][n.x] != '#' {
			fn(n)
		}
	}
}

func (g grid) fill(start vec, steps int) int {
	type item struct {
		pos   vec
		steps int
	}
	visited := map[vec]struct{}{}
	result := map[vec]struct{}{}

	queue := list.New()
	queue.PushBack(item{start, steps})
	visited[start] = struct{}{}

	for queue.Len() > 0 {
		front := queue.Front()
		it := front.Value.(item)
		queue.Remove(front)
		if it.steps%2 == 0 {
			result[it.pos] = struct{}{}
		}
		if it.steps == 0 {
			continue
		}
		g.neighbors(it.pos, func(v vec) {
			if _, found := visited[v]; found {
				return
			}
			visited[v] = struct{}{}
			queue.PushBack(item{v, it.steps - 1})
		})
	}

	return len(result)
}

func part1(g grid, start vec, steps int) {
	fmt.Printf("after %d steps reachable: %d\n", steps, g.fill(start, steps))
}

func part2(g grid, start vec, steps int) {
	if len(g) != len(g[0]) {
		log.Fatalln("Grid needs to be squared.")
	}
	size := len(g)
	if start.x != size/2 || start.y != size/2 {
		log.Fatalln("Starting point is not in the center of the grid.")
	}
	if steps%size != size/2 {
		log.Fatalln("steps dont reach exactly the border.")
	}

	gridWidth := steps/size - 1

	odd := gridWidth/2*2 + 1
	odd *= odd
	even := (gridWidth + 1) / 2 * 2
	even *= even

	var (
		oddPoints  = g.fill(start, size*2+1)
		evenPoints = g.fill(start, size*2)

		cornerT = g.fill(vec{size - 1, start.y}, size-1)
		cornerR = g.fill(vec{start.y, 0}, size-1)
		cornerB = g.fill(vec{0, start.y}, size-1)
		cornerL = g.fill(vec{start.x, size - 1}, size-1)

		smallTr = g.fill(vec{size - 1, 0}, size/2-1)
		smallTl = g.fill(vec{size - 1, size - 1}, size/2-1)
		smallBr = g.fill(vec{0, 0}, size/2-1)
		smallBl = g.fill(vec{0, size - 1}, size/2-1)

		largeTr = g.fill(vec{size - 1, 0}, size*3/2-1)
		largeTl = g.fill(vec{size - 1, size - 1}, size*3/2-1)
		largeBr = g.fill(vec{0, 0}, size*3/2-1)
		largeBl = g.fill(vec{0, size - 1}, size*3/2-1)
	)

	total := odd*oddPoints +
		even*evenPoints +
		cornerT + cornerR + cornerB + cornerL +
		(gridWidth+1)*(smallTr+smallTl+smallBr+smallBl) +
		gridWidth*(largeTr+largeTl+largeBr+largeBl)

	fmt.Printf("after %d steps reachable: %d\n", steps, total)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

var gardenRe = regexp.MustCompile(`^[.#S]+$`)

func main() {

	steps := flag.Int("steps", 0, "number of steps")
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	if *steps <= 0 {
		if *second {
			*steps = 26501365
		} else {
			*steps = 64
		}
	}

	var g grid

	start := vec{-1, -1}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		if !gardenRe.Match(line) {
			log.Printf("umatched line %d: %q\n", lineNo, line)
			continue
		}
		if len(g) > 0 && len(line) != len(g[0]) {
			log.Printf("line %d: length differ.\n", lineNo)
			continue
		}
		g = append(g, bytes.Clone(line))
		if idx := bytes.IndexByte(line, 'S'); idx != -1 {
			start = vec{idx, lineNo - 1}
		}
	}
	check(sc.Err())
	if start.x == -1 {
		log.Fatalln("No starting point found.")
	}

	if *second {
		part2(g, start, *steps)
	} else {
		part1(g, start, *steps)
	}
}
