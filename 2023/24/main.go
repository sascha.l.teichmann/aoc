package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"time"
)

type vec [3]float64

type line struct {
	p vec
	d vec
}

type lines []*line

func (v vec) insideXY(low, high float64) bool {
	return low <= v[0] && v[0] <= high &&
		low <= v[1] && v[1] <= high
}

func (r *line) intersectsXY(s *line) (vec, bool) {
	var (
		x1, y1   = r.p[0], r.p[1]
		dx1, dy1 = r.d[0], r.d[1]
		x2, y2   = s.p[0], s.p[1]
		dx2, dy2 = s.d[0], s.d[1]
	)

	denominator := dx1*dy2 - dy1*dx2
	if denominator == 0 {
		return vec{}, false
	}

	nominatorT := (x2-x1)*dy2 - (y2-y1)*dx2
	nominatorU := (x2-x1)*dy1 - (y2-y1)*dx1

	t := nominatorT / denominator
	u := nominatorU / denominator

	if t < 0 || u < 0 {
		return vec{}, false
	}

	return vec{x1 + t*dx1, y1 + t*dy1}, true
}

func (v vec) round() vec {
	return vec{
		math.Round(v[0]),
		math.Round(v[1]),
		math.Round(v[2]),
	}
}

func gradients(x, y, dx, dy, lx, ly, ldx, ldy float64) (diff, ux, uy, udx, udy float64) {
	diff2 := (lx-x)*(ldy-dy) - (ly-y)*(ldx-dx)
	diff = diff2 * diff2
	ux = -diff2 * (ldy - dy)
	uy = diff2 * (ldx - dx)
	udx = diff2 * (ly - y)
	udy = -diff2 * (lx - x)
	return
}

func (rs lines) approximate(
	iterations int, earlyStop, gradient float64,
	debug bool,
) line {

	approx := line{}

	for i, lastLoss := 0, 0.0; i < iterations; i++ {
		iterLoss := 0.0
		for _, r := range rs {
			for _, pair := range [][2]int{{0, 1}, {1, 2}, {2, 0}} {
				i1, i2 := pair[0], pair[1]

				var (
					x, y     = approx.p[i1], approx.p[i2]
					dx, dy   = approx.d[i1], approx.d[i2]
					lx, ly   = r.p[i1], r.p[i2]
					ldx, ldy = r.d[i1], r.d[i2]
				)

				loss, ux, uy, udx, udy := gradients(
					x, y, dx, dy, lx, ly, ldx, ldy,
				)
				approx.p[i1] -= gradient * ux
				approx.p[i2] -= gradient * uy
				approx.d[i1] -= gradient * udx
				approx.d[i2] -= gradient * udy

				iterLoss += loss
			}
		}
		if math.Abs(lastLoss-iterLoss) < earlyStop {
			if debug {
				log.Printf("descent stopped after %d iterations.\n", i)
			}
			return approx
		}
		lastLoss = iterLoss
	}
	if debug {
		log.Printf("descent run %d iterations\n", iterations)
	}

	return approx
}

func (rs lines) exact(dir vec, upScale float64) vec {
	l1, dl1 := rs[0].p, rs[0].d
	l2, dl2 := rs[1].p, rs[1].d

	xyz := vec{}
	counts := [3]int{}
	for _, pair := range [][2]int{{0, 1}, {1, 2}, {2, 0}} {
		i1, i2 := pair[0], pair[1]
		a := line{
			vec{l1[i1] * upScale, l1[i2] * upScale},
			vec{-dir[i1] + dl1[i1], -dir[i2] + dl1[i2]},
		}
		b := line{
			vec{l2[i1] * upScale, l2[i2] * upScale},
			vec{-dir[i1] + dl2[i1], -dir[i2] + dl2[i2]},
		}
		if inter, ok := a.intersectsXY(&b); ok {
			counts[i1]++
			counts[i2]++
			xyz[i1] += inter[0]
			xyz[i2] += inter[1]
		}
	}
	for i, c := range counts {
		if c > 1 {
			xyz[i] /= float64(c)
		}
	}
	return xyz.round()
}

func parse(r io.Reader, fn func(vx, vy, vz, dx, dy, dz float64)) error {
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		var vx, vy, vz, dx, dy, dz float64
		if _, err := fmt.Sscanf(line, "%f, %f, %f @ %f, %f, %f",
			&vx, &vy, &vz,
			&dx, &dy, &dz,
		); err != nil {
			return fmt.Errorf(
				"canot parse line %d: %q %w", lineNo, line, err)
		}
		fn(vx, vy, vz, dx, dy, dz)
	}
	return sc.Err()
}

func part1(r io.Reader, _ bool) error {
	var hailstones lines
	if err := parse(r, func(vx, vy, _, dx, dy, _ float64) {
		hailstones = append(hailstones, &line{
			p: vec{vx, vy},
			d: vec{dx, dy},
		})
	}); err != nil {
		return err
	}

	const (
		low  = 200000000000000
		high = 400000000000000
	)

	intersections := 0
	for i, hs1 := range hailstones {
		for _, hs2 := range hailstones[i+1:] {
			if in, ok := hs1.intersectsXY(hs2); ok && in.insideXY(low, high) {
				intersections++
			}
		}
	}
	fmt.Printf("number of intersections: %d\n", intersections)
	return nil
}

func part2(r io.Reader, debug bool) error {
	const (
		scale      = 1e13
		gradient   = 1e-6
		earlyStop  = 1e-4
		iterations = 1500
	)
	var hailstones lines
	if err := parse(r, func(vx, vy, vz, dx, dy, dz float64) {
		const downScale = 1 / scale
		hailstones = append(hailstones, &line{
			p: vec{downScale * vx, downScale * vy, downScale * vz},
			d: vec{dx, dy, dz},
		})
	}); err != nil {
		return err
	}
	approximation := hailstones.approximate(
		iterations, earlyStop, gradient, debug)
	dir := approximation.d.round()
	exact := hailstones.exact(dir, scale)
	x, y, z := int64(exact[0]), int64(exact[1]), int64(exact[2])
	if debug {
		log.Printf("apprimation: %v\n", approximation)
		log.Printf("pos: [%d, %d, %d]\n", x, y, z)
		log.Printf("dir: %v\n", dir)
	}
	fmt.Printf("sum of x, y, z: %d\n", x+y+z)
	return nil
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()
	part := part1
	if *second {
		part = part2
	}
	start := time.Now()
	if err := part(os.Stdin, *debug); err != nil {
		log.Fatalln(err)
	}
	if *debug {
		log.Printf("calculation took %.4f seconds\n",
			time.Since(start).Seconds())
	}
}
