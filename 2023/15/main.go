package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"slices"
)

type slot struct {
	label []byte
	focal byte
}

type boxes [256][]slot

func (bs *boxes) add(box int, label []byte, focal byte) {
	slots := bs[box]
	idx := slices.IndexFunc(slots, func(s slot) bool {
		return bytes.Equal(s.label, label)
	})
	if idx != -1 {
		slots[idx].focal = focal
	} else {
		bs[box] = append(slots, slot{
			label: bytes.Clone(label),
			focal: focal,
		})
	}
}

func (bs *boxes) del(box int, label []byte) {
	slots := bs[box]
	idx := slices.IndexFunc(slots, func(s slot) bool {
		return bytes.Equal(s.label, label)
	})
	if idx != -1 {
		nbox := slices.Delete(slots, idx, idx+1)
		slots[len(slots)-1] = slot{}
		bs[box] = nbox
	}
}

func (bs *boxes) power() int {
	sum := 0
	for i, box := range bs {
		for s, sl := range box {
			sum += (i + 1) * (s + 1) * int(sl.focal)
		}
	}
	return sum
}

func hash(p []byte) int {
	var current int
	for _, v := range p {
		current += int(v)
		current *= 17
		current %= 256
	}
	return current
}

func steps(r io.Reader, fn func([]byte)) error {
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Bytes()
		for _, step := range bytes.Split(line, []byte{','}) {
			fn(step)
		}
	}
	return sc.Err()
}

func part1(r io.Reader) {
	sum := 0
	check(steps(r, func(step []byte) { sum += hash(step) }))
	fmt.Printf("sum: %d\n", sum)
}

var stepRe = regexp.MustCompile(`^([a-z]+)([-=])([0-9]?)$`)

func part2(r io.Reader) {
	var bs boxes
	check(steps(r, func(step []byte) {
		m := stepRe.FindSubmatch(step)
		if m == nil {
			log.Printf("unmatched operation %q\n", step)
			return
		}
		box := hash(m[1])
		switch m[2][0] {
		case '-':
			bs.del(box, m[1])
		case '=':
			bs.add(box, m[1], m[3][0]-'0')
		}
	}))
	fmt.Printf("total power: %d\n", bs.power())
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	if *second {
		part2(os.Stdin)
	} else {
		part1(os.Stdin)
	}
}
