package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

func asInts(line string) ([]int, error) {
	var slice []int
	for _, s := range strings.Fields(line) {
		v, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}
		slice = append(slice, v)
	}
	return slice, nil
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {
	second := flag.Bool("second", false, "second task")
	flag.Parse()

	trim := func(line, prefix string) string {
		line = line[len(prefix):]
		if *second {
			return strings.Map(func(r rune) rune {
				if unicode.IsSpace(r) {
					return -1
				}
				return r
			}, line)
		}
		return line
	}

	var times, distances []int
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		var err error
		line := sc.Text()
		switch {
		case strings.HasPrefix(line, "Time:"):
			times, err = asInts(trim(line, "Time:"))
			check(err)
		case strings.HasPrefix(line, "Distance:"):
			distances, err = asInts(trim(line, "Distance:"))
			check(err)
		}
	}
	check(sc.Err())
	if len(times) != len(distances) {
		log.Fatalln("length times does not match length of distances.")
	}
	winProd := 1
	for race, distance := range distances {
		wins := 0
		for t, time := 0, times[race]; t <= time; t++ {
			if dist := (time - t) * t; dist > distance {
				wins++
			}
		}
		winProd *= wins
	}
	fmt.Printf("win prod: %d\n", winProd)
}
