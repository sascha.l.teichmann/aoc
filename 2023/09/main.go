package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func asInts(line string) ([]int, error) {
	var ints []int
	for _, field := range strings.Fields(line) {
		i, err := strconv.Atoi(field)
		if err != nil {
			return nil, err
		}
		ints = append(ints, i)
	}
	return ints, nil
}

type data []int

func (d data) diff() data {
	if len(d) == 0 {
		return nil
	}
	result := make(data, 0, len(d)-1)
	for p := d; len(p) >= 2; p = p[1:] {
		result = append(result, p[1]-p[0])
	}
	return result
}

func (d data) extrapolateRight(x int) int {
	if len(d) == 0 {
		return x
	}
	return d[len(d)-1] + x
}

func (d data) extrapolateLeft(x int) int {
	if len(d) == 0 {
		return x
	}
	return d[0] - x
}

func (d data) zero() bool {
	for _, v := range d {
		if v != 0 {
			return false
		}
	}
	return true
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	sc := bufio.NewScanner(os.Stdin)
	var datas []data
	for sc.Scan() {
		row, err := asInts(sc.Text())
		check(err)
		datas = append(datas, data(row))
	}
	check(sc.Err())

	extrapolate := data.extrapolateRight
	if *second {
		extrapolate = data.extrapolateLeft
	}

	sum := 0
	for _, d := range datas {
		var stack []data
		for !d.zero() {
			stack = append(stack, d)
			d = d.diff()
		}
		a := 0
		for i := len(stack) - 1; i >= 0; i-- {
			a = extrapolate(stack[i], a)
		}
		sum += a
	}
	fmt.Println(sum)
}
