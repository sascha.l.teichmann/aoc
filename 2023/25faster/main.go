package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"slices"
	"strings"
	"time"
)

type edge [2]int

type node struct {
	edges []*edge
	num   int
}

func (n *node) add(e *edge) {
	n.edges = append(n.edges, e)
}

func (e *edge) replace(c, d int) {
	for i, v := range e {
		if v == c {
			e[i] = d
		}
	}
}

func (n *node) removeSelfRefs() {
	for i := 0; i < len(n.edges); {
		if n.edges[i][0] == n.edges[i][1] {
			nedges := slices.Delete(n.edges, i, i+1)
			n.edges[len(n.edges)-1] = nil
			n.edges = nedges
		} else {
			i++
		}
	}
}

func karger(edges []edge, target int) int {

	var (
		nedges = make([]edge, len(edges))
		stack  = make([]*edge, len(edges))
		nnodes = make([]node, 0, len(edges)/2+1)
		nodes  = make(map[int]*node, len(nnodes))
		rnd    = rand.New(rand.NewSource(time.Now().Unix()))
		nindex = func(idx int) *node {
			if n := nodes[idx]; n != nil {
				return n
			}
			nnodes = append(nnodes, node{num: 1})
			n := &nnodes[len(nnodes)-1]
			nodes[idx] = n
			return n
		}
	)

	for {
		clear(nodes)
		nnodes = nnodes[:0]
		copy(nedges, edges)
		stack = stack[:len(edges)]

		for i := range nedges {
			e := &nedges[i]
			na := nindex(e[0])
			nb := nindex(e[1])
			na.add(e)
			nb.add(e)
			stack[i] = e
		}

		rnd.Shuffle(len(stack), func(i, j int) {
			stack[i], stack[j] = stack[j], stack[i]
		})

		for len(nodes) > 2 {
			e := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			a, b := e[0], e[1]
			if a == b {
				continue
			}
			na, nb := nodes[a], nodes[b]

			for _, be := range nb.edges {
				be.replace(b, a)
				na.add(be)
			}
			na.removeSelfRefs()
			na.num += nb.num
			delete(nodes, b)
		}

		if len(first(nodes).edges) <= target {
			prod := 1
			for _, n := range nodes {
				prod *= n.num
			}
			return prod
		}
	}
}

func first[M ~map[K]V, K comparable, V any](m M) V {
	for _, v := range m {
		return v
	}
	var zero V
	return zero
}

func keys[M ~map[K]V, K comparable, V any](m M) []K {
	keys := make([]K, 0, len(m))
	for key := range m {
		keys = append(keys, key)
	}
	return keys
}

func main() {
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	indices := map[string]int{}
	index := func(s string) int {
		if idx, ok := indices[s]; ok {
			return idx
		}
		idx := len(indices)
		indices[s] = idx
		return idx
	}
	edges := map[edge]struct{}{}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		name, succs, ok := strings.Cut(line, ":")
		if !ok {
			log.Printf("line %d: %q has no nodes.\n", lineNo, line)
			continue
		}
		i1 := index(strings.TrimSpace(name))
		for _, succ := range strings.Fields(succs) {
			i2 := index(succ)
			e := edge{min(i1, i2), max(i1, i2)}
			edges[e] = struct{}{}
		}
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *debug {
		log.Printf("num edges: %d\n", len(edges))
	}
	fmt.Printf("product of group sizes: %d\n", karger(keys(edges), 3))
}
