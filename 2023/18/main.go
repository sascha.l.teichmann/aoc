package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type instruction struct {
	dir    direction
	length int
}

type instructions []instruction

type direction int

const (
	right direction = iota
	down
	left
	up
)

var dirs = [4][2]int{
	right: {1, 0},
	down:  {0, 1},
	left:  {-1, 0},
	up:    {0, -1},
}

func parseDirection(s string) (direction, error) {
	switch s {
	case "U":
		return up, nil
	case "D":
		return down, nil
	case "L":
		return left, nil
	case "R":
		return right, nil
	default:
		return -1, fmt.Errorf("unknown direction %q", s)
	}
}

func (instrs instructions) area() int {
	p, a := 0, float64(1)
	for _, instr := range instrs {
		dir := dirs[instr.dir]
		p += dir[0] * instr.length
		a += float64(dir[1]*instr.length*p) + float64(instr.length)/2
	}
	return int(a)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

var digRe = regexp.MustCompile(`^([UDLR]) (\d+) \(#([[:xdigit:]]{6})\)$`)

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var instrs instructions

	parser := func(m []string) instruction {
		dir, err := parseDirection(m[1])
		check(err)
		length, err := strconv.Atoi(m[2])
		check(err)
		return instruction{dir: dir, length: length}
	}

	if *second {
		replace := strings.NewReplacer(
			"0", "R",
			"1", "D",
			"2", "L",
			"3", "U",
		).Replace
		parser = func(m []string) instruction {
			length64, err := strconv.ParseUint(m[3][:5], 16, 64)
			check(err)
			length := int(length64)
			dir, err := parseDirection(replace(m[3][5:]))
			check(err)
			return instruction{dir: dir, length: length}
		}
	}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		m := digRe.FindStringSubmatch(line)
		if m == nil {
			log.Printf("unmatched line %d: %q\n", lineNo, line)
			continue
		}
		instrs = append(instrs, parser(m))
	}
	check(sc.Err())

	fmt.Println(instrs.area())
}
