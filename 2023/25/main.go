package main

import (
	"bufio"
	"cmp"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"runtime"
	"slices"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"net/http"
	_ "net/http/pprof"
)

type edge struct {
	nodes       [2]int
	deactivated bool
}

type node []int

type vis []uint64

type leakyBuffer chan []int

func select3(n int, fn func(int, int, int) bool) bool {
	for i := 0; i < n-2; i++ {
		for j := i + 1; j < n-1; j++ {
			for k := j + 1; k < n; k++ {
				if fn(i, j, k) {
					return true
				}
			}
		}
	}
	return false
}

func (e *edge) other(n int) int {
	switch {
	case e.nodes[0] == n:
		return e.nodes[1]
	case e.nodes[1] == n:
		return e.nodes[0]
	default:
		panic(fmt.Sprintf("should not happen: %d %d got %d",
			e.nodes[0], e.nodes[1], n))
	}
}

func (v vis) visited(i int) bool {
	return v[i/64]&(1<<uint64(i%64)) != 0
}

func (v vis) visit(i int) {
	v[i/64] |= (1 << uint64(i%64))
}

func (lb leakyBuffer) alloc() []int {
	select {
	case buf := <-lb:
		return buf
	default:
		return make([]int, 3*10*1024)
	}
}

func (lb leakyBuffer) free(buf []int) {
	select {
	case lb <- buf:
	default:
	}
}

func cut(links [][2]string, workers int, debug bool) int {

	edges := make([]edge, 0, len(links))
	var nodes []node
	nameToNode := map[string]int{}

	nodeIndex := func(name string) int {
		if idx, ok := nameToNode[name]; ok {
			return idx
		}
		idx := len(nodes)
		nodes = append(nodes, nil)
		nameToNode[name] = idx
		return idx
	}

	for _, link := range links {
		a := nodeIndex(link[0])
		b := nodeIndex(link[1])
		nodes[a] = append(nodes[a], len(edges))
		nodes[b] = append(nodes[b], len(edges))
		e := edge{nodes: [2]int{a, b}}
		edges = append(edges, e)
	}

	leaky := make(leakyBuffer, workers)

	var wg sync.WaitGroup

	var result atomic.Int64
	result.Store(-1)

	deactivateCh := make(chan []int)

	worker := func() {
		defer wg.Done()
		wedges := make([]edge, len(edges))
		copy(wedges, edges)

		v := make(vis, len(nodes)/64+1)

		var stack []int
		var sizes []int

		for batch := range deactivateCh {
			for deact := batch; len(deact) >= 3; deact = deact[3:] {
				for _, e := range deact[:3] {
					wedges[e].deactivated = true
				}
				clear(v)

				sizes = sizes[:0]
				for i := range nodes {
					if v.visited(i) {
						continue
					}
					v.visit(i)
					stack = append(stack, i)
					count := 1
					for len(stack) > 0 {
						cur := stack[len(stack)-1]
						stack = stack[:len(stack)-1]
						for _, e := range nodes[cur] {
							if wedges[e].deactivated {
								continue
							}
							if o := wedges[e].other(cur); !v.visited(o) {
								v.visit(o)
								count++
								stack = append(stack, o)
							}
						}
					}
					sizes = append(sizes, count)
				}
				if len(sizes) == 2 {
					prod := 1
					for i, size := range sizes {
						if debug {
							log.Printf("%d: size: %d\n", i+1, size)
						}
						prod *= size
					}
					result.Store(int64(prod))
				}

				for _, e := range deact[:3] {
					wedges[e].deactivated = false
				}
			}
			leaky.free(batch)
		}
	}

	for i := 0; i < workers; i++ {
		wg.Add(1)
		go worker()
	}

	batch := leaky.alloc()
	bpos := 0

	last := -1

	found := select3(len(edges), func(e1, e2, e3 int) bool {
		if result.Load() != -1 {
			return true
		}
		if debug && e1 != last {
			last = e1
			log.Printf("first changed: %d\n", last)
		}
		batch[bpos+0] = e1
		batch[bpos+1] = e2
		batch[bpos+2] = e3
		if bpos += 3; bpos >= len(batch) {
			deactivateCh <- batch
			batch = leaky.alloc()
			bpos = 0
		}
		return false
	})

	if !found {
		deactivateCh <- batch[:bpos]
	}

	close(deactivateCh)
	wg.Wait()
	return int(result.Load())
}

func part1(links [][2]string, workers int, debug bool) {
	if debug {
		log.Printf("number of links: %d\n", len(links))
	}
	start := time.Now()
	result := cut(links, workers, debug)
	if debug {
		log.Printf("calculation took: %s\n", time.Since(start))
	}
	if result != -1 {
		fmt.Printf("product of group sizes: %d\n", result)
	}
}

var lineRe = regexp.MustCompile(`^([a-z]+): (.+)$`)

func main() {
	workers := flag.Int("workers", runtime.NumCPU(), "number of workers")
	profile := flag.Bool("profile", false, "start profiling web server")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	if *profile {
		go func() {
			log.Fatal(http.ListenAndServe(":8080", nil))
		}()
	}

	var links [][2]string

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		m := lineRe.FindStringSubmatch(line)
		if m == nil {
			log.Printf("unmatched line %d: %q\n", lineNo, line)
			continue
		}
		compA := m[1]
		for _, compB := range strings.Fields(m[2]) {
			link := [2]string{
				min(compA, compB),
				max(compA, compB),
			}
			links = append(links, link)
		}
	}
	if err := sc.Err(); err != nil {
		log.Fatalln(err)
	}
	slices.SortFunc(links, func(a, b [2]string) int {
		for i := range a {
			if as, bs := a[i], b[i]; as != bs {
				return cmp.Compare(as, bs)
			}
		}
		return 0
	})
	links = slices.Compact(links)
	part1(links, *workers, *debug)
}
