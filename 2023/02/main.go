package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type draw struct {
	red   int
	green int
	blue  int
}

type game struct {
	id    int
	hints []draw
}

var gameRe = regexp.MustCompile(`^Game ([0-9]+): (.+)$`)

func (g *game) possible(red, green, blue int) bool {
	for _, h := range g.hints {
		if h.red > red || h.blue > blue || h.green > green {
			return false
		}
	}
	return true
}

func (g *game) fewest() int {
	var m draw
	for _, h := range g.hints {
		m.red = max(m.red, h.red)
		m.green = max(m.green, h.green)
		m.blue = max(m.blue, h.blue)
	}
	return m.red * m.green * m.blue
}

func part1(games []*game, red, green, blue int) {
	sum := 0
	for _, g := range games {
		if g.possible(red, green, blue) {
			sum += g.id
		}
	}
	fmt.Printf("number games: %d\n", sum)
}

func part2(games []*game) {
	power := 0
	for _, g := range games {
		power += g.fewest()
	}
	fmt.Printf("power: %d\n", power)
}

func main() {

	var (
		red    = flag.Int("red", 12, "red cubes")
		green  = flag.Int("green", 13, "green cubes")
		blue   = flag.Int("blue", 14, "blue cubes")
		second = flag.Bool("second", false, "second puzzle")
	)
	flag.Parse()

	var games []*game
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		m := gameRe.FindStringSubmatch(line)
		if len(m) == 0 {
			continue
		}
		id, _ := strconv.Atoi(m[1])
		//log.Printf("Game %d:\n", id)
		var hints []draw
		for _, gs := range strings.Split(m[2], ";") {
			gs = strings.TrimSpace(gs)
			//log.Printf("\t%s\n", gs)
			var g draw
			for _, col := range strings.Split(gs, ",") {
				col = strings.TrimSpace(col)
				ns, cs, ok := strings.Cut(col, " ")
				if !ok {
					continue
				}
				n, err := strconv.Atoi(ns)
				if err != nil {
					log.Printf("invalid integer %q\n", ns)
					continue
				}
				switch cs {
				case "red":
					g.red += n
				case "green":
					g.green += n
				case "blue":
					g.blue += n
				default:
					log.Printf("unknown color %q\n", cs)
				}
			}
			hints = append(hints, g)
		}
		games = append(games, &game{id: id, hints: hints})
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if *second {
		part2(games)
	} else {
		part1(games, *red, *green, *blue)
	}
}
