package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type part [4]int

type sorter map[string]*rule

type ruleType int

const (
	accept ruleType = iota
	reject
	descent
	greater
	lesser
)

type rule struct {
	typ         ruleType
	name        string
	idx         int
	value       int
	left, right *rule
}

type ranges [4][2]int

type limiter func(ranges) (ranges, bool)

var (
	workflowRe = regexp.MustCompile(
		`^([a-z]+)\{([^\}]+)}$`)
	partsRe = regexp.MustCompile(
		`^{x=(\d+),m=(\d+),a=(\d+),s=(\d+)}$`)
	ruleRe = regexp.MustCompile(
		`^(?:([xmas])([<>])(\d+):([a-zAR]+))|([a-zAR]+)$`)
)

var indices = map[string]int{
	"x": 0,
	"m": 1,
	"a": 2,
	"s": 3,
}

var full = ranges{
	{1, 4000},
	{1, 4000},
	{1, 4000},
	{1, 4000},
}

func (p part) rating() int {
	return p[0] + p[1] + p[2] + p[3]
}

func (s sorter) accept(r *rule, p part) (bool, error) {

down:
	switch r.typ {
	case accept:
		return true, nil
	case reject:
		// Do nothing
	case descent:
		if nr := s[r.name]; nr != nil {
			r = nr
			goto down
		}
		return false, fmt.Errorf("unknown workflow: %s", r.name)
	case greater:
		if p[r.idx] > r.value {
			r = r.left
		} else {
			r = r.right
		}
		goto down
	case lesser:
		if p[r.idx] < r.value {
			r = r.left
		} else {
			r = r.right
		}
		goto down
	default:
		panic("unknown rule type")
	}
	return false, nil
}

func (s sorter) ratings(parts ...part) (int, error) {
	root := s["in"]
	if root == nil {
		return 0, errors.New("no 'in' workflow found")
	}
	ratings := 0
	for _, p := range parts {
		if root := s["in"]; root != nil {
			accepted, err := s.accept(root, p)
			if err != nil {
				return 0, err
			}
			if accepted {
				ratings += p.rating()
			}
		}
	}
	return ratings, nil
}

func (r *rule) leaf(name string) {
	switch name {
	case "A":
		r.typ = accept
	case "R":
		r.typ = reject
	default:
		r.typ = descent
		r.name = name
	}
}

func compileWorkflow(workflow string) (*rule, error) {
	var root, last *rule
	for _, op := range strings.Split(workflow, ",") {
		//fmt.Printf("%q %q\n", r[1], match)
		terms := ruleRe.FindStringSubmatch(op)
		if terms == nil {
			return nil, fmt.Errorf("does not recognize: %q", op)
		}
		r := new(rule)
		terms = terms[1:]
		//fmt.Printf("\n%q: %q\n", op, terms)
		switch terms[1] {
		case "<", ">":
			if terms[1] == "<" {
				r.typ = lesser
			} else {
				r.typ = greater
			}
			r.idx = indices[terms[0]]
			v, err := strconv.Atoi(terms[2])
			if err != nil {
				return nil, fmt.Errorf("invalid value in %q: %w", terms[2], err)
			}
			r.value = v
			r.left = new(rule)
			r.left.leaf(terms[3])
		default:
			r.leaf(terms[4])
		}
		if last == nil {
			root = r
		} else {
			last.right = r
		}
		last = r
	}
	return root, nil
}

func compilePart(vars []string) (part, error) {
	x, errX := strconv.Atoi(vars[0])
	m, errM := strconv.Atoi(vars[1])
	a, errA := strconv.Atoi(vars[2])
	s, errS := strconv.Atoi(vars[3])
	return part{x, m, a, s}, errors.Join(errX, errM, errA, errS)
}

func (s sorter) combinations() (int, error) {
	root := s["in"]
	if root == nil {
		return 0, errors.New("no 'in' workflow found")
	}
	sum := 0
	limit := func(rs ranges) (ranges, bool) {
		p := 1
		for _, r := range rs {
			p *= r[1] - r[0] + 1
		}
		sum += p
		return rs, true
	}
	if err := root.propagateLimits(s, limit); err != nil {
		return 0, err
	}
	return sum, nil
}

func compose(a, b limiter) limiter {
	return func(rs ranges) (ranges, bool) {
		if nrs, ok := a(rs); ok {
			return b(nrs)
		}
		return ranges{}, false
	}
}

func (r *rule) propagate(s sorter, left, right, parent limiter) error {
	errL := r.left.propagateLimits(s, compose(left, parent))
	errR := r.right.propagateLimits(s, compose(right, parent))
	return errors.Join(errL, errR)
}

func (r *rule) propagateLimits(s sorter, parent limiter) (err error) {
	//const xmas = "xmas"
	if r == nil {
		return nil
	}
	switch r.typ {
	case reject:
		// Do nothing
	case accept:
		parent(full)
	case descent:
		if nr := s[r.name]; nr != nil {
			return nr.propagateLimits(s, parent)
		}
		err = fmt.Errorf("unknown workflow %q", r.name)
	case lesser:
		err = r.propagate(s,
			func(rs ranges) (ranges, bool) {
				//fmt.Printf("eval %c < %d %v\n", xmas[r.idx], r.value, rs[r.idx])
				if rs[r.idx][0] >= r.value {
					return ranges{}, false
				}
				rs[r.idx][1] = min(rs[r.idx][1], r.value-1)
				return rs, true
			},
			func(rs ranges) (ranges, bool) {
				//fmt.Printf("eval %c >= %d %v\n", xmas[r.idx], r.value, rs[r.idx])
				if rs[r.idx][1] < r.value {
					return ranges{}, false
				}
				rs[r.idx][0] = max(rs[r.idx][0], r.value)
				return rs, true
			},
			parent,
		)
	case greater:
		err = r.propagate(s,
			func(rs ranges) (ranges, bool) {
				//fmt.Printf("eval %c > %d %v\n", xmas[r.idx], r.value, rs[r.idx])
				if rs[r.idx][1] <= r.value {
					return ranges{}, false
				}
				rs[r.idx][0] = max(rs[r.idx][0], r.value+1)
				return rs, true
			},
			func(rs ranges) (ranges, bool) {
				//fmt.Printf("eval %c <= %d %v\n", xmas[r.idx], r.value, rs[r.idx])
				if rs[r.idx][0] > r.value {
					return ranges{}, false
				}
				rs[r.idx][1] = min(rs[r.idx][1], r.value)
				return rs, true
			},
			parent,
		)
	default:
		panic("should not happen")
	}
	return
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var parts []part
	s := sorter{}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := strings.TrimSpace(sc.Text())
		if line == "" {
			continue
		}
		if match := workflowRe.FindStringSubmatch(line); match != nil {
			r, err := compileWorkflow(match[2])
			if err != nil {
				log.Fatalf("cannot compile line %d: %q %v\n", lineNo, line, err)
			}
			s[match[1]] = r
			continue
		}
		if match := partsRe.FindStringSubmatch(line); match != nil {
			xmas, err := compilePart(match[1:])
			if err != nil {
				log.Fatalf("Cannot parse line %d: %q %v\n", lineNo, line, err)
			}
			parts = append(parts, xmas)
			continue
		}
		log.Printf("unmatched line %d: %q\n", lineNo, line)
	}
	check(sc.Err())

	if *second {
		combinations, err := s.combinations()
		check(err)
		fmt.Printf("distinct combinations of ratings: %d\n", combinations)
	} else {
		ratings, err := s.ratings(parts...)
		check(err)
		fmt.Printf("sum of parts ratings: %d\n", ratings)
	}
}
