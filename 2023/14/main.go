package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"hash"
	"hash/fnv"
	"io"
	"log"
	"os"
	"regexp"
)

type platform [][]byte

type cacheValue struct {
	before platform
	after  platform
	seen   int
}

type platformCycler struct {
	cache map[uint64][]cacheValue
	hash  hash.Hash64
}

func newPlatformCycler() *platformCycler {
	return &platformCycler{
		cache: map[uint64][]cacheValue{},
		hash:  fnv.New64(),
	}
}

func (pf platform) totalLoad() int {
	total := 0
	for i, row := range pf {
		cost := len(pf) - i
		for _, c := range row {
			if c == 'O' {
				total += cost
			}
		}
	}
	return total
}

func (pf platform) equals(o platform) bool {
	for i, row := range pf {
		if !bytes.Equal(row, o[i]) {
			return false
		}
	}
	return true
}

func (pf platform) clone() platform {
	data := make([]byte, len(pf[0])*len(pf))
	npf := make(platform, 0, len(pf))

	for p := data; len(p) > 0; p = p[len(pf[0]):] {
		npf = append(npf, p[:len(pf[0])])
	}
	npf.set(pf)
	return npf
}

func (pf platform) set(o platform) {
	for i, row := range pf {
		copy(row, o[i])
	}
}

func (pf platform) write(w io.Writer) {
	for _, row := range pf {
		w.Write(row)
	}
}

func (pf platform) dump() {
	for _, row := range pf {
		fmt.Println(string(row))
	}
}

func (pf platform) tiltSouth() {
	for i := len(pf) - 1; i > 0; i-- {
		row := pf[i]
	next:
		for j, c := range row {
			if c != '.' {
				continue
			}
			for k := i - 1; k >= 0; k-- {
				switch pf[k][j] {
				case '#':
					continue next
				case 'O':
					pf[k][j] = '.'
					row[j] = 'O'
					continue next
				}
			}
		}
	}
}

func (pf platform) tiltEast() {
	for i := len(pf[0]) - 1; i > 0; i-- {
	next:
		for j := 0; j < len(pf); j++ {
			row := pf[j]
			if row[i] != '.' {
				continue
			}
			for k := i - 1; k >= 0; k-- {
				switch row[k] {
				case '#':
					continue next
				case 'O':
					row[k] = '.'
					row[i] = 'O'
					continue next
				}
			}
		}
	}
}

func (pf platform) tiltWest() {
	for i := 0; i < len(pf[0])-1; i++ {
	next:
		for j := 0; j < len(pf); j++ {
			row := pf[j]
			if row[i] != '.' {
				continue
			}
			for k := i + 1; k < len(row); k++ {
				switch row[k] {
				case '#':
					continue next
				case 'O':
					row[k] = '.'
					row[i] = 'O'
					continue next
				}
			}
		}
	}
}

func (pf platform) tiltNorth() {
	for i := 0; i < len(pf)-1; i++ {
		row := pf[i]
	next:
		for j, c := range row {
			if c != '.' {
				continue
			}
			for k := i + 1; k < len(pf); k++ {
				switch pf[k][j] {
				case '#':
					continue next
				case 'O':
					pf[k][j] = '.'
					row[j] = 'O'
					continue next
				}
			}
		}
	}
}

func (pf platform) spinCycle() {
	pf.tiltNorth()
	pf.tiltWest()
	pf.tiltSouth()
	pf.tiltEast()
}

func (pfc *platformCycler) spinCycle(pf platform, cycle int) int {

	pfc.hash.Reset()
	pf.write(pfc.hash)
	key := pfc.hash.Sum64()

	list := pfc.cache[key]
	for i := range list {
		if cv := &list[i]; cv.before.equals(pf) {
			pf.set(cv.after)
			last := cv.seen
			cv.seen = cycle
			return last
		}
	}

	before := pf.clone()

	pf.spinCycle()

	list = append(list, cacheValue{
		before: before,
		after:  pf.clone(),
		seen:   cycle,
	})
	pfc.cache[key] = list
	return cycle
}

var rocksRe = regexp.MustCompile(`^[O#\.]+$`)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {

	debug := flag.Bool("debug", false, "turn on debug output")
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var pf platform
	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		if !rocksRe.Match(line) {
			log.Printf("unmatches line %d: %q\n", lineNo, line)
			continue
		}
		row := bytes.Clone(line)
		if len(pf) > 0 && len(pf[0]) != len(row) {
			log.Printf("line %d length %d does not match %d\n",
				lineNo, len(row), len(pf[0]))
			continue
		}
		pf = append(pf, row)
	}
	check(sc.Err())

	if *second {
		const cycles = 1_000_000_000

		for i, pfc := 0, newPlatformCycler(); i < cycles; i++ {
			if seen := pfc.spinCycle(pf, i); seen < i {
				// Don't jump too far ahead!
				if since := i - seen; i+since <= cycles {
					i += since
				}
			}
		}
	} else {
		pf.tiltNorth()
	}

	if *debug {
		pf.dump()
	}

	fmt.Printf("total load: %d\n", pf.totalLoad())
}
