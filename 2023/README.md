# Solutions for Advent of Code 2023

To compile the example you need at least Go 1.21.

All solutions take their input from STDIN and writes the results
to STDOUT. Adding the flag `-second` will print the
solution of the second puzzle of the day. For example:

```
go run 01/main.go < /location/of/your/aoc2023/01/input
go run 01/main.go -second < /location/of/your/aoc2023/01/input
```

**Warning**: The solution of day [25](./25) is a blunt brute force one.
So execute with care. On my machine (Ryzen 9 5950X 16C/32T) it takes ~40min to complete.
I have decided to let it in because it granted me the stars for the last day.
For my personal satisfaction I've added [25faster](./25faster) which uses
the much better minimum cut algorithm from [Karger](https://en.wikipedia.org/wiki/Karger%27s_algorithm).

