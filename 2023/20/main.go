package main

import (
	"bufio"
	"container/list"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

type moduleType int

const (
	broadcaster moduleType = iota
	flipFlop
	conjunction
	output
)

type pulse bool

const (
	low  pulse = false
	high pulse = true
)

type module struct {
	typ       moduleType
	state     pulse
	name      string
	receivers []string
	inputs    map[string]pulse
}

type message struct {
	sender    string
	receivers []string
	msg       pulse
}

type system struct {
	modules map[string]*module
	queue   *list.List
	highs   int
	lows    int
	debug   bool
}

func (p pulse) String() string {
	if p {
		return "high"
	}
	return "low"
}

func newSystem(debug bool) *system {
	return &system{
		modules: map[string]*module{},
		queue:   list.New(),
		debug:   debug,
	}
}

func (s *system) send(sender string, receivers []string, msg pulse) {
	s.queue.PushBack(&message{sender, receivers, msg})
}

func (s *system) run() {
	for s.queue.Len() > 0 {
		last := s.queue.Front()
		msg := last.Value.(*message)
		s.queue.Remove(last)
		if msg.msg {
			s.highs += len(msg.receivers)
		} else {
			s.lows += len(msg.receivers)
		}
		for _, receiver := range msg.receivers {
			if module := s.modules[receiver]; module != nil {
				if s.debug {
					log.Printf("%s -%s-> %s\n", msg.sender, msg.msg, receiver)
				}
				module.receive(s, msg.sender, msg.msg)
			}
		}
	}
}

func (s *system) button() {
	if b := s.modules["broadcaster"]; b != nil {
		s.lows++
		s.send(b.name, b.receivers, low)
	} else {
		log.Println("nothing happened")
	}
}

func (s *system) wire() {
	for _, m := range s.modules {
		for _, receiver := range m.receivers {
			rm := s.modules[receiver]
			if rm == nil {
				log.Printf("reciever %q not found", receiver)
				continue
			}
			if rm.typ == conjunction {
				if rm.inputs == nil {
					rm.inputs = map[string]pulse{}
				}
				rm.inputs[m.name] = low
			}
		}
	}
}

func (m *module) receive(s *system, sender string, p pulse) {
	switch m.typ {
	case flipFlop:
		if p == low {
			m.state = !m.state
			s.send(m.name, m.receivers, m.state)
		}
	case conjunction:
		m.inputs[sender] = p
		all := true
		for _, on := range m.inputs {
			if !on {
				all = false
				break
			}
		}
		s.send(m.name, m.receivers, pulse(!all))
	}
}

func (s *system) part1(presses int) {
	for i := 0; i < presses; i++ {
		s.button()
		s.run()
	}

	log.Printf("low pulses: %d high pulses: %d\n", s.lows, s.highs)
	fmt.Println(s.lows * s.highs)
}

func (s *system) part2() {
	// Inspired by a reddit post
	m := s.modules["broadcaster"]

	p2 := uint64(1)
	for _, r := range m.receivers {
		current := s.modules[r]
		var b, mask uint64 = 0, 1
		for current != nil {
			var next *module
			for _, cr := range current.receivers {
				dst := s.modules[cr]
				if dst.typ == conjunction {
					b |= mask
				}
				if next == nil && dst.typ == flipFlop {
					next = dst
				}
			}
			mask <<= 1
			current = next
		}
		p2 *= b
	}
	fmt.Println(p2)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

var lineRe = regexp.MustCompile(
	`^(?:broadcaster|([%&])([a-z]+))\s->\s(.+)$`)

func asSlice(s string) []string {
	fs := strings.Split(s, ",")
	slice := make([]string, len(fs))
	for i, f := range fs {
		slice[i] = strings.TrimSpace(f)
	}
	return slice
}

func main() {
	var (
		debug   = flag.Bool("debug", false, "turn on debug output")
		second  = flag.Bool("second", false, "second puzzle")
		presses = flag.Int("presses", 1000, "number of button presses")
	)
	flag.Parse()

	sys := newSystem(*debug)

	sc := bufio.NewScanner(os.Stdin)
lines:
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		m := lineRe.FindStringSubmatch(line)
		if m == nil {
			log.Printf("unmatched line %d: %q\n", lineNo, line)
			continue
		}
		m = m[1:]
		var mt moduleType
		switch {
		case m[0] == "":
			mt = broadcaster
			m[1] = "broadcaster"
		case m[0] == "%":
			mt = flipFlop
		case m[0] == "&":
			mt = conjunction
		default:
			log.Printf("unknown module %q in line %d\n", m[1], lineNo)
			continue lines
		}
		sys.modules[m[1]] = &module{
			typ:       mt,
			name:      m[1],
			receivers: asSlice(m[2]),
		}

		//fmt.Printf("%+q\n", m)
	}
	check(sc.Err())

	sys.modules["output"] = &module{
		typ:  output,
		name: "output",
	}

	sys.wire()
	if *second {
		sys.part2()
	} else {
		sys.part1(*presses)
	}
}
