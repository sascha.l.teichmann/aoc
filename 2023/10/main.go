package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

type (
	segment byte
	vec     [2]int
	raster  [][]segment
)

const (
	northSouth segment = '|'
	eastWest   segment = '-'
	northEast  segment = 'L'
	northWest  segment = 'J'
	southWest  segment = '7'
	southEast  segment = 'F'
	ground     segment = '.'
	start      segment = 'S'
)

var (
	north = vec{0, -1}
	east  = vec{+1, 0}
	south = vec{0, +1}
	west  = vec{-1, 0}
	none  = vec{0, 0}
)

func (v vec) add(o vec) vec {
	return vec{v[0] + o[0], v[1] + o[1]}
}

func (v vec) sub(o vec) vec {
	return vec{v[0] - o[0], v[1] - o[1]}
}

func (v vec) negate() vec {
	return vec{-v[0], -v[1]}
}

func (p segment) openings() [2]vec {
	switch p {
	case northSouth:
		return [2]vec{north, south}
	case eastWest:
		return [2]vec{east, west}
	case northEast:
		return [2]vec{north, east}
	case northWest:
		return [2]vec{north, west}
	case southWest:
		return [2]vec{south, west}
	case southEast:
		return [2]vec{south, east}
	default:
		return [2]vec{none, none}
	}
}

func (r raster) valid(p vec) bool {
	if p[1] < 0 || p[1] >= len(r) {
		return false
	}
	row := r[p[1]]
	return p[0] >= 0 && p[0] < len(row)
}

func (r raster) get(p vec) segment {
	return r[p[1]][p[0]]
}

func (r raster) set(p vec, s segment) {
	r[p[1]][p[0]] = s
}

func (r raster) size() (int, int) {
	h := len(r)
	if h == 0 {
		return 0, 0
	}
	return len(r[0]), h
}

func (r raster) connected(a, b vec) bool {
	from := func(a, b vec) bool {
		for _, open := range r.get(a).openings() {
			if a.add(open) == b {
				return true
			}
		}
		return false
	}
	return from(a, b) && from(b, a)
}

func (r raster) display() {
	for y, row := range r {
		fmt.Printf("%3d %s\n", y, string(row))
	}
}

func newRaster(w, h int) raster {
	data := make([]segment, w*h)
	for i := range data {
		data[i] = ground
	}
	nr := make(raster, 0, h)
	for p := data; len(p) >= w; p = p[w:] {
		nr = append(nr, p[:w])
	}
	return nr
}

func other(dirs [2]vec, dir vec) vec {
	switch {
	case dir == dirs[0]:
		return dirs[1]
	case dir == dirs[1]:
		return dirs[0]
	default:
		panic("should not happen")
	}
}

var marks = map[segment]map[vec][]vec{
	northSouth: {north: {east}, south: {west}},
	eastWest:   {east: {south}, west: {north}},
	northEast:  {east: {south, west}},
	northWest:  {north: {south, east}},
	southWest:  {west: {north, east}},
	southEast:  {south: {north, west}},
}

func (r raster) enclosed(startPos, entry vec) int {

	// Do a simulation run to check if
	// the order is right to wrap the loop.

	// find lowest y
	pos, last := entry, startPos
	yMin := min(pos[1], last[1])
	for pos != startPos {
		yMin = min(yMin, pos[1])
		from := last.sub(pos)
		dir := other(r.get(pos).openings(), from)
		last, pos = pos, pos.add(dir)
	}

	pos, last = entry, startPos
	for pos != startPos {
		from := last.sub(pos)
		v := r.get(pos)
		dir := other(v.openings(), from)

		for _, m := range marks[v][from] {
			if mp := pos.add(m); mp[1] < yMin {
				// being smaller means that the loop gets wrapped.
				goto orderIsLeft
			}
		}
		last, pos = pos, pos.add(dir)
	}
	// oder is right -> swap
	log.Println("traversal order needs to be swapped")
	entry, startPos = startPos, entry

orderIsLeft:
	// Now do the real wrapping.
	// left in traversal direction is outside.
	// mark ground directly on the outside with 'o's.
	pos, last = entry, startPos
	for pos != startPos {
		from := last.sub(pos)
		v := r.get(pos)
		dir := other(v.openings(), from)

		for _, m := range marks[v][from] {
			if mp := pos.add(m); r.valid(mp) && r.get(mp) == ground {
				r.set(mp, 'o')
			}
		}
		last, pos = pos, pos.add(dir)
	}

	// flood fill the rest
	width, height := r.size()
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pos := vec{x, y}
			if r.get(pos) == ground && r.floodFind(pos, 'o') {
				r.set(pos, 'o')
			}
		}
	}

	// count remaining groud tiles
	sum := 0
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pos := vec{x, y}
			if r.get(pos) == ground {
				sum++
			}
		}
	}
	return sum
}

func (r raster) floodFind(pos vec, s segment) bool {
	orig := r.get(pos)
	if orig == s {
		return true
	}
	visted := map[vec]bool{pos: true}
	stack := []vec{pos}
	for len(stack) > 0 {
		current := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		for _, dir := range []vec{north, east, south, west} {
			if p := current.add(dir); r.valid(p) && !visted[p] {
				v := r.get(p)
				if v == s {
					return true
				}
				if v != orig {
					continue
				}
				visted[p] = true
				stack = append(stack, p)
			}
		}
	}
	return false
}

func (r raster) replace(start vec, entries []vec) {
	old := r.get(start)
nextSegment:
	for _, s := range []segment{
		northSouth,
		eastWest,
		northEast,
		northWest,
		southWest,
		southEast,
	} {
		r.set(start, s)
		for _, entry := range entries {
			if !r.connected(start, entry) {
				continue nextSegment
			}
		}
		return
	}
	r.set(start, old)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	displayTrace := flag.Bool("displaytrace", false, "display trace")
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	sc := bufio.NewScanner(os.Stdin)
	var r raster
	startPos := vec{-1, -1}
	for sc.Scan() {
		line := sc.Bytes()
		row := make([]segment, 0, len(line))
		for x, c := range line {
			if c == 'S' {
				startPos = vec{x, len(r)}
				log.Printf("start found: %v\n", startPos)
			}
			row = append(row, segment(c))
		}
		r = append(r, row)
	}
	check(sc.Err())

	if !r.valid(startPos) {
		log.Fatalln("no start found")
	}

	var entries []vec

	for _, dir := range []vec{north, east, south, west} {
		p := startPos.add(dir)
		if !r.valid(p) {
			continue
		}
		s := r.get(p)
		so := s.openings()
		if slices.Contains(so[:], dir.negate()) {
			entries = append(entries, p)
		}
	}

	if len(entries) != 2 {
		log.Fatalf(
			"wrong number of entries: need 2 have %d\n",
			len(entries))
	}

	pos := entries[0]
	last := startPos
	length := 1

	trace := newRaster(r.size())

	trace.set(startPos, start)

tracing:
	for ; pos != startPos; length++ {
		s := r.get(pos)
		trace.set(pos, s)
		for _, open := range s.openings() {
			exit := pos.add(open)
			if exit != last && r.valid(exit) {
				pos, last = exit, pos
				continue tracing
			}
		}
		log.Fatalln("fell of map")
	}

	if !*second {
		fmt.Printf("farest: %d\n", length/2)
		return
	}

	trace.replace(startPos, entries)
	fmt.Printf("enclosed: %d\n", trace.enclosed(startPos, entries[0]))

	if *displayTrace {
		trace.display()
	}
}
