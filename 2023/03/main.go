package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type schematic [][]byte

func loadSchematic(r io.Reader) (schematic, error) {
	sc := bufio.NewScanner(r)
	var s schematic
	for sc.Scan() {
		line := sc.Bytes()
		s = append(s, bytes.Clone(line))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return s, nil
}

func (s schematic) neighbors(x, y int, fn func(int, int, []byte) bool) bool {
	for i := -1; i <= 1; i++ {
		yi := y + i
		if yi < 0 || yi >= len(s) {
			continue
		}
		for j, row := -1, s[yi]; j <= 1; j++ {
			if i == 0 && j == 0 {
				continue
			}
			if xj := x + j; xj >= 0 && xj < len(row) {
				if !fn(xj, yi, row) {
					return false
				}
			}
		}
	}
	return true
}

func isDigit(x byte) bool { return '0' <= x && x <= '9' }

func (s schematic) connected(x, y int) bool {
	con := false
	s.neighbors(x, y, func(x, _ int, row []byte) bool {
		if c := row[x]; c != '.' && !isDigit(c) {
			con = true
		}
		return !con
	})
	return con
}

func (s schematic) partNumberSum() int {

	var (
		con bool
		sum int
		b   strings.Builder
	)

	add := func() {
		if con {
			num, _ := strconv.Atoi(b.String())
			sum += num
			con = false
		}
		b.Reset()
	}

	for i, row := range s {
		for j, c := range row {
			if b.Len() == 0 { // out of number
				if isDigit(c) { // start a number
					b.WriteByte(c)
				}
			} else { // inside number
				if isDigit(c) {
					b.WriteByte(c)
				} else { // number left
					add()
				}
			}
			if b.Len() > 0 && !con { // need to check if connected
				con = s.connected(j, i)
			}
		}
		if b.Len() > 0 {
			add()
		}
	}
	return sum
}

func (s schematic) gearRatioSum() int {

	type coord struct {
		x int
		y int
	}

	sum := 0
	for i, row := range s {
		for j, c := range row {
			if c != '*' {
				continue
			}
			numbers := map[coord]int{}
			s.neighbors(j, i, func(x, y int, row []byte) bool {
				if !isDigit(row[x]) {
					return true
				}
				//revive:disable:empty-block
				for ; x > 0 && isDigit(row[x-1]); x-- {
				}
				//revive:enable:empty-block
				key := coord{x, y}
				if _, already := numbers[key]; already {
					return true
				}
				end := x + 1
				//revive:disable:empty-block
				for ; end < len(row) && isDigit(row[end]); end++ {
				}
				//revive:enable:empty-block
				num, _ := strconv.Atoi(string(row[x:end]))
				numbers[key] = num
				return true
			})
			if len(numbers) == 2 {
				prod := 1
				for _, num := range numbers {
					prod *= num
				}
				sum += prod
			}
		}
	}
	return sum
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	s, err := loadSchematic(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		fmt.Printf("gear ratio sum: %d\n", s.gearRatioSum())
	} else {
		fmt.Printf("part number sum: %d\n", s.partNumberSum())
	}
}
