package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

var cardRe = regexp.MustCompile(`^Card\s+(\d+):\s+([^|]+)\|\s*(.+)$`)

func main() {

	var (
		sc        = bufio.NewScanner(os.Stdin)
		pointsSum = 0
		cardWins  = map[int]int{}
		maxCard   = -1
		stack     []int
		second    = flag.Bool("second", false, "second puzzle")
	)

	flag.Parse()

	for lineNo := 1; sc.Scan(); lineNo++ {
		m := cardRe.FindStringSubmatch(sc.Text())
		if len(m) == 0 {
			log.Printf("line %d is not matching.\n", lineNo)
			continue
		}
		fields := strings.Fields(m[2])
		winning := make([]int, len(fields))
		for i, s := range fields {
			x, err := strconv.Atoi(s)
			if err != nil {
				log.Fatalf("error: %v\n", err)
			}
			winning[i] = x
		}
		points, wins := 0, 0
		for _, s := range strings.Fields(m[3]) {
			x, err := strconv.Atoi(s)
			if err != nil {
				log.Fatalf("error: %v\n", err)
			}
			if slices.Contains(winning, x) {
				wins++
				if points == 0 {
					points = 1
				} else {
					points *= 2
				}
			}
		}
		pointsSum += points
		card, _ := strconv.Atoi(m[1])
		cardWins[card] = wins
		maxCard = max(maxCard, card)

		stack = append(stack, card)
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if !*second {
		fmt.Printf("points: %d\n", pointsSum)
		return
	}

	var (
		counts = make(map[int]int, len(stack))
		next   = make([]int, 0, len(stack))
	)

	for len(stack) > 0 {
		for _, card := range stack {
			counts[card]++
			for i, wins := 1, cardWins[card]; i <= wins; i++ {
				if card+i <= maxCard {
					next = append(next, card+i)
				}
			}
		}
		stack, next = next, stack[:0]
	}

	cardsSum := 0
	for _, s := range counts {
		cardsSum += s
	}

	fmt.Printf("cards: %d\n", cardsSum)
}
