package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"time"
)

type (
	grid [][]byte

	vec2 [2]int

	edge struct {
		next   int32
		length int32
	}
	graph [][]edge
)

var dirs = [4]vec2{
	{0, -1},
	{+1, 0},
	{0, +1},
	{-1, 0},
}

func (v vec2) add(b vec2) vec2 {
	return vec2{v[0] + b[0], v[1] + b[1]}
}

func (g grid) isValid(p vec2) bool {
	return 0 <= p[1] && p[1] < len(g) && 0 <= p[0] && p[0] < len(g[p[1]])
}

func (gr graph) add(p int32, e edge) {
	if es := gr[p]; !slices.Contains(es, e) {
		gr[p] = append(es, e)
	}
}

func (gr graph) del(p int32, e edge) {
	es := gr[p]
	if idx := slices.Index(es, e); idx != -1 {
		gr[p] = slices.Delete(es, idx, idx+1)
	}
}

func vecIndex(gr *graph, vs map[vec2]int32) func(vec2) int32 {
	return func(v vec2) int32 {
		if idx, ok := vs[v]; ok {
			return idx
		}
		idx := int32(len(*gr))
		*gr = append(*gr, nil)
		vs[v] = idx
		return idx
	}
}

func (g grid) buildSlopeGraph() (graph, map[vec2]int32) {
	gr := graph{}
	vs := map[vec2]int32{}
	vi := vecIndex(&gr, vs)

	for r, row := range g {
		for c, v := range row {
			current := vec2{c, r}
			switch v {
			case '.':
				for _, d := range dirs {
					n := current.add(d)
					if !g.isValid(n) {
						continue
					}
					if g[n[1]][n[0]] == '.' {
						gr.add(vi(current), edge{vi(n), 1})
						gr.add(vi(n), edge{vi(current), 1})
					}
				}
			case '>':
				gr.add(vi(current), edge{vi(current.add(vec2{1, 0})), 1})
				gr.add(vi(current.add(vec2{-1, 0})), edge{vi(current), 1})
			case 'v':
				gr.add(vi(current), edge{vi(current.add(vec2{0, 1})), 1})
				gr.add(vi(current.add(vec2{0, -1})), edge{vi(current), 1})
			case '<':
				gr.add(vi(current), edge{vi(current.add(vec2{-1, 0})), 1})
				gr.add(vi(current.add(vec2{1, 0})), edge{vi(current), 1})
			case '^':
				gr.add(vi(current), edge{vi(current.add(vec2{0, -1})), 1})
				gr.add(vi(current.add(vec2{0, 1})), edge{vi(current), 1})
			}
		}
	}
	return gr, vs
}

func (g grid) buildGraph() (graph, map[vec2]int32) {
	gr := graph{}
	vs := map[vec2]int32{}
	vi := vecIndex(&gr, vs)

	for r, row := range g {
		for c, v := range row {
			if v == '#' {
				continue
			}
			current := vec2{c, r}
			for _, d := range dirs {
				n := current.add(d)
				if !g.isValid(n) {
					continue
				}
				if g[n[1]][n[0]] != '#' {
					gr.add(vi(current), edge{vi(n), 1})
					gr.add(vi(n), edge{vi(current), 1})
				}
			}
		}
	}

outer:
	for {
		for n, e := range gr {
			if len(e) == 2 {
				a, b := e[0], e[1]
				gr.del(a.next, edge{int32(n), a.length})
				gr.del(b.next, edge{int32(n), b.length})
				gr.add(a.next, edge{b.next, a.length + b.length})
				gr.add(b.next, edge{a.next, a.length + b.length})
				gr[n] = nil
				continue outer
			}
		}
		break
	}

	return gr, vs
}

func (gr graph) longestPath(start, goal int32) int {
	longest := 0
	visited := make([]uint64, len(gr)/64+1)
	stack := []edge{{start, 0}}
	for len(stack) > 0 {
		current := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		if current.length == -1 {
			visited[current.next/64] &= ^(1 << uint64(current.next%64))
			continue
		}
		if current.next == goal {
			longest = max(longest, int(current.length))
			continue
		}
		if visited[current.next/64]&(1<<uint64(current.next%64)) != 0 {
			continue
		}
		visited[current.next/64] |= 1 << uint64(current.next%64)
		stack = append(stack, edge{current.next, -1})
		for _, n := range gr[current.next] {
			stack = append(stack, edge{n.next, current.length + n.length})
		}
	}
	return longest
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

var lineRe = regexp.MustCompile(`^[#\.\^\>v\<]+$`)

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	var g grid
	start := vec2{-1, -1}
	goal := vec2{-1, -1}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		if !lineRe.Match(line) {
			log.Printf("unmatched line %d: %q\n", lineNo, line)
			continue
		}
		if len(g) > 0 && len(line) != len(g[0]) {
			log.Printf("line %d has a different length\n", lineNo)
			continue
		}
		goal[0] = bytes.IndexByte(line, '.')
		goal[1] = lineNo - 1
		if start[0] == -1 {
			start[0] = goal[0]
			start[1] = lineNo - 1
		}

		g = append(g, bytes.Clone(line))
	}
	check(sc.Err())

	if len(g) == 0 {
		log.Fatalln("grid is empty")
	}
	if start[0] == -1 {
		log.Fatalln("no start position found")
	}

	builder := grid.buildSlopeGraph
	if *second {
		builder = grid.buildGraph
	}

	begin := time.Now()
	gr, vs := builder(g)
	if *debug {
		log.Printf("num edges: %d\n", len(gr))
	}
	fmt.Printf("longest path: %d\n", gr.longestPath(vs[start], vs[goal]))
	if *debug {
		log.Printf("took: %s\n", time.Since(begin))
	}
}
