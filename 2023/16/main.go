package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

type grid [][]byte

type vec struct {
	x int
	y int
}

type direction int

const (
	north direction = iota
	east
	south
	west
)

type beam struct {
	vec
	from direction
}

var dirVecs = [4]vec{
	north: {0, -1},
	east:  {+1, 0},
	south: {0, +1},
	west:  {-1, 0},
}

var mirrors = map[byte][]direction{
	'/': {
		north: west,
		east:  south,
		south: east,
		west:  north,
	},
	'\\': {
		north: east,
		east:  north,
		south: west,
		west:  south,
	},
}

var opposite = [4]direction{
	north: south,
	east:  west,
	south: north,
	west:  east,
}

var splits = map[byte][][]direction{
	'|': {
		north: nil,
		east:  {north, south},
		south: nil,
		west:  {north, south},
	},
	'-': {
		north: {east, west},
		east:  nil,
		south: {east, west},
		west:  nil,
	},
}

func (v vec) add(o vec) vec {
	return vec{
		x: v.x + o.x,
		y: v.y + o.y,
	}
}

func (d direction) String() string {
	switch d {
	case north:
		return "north"
	case east:
		return "east"
	case south:
		return "south"
	case west:
		return "west"
	default:
		return fmt.Sprintf("unknown direction %d", d)
	}
}

func (b beam) advance() beam {
	return beam{
		vec:  b.vec.add(dirVecs[opposite[b.from]]),
		from: b.from,
	}
}

func (g grid) valid(v vec) bool {
	return 0 <= v.y && v.y < len(g) && 0 <= v.x && v.x < len(g[v.y])
}

func energized(visited map[beam]struct{}) int {
	m := map[vec]struct{}{}
	for k := range visited {
		m[k.vec] = struct{}{}
	}
	return len(m)
}

func (g grid) run(startVec vec, startFrom direction, debug bool) int {

	var (
		start   = beam{vec: startVec, from: startFrom}
		stack   = []beam{start}
		visited = map[beam]struct{}{start: {}}
	)

	visit := func(n beam) {
		if g.valid(n.vec) {
			key := beam{vec: n.vec, from: opposite[n.from]}
			if _, ok := visited[key]; !ok {
				visited[key] = struct{}{}
				stack = append(stack, n)
			}
		}
	}

	for len(stack) > 0 {
		current := stack[len(stack)-1]
		stack = stack[:len(stack)-1]
		tile := g[current.y][current.x]
		if debug {
			log.Printf("pos: %c %v from %s\n", tile, current.vec, current.from)
		}
		switch tile {
		case '.':
			visit(current.advance())
		case '/', '\\':
			dir := mirrors[tile][current.from]
			if debug {
				log.Printf("mirror %c from %s goto %s\n", tile, current.from, dir)
			}
			visit(beam{vec: current.vec, from: opposite[dir]}.advance())
		case '|', '-':
			if debug {
				log.Printf("split %c %v from %s\n", tile, current.vec, current.from)
			}
			if dirs := splits[tile][current.from]; len(dirs) == 0 { // like '.'
				if debug {
					log.Println("-> like .")
				}
				visit(current.advance())
			} else {
				if debug {
					log.Printf("-> goto %s and %s\n", dirs[0], dirs[1])
				}
				visit(beam{vec: current.vec, from: dirs[0]}.advance())
				visit(beam{vec: current.vec, from: dirs[1]}.advance())
			}
		}
	}

	return energized(visited)
}

func part1(g grid, debug bool) {
	fmt.Printf("energized: %d\n", g.run(vec{x: 0, y: 0}, west, debug))
}

func part2(g grid, debug bool) {
	energy := 0
	for y, row := range g {
		energy = max(energy, g.run(vec{0, y}, west, debug))
		energy = max(energy, g.run(vec{len(row) - 1, y}, east, debug))
	}
	for x := range g[0] {
		energy = max(energy, g.run(vec{x, 0}, north, debug))
		energy = max(energy, g.run(vec{x, len(g) - 1}, south, debug))
	}
	fmt.Printf("energized: %d\n", energy)
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

var gridRe = regexp.MustCompile(`^([/\\|\.\-]+)$`)

func main() {
	debug := flag.Bool("debug", false, "turn on debug output")
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var g grid

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		if m := gridRe.FindSubmatch(line); m != nil {
			if len(g) > 0 && len(line) != len(g[0]) {
				log.Fatalf("line %d differs in length.\n", lineNo)
			}
			g = append(g, bytes.Clone(line))
		} else {
			log.Printf("unmatched line %d: %q\n", lineNo, line)
		}
	}
	check(sc.Err())

	if len(g) == 0 {
		log.Fatalln("grid is empty")
	}

	if *second {
		part2(g, *debug)
	} else {
		part1(g, *debug)
	}
}
