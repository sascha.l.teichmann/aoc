package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

var (
	instrRe = regexp.MustCompile(`^([LR]+)$`)
	dataRe  = regexp.MustCompile(`^([0-9A-Z]+) = \(([0-9A-Z]+), ([0-9A-Z]+)\)$`)
)

func part1(instructions string, routes map[string][2]string) {
	if instructions == "" {
		log.Fatalln("no instructions given")
	}

	pos := "AAA"

	current, ok := routes[pos]
	if !ok {
		log.Fatalln("No start found")
	}

	count := 0
	for instr := 0; pos != "ZZZ"; instr, count = (instr+1)%len(instructions), count+1 {
		var dir int
		if instructions[instr] == 'R' {
			dir = 1
		}
		next := current[dir]
		if current, ok = routes[next]; !ok {
			log.Fatalf("route to %q not found.\n", next)
		}
		pos = next
	}
	fmt.Println(count)
}

func part2(instructions string, routes map[string][2]string) {

	names := map[string]int16{}

	for start, next := range routes {
		for _, name := range []string{start, next[0], next[1]} {
			if _, ok := names[name]; !ok {
				names[name] = int16(len(names))
			}
		}
	}

	var (
		nodes  = make([]int16, len(names)*2)
		ends   = make([]bool, len(names))
		ghosts []int16
	)

	for start, next := range routes {
		n := names[start]
		nodes[n*2+0] = names[next[0]]
		nodes[n*2+1] = names[next[1]]
		switch start[len(start)-1] {
		case 'A':
			ghosts = append(ghosts, n)
		case 'Z':
			ends[n] = true
		}
	}

	if len(ghosts) == 0 {
		log.Fatalln("no starting points found")
	}

	instrs := make([]int16, len(instructions))
	for i, instr := range instructions {
		if instr == 'R' {
			instrs[i] = 1
		}
	}

	type steps struct {
		next *steps
		num  uint64
	}

	buildChain := func(ghost int16) *steps {
		type loopKey struct {
			last    int16
			current int16
			instr   int
		}
		var (
			detector     = map[loopKey]*steps{}
			last         int16
			instr        int
			first, chain *steps
		)
		for {
			var counter uint64
			for ; !ends[ghost]; counter++ {
				last = ghost
				ghost = nodes[ghost*2+instrs[instr]]
				instr = (instr + 1) % len(instrs)
			}
			key := loopKey{
				last:    last,
				current: ghost,
				instr:   instr,
			}
			nsteps := &steps{num: counter}
			if first == nil {
				first = nsteps
				detector[key] = first
				chain = first
			} else {
				if already := detector[key]; already != nil {
					chain.next = already
					break
				}
				chain.next = nsteps
				chain = nsteps
				detector[key] = nsteps
			}
		}
		return first
	}

	var (
		counters = make([]uint64, len(ghosts))
		chains   = make([]*steps, len(ghosts))
	)
	for i, g := range ghosts {
		chains[i] = buildChain(g)
	}

	var maxCounter uint64

	for i, chain := range chains {
		counters[i] = chain.num
		maxCounter = max(maxCounter, chain.num)
		chains[i] = chain.next
	}

	for {
		behind := false
		for i, c := range counters {
			if c < maxCounter {
				behind = true
				counters[i] += chains[i].num
				maxCounter = max(maxCounter, counters[i])
				chains[i] = chains[i].next
			}
		}
		if !behind {
			break
		}
	}
	fmt.Println(counters[0])
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var (
		routes       = map[string][2]string{}
		instructions string
	)

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		if m := instrRe.FindStringSubmatch(line); m != nil {
			instructions = m[1]
			continue
		}
		if m := dataRe.FindStringSubmatch(line); m != nil {
			routes[m[1]] = [2]string{m[2], m[3]}
			continue
		}
		//log.Printf("unmatched line %d: %q\n", lineNo, line)
	}
	if err := sc.Err(); err != nil {
		log.Fatalln(err)
	}

	if *second {
		part2(instructions, routes)
	} else {
		part1(instructions, routes)
	}
}
