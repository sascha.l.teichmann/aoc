package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var springsRe = regexp.MustCompile(
	`^([?#.]+)\s(.+)$`)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func asInts(s string) ([]int, error) {
	ns := strings.Split(s, ",")
	result := make([]int, 0, len(ns))
	for _, x := range ns {
		v, err := strconv.Atoi(x)
		if err != nil {
			return nil, err
		}
		result = append(result, v)
	}
	return result, nil
}

func memoize(fn func(int, int) int) func(int, int) int {
	cache := map[[2]int]int{}
	return func(a, b int) int {
		key := [2]int{a, b}
		if v, ok := cache[key]; ok {
			return v
		}
		v := fn(a, b)
		cache[key] = v
		return v
	}
}

func process(pattern string, numbers []int) int {

	var recurse func(int, int) int
	recurse = func(pIdx, nIdx int) int {
		if pIdx == len(pattern) {
			if nIdx == len(numbers) {
				return 1
			}
			return 0
		}
		total := 0

		if p := pattern[pIdx]; p == '.' || p == '?' {
			total = recurse(pIdx+1, nIdx)
		}

		if nIdx == len(numbers) {
			return total
		}

		pnIdx := pIdx + int(numbers[nIdx])
		if pnIdx >= len(pattern) {
			return total
		}

		if p := pattern[pIdx]; (p == '#' || p == '?') &&
			strings.IndexByte(pattern[pIdx:pnIdx], '.') == -1 &&
			pattern[pnIdx] != '#' {
			total += recurse(pnIdx+1, nIdx+1)
		}

		return total
	}
	recurse = memoize(recurse)
	return recurse(0, 0)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	total := 0
	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		m := springsRe.FindStringSubmatch(line)
		if m != nil {
			pattern := m[1] + "?"
			numbers, err := asInts(m[2])
			check(err)
			if *second {
				pattern = strings.Repeat(pattern, 5)
				nnumbers := make([]int, 0, 5*len(numbers))
				for i := 0; i < 5; i++ {
					nnumbers = append(nnumbers, numbers...)
				}
				numbers = nnumbers
			}
			total += process(pattern, numbers)
			continue
		}
		log.Printf("unmatched line %d: %q\n", lineNo, line)
	}
	check(sc.Err())
	fmt.Println(total)
}
