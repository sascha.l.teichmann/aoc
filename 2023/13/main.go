package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

var patternRe = regexp.MustCompile(
	`^([#.]+)$`)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

type matrix [][]rune

func (m matrix) sameRow(i, j int) bool {
	irow, jrow := m[i], m[j]
	for k, a := range irow {
		if a != jrow[k] {
			return false
		}
	}
	return true
}

func (m matrix) sameColumn(i, j int) bool {
	for _, row := range m {
		if row[i] != row[j] {
			return false
		}
	}
	return true
}

func (m matrix) findVerticalMirror() []int {
	var columns []int
	for i := 0; i < len(m[0])-1; i++ {
		j := i + 1
		if m.sameColumn(i, j) {
			ki := i - 1
			kj := j + 1
			for ; ki >= 0 && kj < len(m[0]); ki, kj = ki-1, kj+1 {
				if !m.sameColumn(ki, kj) {
					break
				}
			}
			if ki < 0 || kj >= len(m[0]) {
				columns = append(columns, i)
			}
		}
	}
	return columns
}

func (m matrix) findHorizontalMirror() []int {
	var lines []int
	for i := 0; i < len(m)-1; i++ {
		j := i + 1
		if m.sameRow(i, j) {
			ki := i - 1
			kj := j + 1
			for ; ki >= 0 && kj < len(m); ki, kj = ki-1, kj+1 {
				if !m.sameRow(ki, kj) {
					break
				}
			}
			if ki < 0 || kj >= len(m) {
				lines = append(lines, i)
			}
		}
	}
	return lines
}

func (m matrix) part1(index int, debug bool) int {
	if cols := m.findVerticalMirror(); len(cols) > 0 {
		if debug {
			log.Printf("%d: vertical mirror found at %d\n", index, cols[0])
		}
		return cols[0] + 1
	}
	if rows := m.findHorizontalMirror(); len(rows) > 0 {
		if debug {
			log.Printf("%d: horizontal mirror found at %d\n", index, rows[0])
		}
		return (rows[0] + 1) * 100
	}
	return 0
}

func (m matrix) variate(fn func(matrix)) {
	data := make([]rune, len(m)*len(m[0]))

	o := make(matrix, 0, len(m))
	for p := data; len(p) > 0; p = p[len(m[0]):] {
		o = append(o, p[:len(m[0])])
	}

	for i := range m {
		copy(o[i], m[i])
	}

	for i, old := range data {
		switch old {
		case '.':
			data[i] = '#'
		case '#':
			data[i] = '.'
		}
		fn(o)
		data[i] = old
	}
}

func other(a, b []int) (int, bool) {
	for _, x := range a {
		for _, y := range b {
			if x != y {
				return y, true
			}
		}
	}
	return 0, false
}

func (m matrix) part2(_ int, _ bool) int {
	var (
		ocols = m.findVerticalMirror()
		orows = m.findHorizontalMirror()
		col   = -1
		row   = -1
	)

	m.variate(func(o matrix) {
		if nrows := o.findHorizontalMirror(); len(nrows) != 0 {
			if len(orows) == 0 {
				row = nrows[0]
			} else {
				if y, ok := other(orows, nrows); ok {
					row = y
				}
			}
		}
		if ncols := o.findVerticalMirror(); len(ncols) > 1 {
			if len(ocols) == 0 {
				col = ncols[0]
			} else {
				if x, ok := other(ocols, ncols); ok {
					col = x
				}
			}
		}
	})

	if col != -1 {
		return col + 1
	}
	if row != -1 {
		return (row + 1) * 100
	}
	return 0
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	var (
		pattern matrix
		sum     = 0
		index   = 0
	)

	var findMirror = matrix.part1
	if *second {
		findMirror = matrix.part2
	}

	process := func() {
		if len(pattern) > 0 {
			if len(pattern)%2 == 0 || len(pattern[0])%2 == 0 {
				log.Printf("pattern %d has even side length.\n", index)
			}
			sum += findMirror(pattern, index, *debug)
			index++
			pattern = nil
		}
	}

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		if m := patternRe.FindStringSubmatch(line); m != nil {
			pattern = append(pattern, []rune(m[1]))
			continue
		}
		if line == "" {
			process()
			continue
		}
		log.Printf("unmatched line %d: %q\n", lineNo, line)
	}
	check(sc.Err())
	process()
	fmt.Println(sum)
}
