package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

type point struct {
	x int
	y int
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "print debug output")
	flag.Parse()

	var (
		ysteps   = make([]uint64, 0, 140)
		xsteps   = make([]uint64, 0, 140)
		galaxies = make([]point, 0, 500)
		scale    uint64
		sc       = bufio.NewScanner(os.Stdin)
	)
	if *second {
		scale = 1_000_000
	} else {
		scale = 2
	}
	for y := 0; sc.Scan(); y++ {
		yscale := scale
		for x, s := range sc.Bytes() {
			if len(xsteps) <= x {
				xsteps = append(xsteps, scale)
			}
			if s == '#' {
				galaxies = append(galaxies, point{x, y})
				xsteps[x] = 1
				yscale = 1
			}
		}
		ysteps = append(ysteps, yscale)
	}
	check(sc.Err())
	if *debug {
		log.Printf("number galaxies: %d\n", len(galaxies))
	}

	start := time.Now()

	total := uint64(0)
	for i := range galaxies {
		for j := i + 1; j < len(galaxies); j++ {
			p0, p1 := galaxies[i], galaxies[j]
			p0.x, p1.x = min(p0.x, p1.x), max(p0.x, p1.x)
			p0.y, p1.y = min(p0.y, p1.y), max(p0.y, p1.y)
		out:
			for {
				switch {
				case p0.x < p1.x:
					total += xsteps[p0.x]
					p0.x++
				case p0.y < p1.y:
					total += ysteps[p0.y]
					p0.y++
				default:
					break out
				}
			}
		}
	}
	if *debug {
		log.Printf("calculation took: %s\n", time.Since(start))
	}
	fmt.Printf("sum of length: %d\n", total)
}
