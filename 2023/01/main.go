package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

func reverse(s string) string {
	rs := []rune(s)
	slices.Reverse(rs)
	return string(rs)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	values := map[string]int{}

	var firstRe, secondRe *regexp.Regexp
	if !*second {
		firstRe = regexp.MustCompile(`[1-9]`)
		secondRe = firstRe
		for i := 1; i <= 9; i++ {
			values[strconv.Itoa(i)] = i
		}
	} else {
		var first, second []string
		for i, digit := range []string{
			"one", "two", "three", "four", "five",
			"six", "seven", "eight", "nine",
		} {
			first = append(first, fmt.Sprintf("(%d|%s)", i+1, digit))
			second = append(second, fmt.Sprintf("(%d|%s)", i+1, reverse(digit)))
			values[digit] = i + 1
			values[reverse(digit)] = i + 1
			values[strconv.Itoa(i+1)] = i + 1
		}
		firstRe = regexp.MustCompile(strings.Join(first, "|"))
		secondRe = regexp.MustCompile(strings.Join(second, "|"))
	}

	sum := 0
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		first := firstRe.FindString(line)
		second := secondRe.FindString(reverse(line))
		num := values[first]*10 + values[second]
		if *debug {
			log.Printf("%s %d\n", line, num)
		}
		sum += num
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
	fmt.Println(sum)
}
