package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
)

var (
	seedRe  = regexp.MustCompile(`^seeds:\s*(.+)$`)
	mapRe   = regexp.MustCompile(`^([a-z]+)-to-([a-z]+) map:$`)
	rangeRe = regexp.MustCompile(`^(\d+) (\d+) (\d+)$`)
)

type ranging struct {
	src uint64
	end uint64
	dst uint64
}

func (r *ranging) convert(x uint64) (uint64, bool) {
	if r.src <= x && x <= r.end {
		return x - r.src + r.dst, true
	}
	return x, false
}

type mapping []*ranging

func (m mapping) convert(x uint64) uint64 {
	for i, r := range m {
		if y, ok := r.convert(x); ok {
			// Sort to front
			m[0], m[i] = r, m[0]
			return y
		}
	}
	return x
}

type chain []mapping

func (c chain) convert(x uint64) uint64 {
	for _, m := range c {
		x = m.convert(x)
	}
	return x
}

// clone for each go routine is needed.
func (c chain) clone() chain {
	n := make(chain, len(c))
	for i, m := range c {
		nm := make(mapping, len(m))
		copy(nm, m)
		n[i] = nm
	}
	return n
}

func main() {
	numWorkers := flag.Int("workers", runtime.NumCPU(), "number of workers")
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var remap chain
	var seeds []uint64

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()

		if m := seedRe.FindStringSubmatch(line); len(m) > 0 {
			for _, s := range strings.Fields(m[1]) {
				seed, err := strconv.ParseUint(s, 10, 64)
				if err != nil {
					log.Fatalf("error: %v\n", err)
				}
				seeds = append(seeds, seed)
			}
			//log.Printf("seeds found: %v\n", seeds)
			continue
		}
		if m := mapRe.FindStringSubmatch(line); len(m) > 0 {
			remap = append(remap, mapping{})
			//log.Printf("mapping found: %q -> %q\n", m[1], m[2])
			continue
		}
		if m := rangeRe.FindStringSubmatch(line); len(m) > 0 {
			n := len(remap)
			if n == 0 {
				log.Fatalln("no mapping found")
			}
			var (
				dst, _   = strconv.ParseUint(m[1], 10, 64)
				src, _   = strconv.ParseUint(m[2], 10, 64)
				width, _ = strconv.ParseUint(m[3], 10, 64)
				r        = &ranging{
					src: src,
					end: src + width,
					dst: dst,
				}
			)
			remap[n-1] = append(remap[n-1], r)
		}
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	minLoc := uint64(math.MaxUint64)

	for _, seed := range seeds {
		location := remap.convert(seed)
		minLoc = min(minLoc, location)
		//log.Printf("location: %d\n", location)
	}

	if !*second {
		fmt.Printf("minimum: %d\n", minLoc)
		return
	}

	var (
		wg     sync.WaitGroup
		ranges = make(chan [2]uint64)
		mins   = make(chan uint64)
		done   = make(chan struct{})
	)

	worker := func(remap chain) {
		defer wg.Done()
		minL := uint64(math.MaxUint64)
		for r := range ranges {
			//log.Printf("range %d - %d\n", r[0], r[1])
			for x := r[0]; x <= r[1]; x++ {
				minL = min(minL, remap.convert(x))
			}
		}
		mins <- minL
	}

	for i, n := 0, max(1, *numWorkers); i < n; i++ {
		wg.Add(1)
		go worker(remap.clone())
	}

	minLoc = uint64(math.MaxUint64)
	go func() {
		defer close(done)
		for m := range mins {
			minLoc = min(minLoc, m)
		}
	}()

	const partitionSize = 1_000_000

	for sr := seeds; len(sr) >= 2; sr = sr[2:] {
		for p, rest := sr[0], sr[1]; rest > 0; {
			n := min(rest, partitionSize)
			ranges <- [2]uint64{p, p + n}
			rest -= n
			p += n
		}
	}
	close(ranges)
	wg.Wait()
	close(mins)
	<-done
	fmt.Printf("range minimum: %d\n", minLoc)
}
