package main

import (
	"bufio"
	"container/heap"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

type grid [][]int

type vec struct {
	x int
	y int
}

type coord struct {
	pos   vec
	dir   vec
	steps int
}

type state struct {
	heat int
	coord
}

type queue []*state

func (c coord) less(o coord) bool {
	if c.pos.x != o.pos.x {
		return c.pos.x < o.pos.x
	}
	if c.pos.y != o.pos.y {
		return c.pos.y < o.pos.y
	}
	if c.dir.x != o.dir.x {
		return c.dir.x < o.dir.x
	}
	if c.dir.y != o.dir.y {
		return c.dir.y < o.dir.y
	}
	return c.steps < o.steps
}

func (q queue) Less(i, j int) bool {
	qi, qj := q[i], q[j]
	if qi.heat != qj.heat {
		return qi.heat < qj.heat
	}
	// return qi.coord.less(qj.coord)
	return qi.coord.less(qj.coord)
}

func (q queue) Len() int      { return len(q) }
func (q queue) Swap(i, j int) { q[i], q[j] = q[j], q[i] }
func (q *queue) Push(p any)   { *q = append(*q, p.(*state)) }

func (q *queue) Pop() any {
	n := len(*q) - 1
	it := (*q)[n]
	(*q)[n] = nil
	*q = (*q)[:n]
	return it
}

func (g grid) valid(v vec) bool {
	return v.y >= 0 && v.y < len(g) &&
		v.x >= 0 && v.x < len(g[v.y])
}

func (v vec) add(o vec) vec { return vec{v.x + o.x, v.y + o.y} }

func (g grid) solve(minSteps, maxSteps int) int {
	visted := make(map[coord]struct{})
	q := &queue{{
		heat: g[0][1],
		coord: coord{
			pos: vec{1, 0},
			dir: vec{1, 0},
		},
	}, {
		heat: g[1][0],
		coord: coord{
			pos: vec{0, 1},
			dir: vec{0, 1},
		},
	}}
	goal := vec{len(g[0]) - 1, len(g) - 1}
	for q.Len() > 0 {
		st := heap.Pop(q).(*state)
		if st.pos == goal && minSteps <= st.steps {
			return st.heat
		}
		if _, ok := visted[st.coord]; ok {
			continue
		}
		visted[st.coord] = struct{}{}

		if npos := st.pos.add(st.dir); g.valid(npos) && st.steps < maxSteps-1 {
			heap.Push(q, &state{
				heat: st.heat + g[npos.y][npos.x],
				coord: coord{
					pos:   npos,
					dir:   st.dir,
					steps: st.steps + 1,
				}})
		}
		if minSteps <= st.steps {
			ldir := vec{st.dir.y, -st.dir.x}
			rdir := vec{-st.dir.y, st.dir.x}
			for _, dir := range []vec{ldir, rdir} {
				if pos := st.pos.add(dir); g.valid(pos) {
					heap.Push(q, &state{
						heat: st.heat + g[pos.y][pos.x],
						coord: coord{
							pos: pos,
							dir: dir,
						}})
				}
			}
		}
	}
	return 0
}

var inputRe = regexp.MustCompile(`^(\d+)$`)

func asInt(line []byte) []int {
	out := make([]int, len(line))
	for i := range line {
		out[i] = int(line[i] - '0')
	}
	return out
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	var raster grid
	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		if !inputRe.Match(line) {
			log.Printf("umatched line %d: %q\n", lineNo, line)
			continue
		}
		if len(raster) > 0 && len(line) != len(raster[0]) {
			log.Printf("length of line %d does not match.\n", lineNo)
		}
		raster = append(raster, asInt(line))
	}
	if err := sc.Err(); err != nil {
		log.Fatalln(err)
	}
	if len(raster) == 0 {
		log.Fatalln("raster is empty")
	}

	if *second {
		fmt.Println(raster.solve(3, 10))
	} else {
		fmt.Println(raster.solve(0, 3))
	}
}
