package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"slices"
	"sync"
)

type vec3 struct {
	x int
	y int
	z int
}

type brick struct {
	min vec3
	max vec3
}

func (b *brick) inZ(a *brick) bool {
	return !(b.min.z > a.max.z || a.min.z > b.max.z)
}
func (b *brick) inXY(a *brick) bool {
	return !(b.min.x > a.max.x || a.min.x > b.max.x ||
		b.min.y > a.max.y || a.min.y > b.max.y)
}

func (b *brick) below(a *brick) bool {
	return b.max.z < a.min.z
}

func (b *brick) onTop(a *brick) bool {
	return b.min.z == a.max.z+1
}

func (b *brick) order() {
	b.min.x, b.max.x = min(b.min.x, b.max.x), max(b.min.x, b.max.x)
	b.min.y, b.max.y = min(b.min.y, b.max.y), max(b.min.y, b.max.y)
	b.min.z, b.max.z = min(b.min.z, b.max.z), max(b.min.z, b.max.z)
}

func buildXYs(bricks []*brick) [][]int {
	xys := make([][]int, len(bricks))

	for i, bi := range bricks {
		for j, bj := range bricks {
			if i != j && bj.inXY(bi) {
				if bj.inZ(bi) {
					panic("that should not happen")
				}
				xys[i] = append(xys[i], j)
			}
		}
	}
	return xys
}

func doFalling(bricks []*brick, xys [][]int) int {

	if xys == nil {
		xys = buildXYs(bricks)
	}

	fallen := make([]bool, len(bricks))

again:
	for i, bi := range bricks {
		//fmt.Printf("checking %c\n", 'A'+i)

		below := -1

		for _, j := range xys[i] {
			if bj := bricks[j]; bj.below(bi) {
				if below == -1 || bj.max.z > bricks[below].max.z {
					below = j
				}
			}
		}
		if below == -1 { // no one below
			if bi.min.z > 1 { // over ground -> land it
				dh := bi.max.z - bi.min.z
				bi.min.z = 1
				bi.max.z = dh + 1
				fallen[i] = true
				goto again
			}
		} else {
			if bricks[below].max.z+1 == bi.min.z { // stands already on other
				continue
			}
			// needs to fall
			dh := bi.max.z - bi.min.z
			bi.min.z = bricks[below].max.z + 1
			bi.max.z = bi.min.z + dh
			fallen[i] = true
			goto again
		}
	}
	count := 0
	for _, f := range fallen {
		if f {
			count++
		}
	}
	return count
}

func disintegrationCandidates(bricks []*brick, xys [][]int) []int {
	supported := make([]int, len(bricks))

	for i, a := range bricks {
		for _, j := range xys[i] {
			if a.onTop(bricks[j]) {
				supported[i]++
			}
		}
	}

	var candidates []int
next:
	for i, a := range bricks {
		for _, j := range xys[i] {
			if bricks[j].onTop(a) && supported[j] < 2 {
				continue next
			}
		}
		candidates = append(candidates, i)
	}
	return candidates
}

func simulateDisintegration(bricks []*brick, safe []int, workers int, debug bool) int {

	var (
		sumCh    = make(chan int)
		done     = make(chan struct{})
		removeCh = make(chan int)
		wg       sync.WaitGroup
		sum      = 0
	)

	go func() {
		defer close(done)
		for s := range sumCh {
			sum += s
		}
	}()

	worker := func() {
		defer wg.Done()
		for rm := range removeCh {
			clone := make([]*brick, 0, len(bricks)-1)
			for i, b := range bricks {
				if i != rm {
					bclone := *b
					clone = append(clone, &bclone)
				}
			}
			fallen := doFalling(clone, nil)
			if debug {
				log.Printf("removed %d -> %d fallen\n", rm, fallen)
			}
			sumCh <- fallen
		}
	}

	for i := 0; i < workers; i++ {
		wg.Add(1)
		go worker()
	}

	slices.Sort(safe)
	for i := range bricks {
		if _, found := slices.BinarySearch(safe, i); !found {
			removeCh <- i
		}
	}
	close(removeCh)
	wg.Wait()
	close(sumCh)
	<-done

	return sum
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	workers := flag.Int("workers", runtime.NumCPU(), "number of sim workers")
	flag.Parse()

	var bricks []*brick

	sc := bufio.NewScanner(os.Stdin)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		var b brick
		c0, c1 := &b.min, &b.max
		_, err := fmt.Sscanf(line,
			"%d,%d,%d~%d,%d,%d",
			&c0.x, &c0.y, &c0.z,
			&c1.x, &c1.y, &c1.z)
		if err != nil {
			log.Printf("invalid line %d: %q\n", lineNo, err)
			continue
		}
		b.order()
		bricks = append(bricks, &b)
	}
	check(sc.Err())
	if *debug {
		log.Printf("number bricks: %d\n", len(bricks))
	}

	xys := buildXYs(bricks)

	doFalling(bricks, xys)

	candidates := disintegrationCandidates(bricks, xys)

	if *second {
		fmt.Printf("sum of other fallen: %d\n",
			simulateDisintegration(bricks, candidates, *workers, *debug))
	} else {
		fmt.Printf("safe to disintegrate: %d\n", len(candidates))
	}
}
