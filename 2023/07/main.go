package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
)

var cardsRe = regexp.MustCompile(`^([AKQJT2-9]+)\s+(\d+)$`)

const (
	cardsPart1 = "AKQJT98765432"
	cardsPart2 = "AKQT98765432J"
)

var cardValues = buildCardValues(cardsPart1)

func buildCardValues(cards string) map[rune]int {
	values := map[rune]int{}
	for i, r := range cards {
		values[r] = len(cards) - i
	}
	return values
}

type hand string

type typ int

const (
	unknown typ = iota
	highCard
	onePair
	twoPair
	threeOfAKind
	fullHouse
	fourOfAKind
	fiveOfAKind
)

func (t typ) String() string {
	switch t {
	case fiveOfAKind:
		return "Five of a kind"
	case fourOfAKind:
		return "Four of a kind"
	case fullHouse:
		return "Full house"
	case threeOfAKind:
		return "Three of a kind"
	case twoPair:
		return "Two pair"
	case onePair:
		return "One pair"
	case highCard:
		return "High card"
	default:
		return fmt.Sprintf("Unknown %d", t)
	}
}

func count(h hand) map[rune]int {
	cs := make(map[rune]int, 5)
	for _, r := range h {
		cs[r]++
	}
	return cs
}

func (h hand) nOfAKind(n int) bool {
	cs := count(h)
	for _, count := range cs {
		if count >= n {
			return true
		}
	}
	return false
}

func (h hand) fullHouse() bool {
	cs := count(h)
	var three, two bool
	for _, v := range cs {
		switch {
		case v >= 3:
			three = true
		case v >= 2:
			two = true
		}
	}
	return three && two
}

func (h hand) nPairs(n int) bool {
	cs := count(h)
	pairs := 0
	for _, v := range cs {
		if v >= 2 {
			pairs++
		}
	}
	return pairs >= n
}

func (h hand) fiveOfAKind() bool  { return h.nOfAKind(5) }
func (h hand) fourOfAKind() bool  { return h.nOfAKind(4) }
func (h hand) threeOfAKind() bool { return h.nOfAKind(3) }
func (h hand) twoPair() bool      { return h.nPairs(2) }
func (h hand) onePair() bool      { return h.nPairs(1) }
func (h hand) highCard() bool     { return len(count(h)) >= 5 }

var allHands = allHandsPart1

func allHandsPart1(h hand, fn func(hand)) { fn(h) }

func allHandsPart2(h hand, fn func(hand)) {
	var recurse func(int, hand)
	recurse = func(pos int, h hand) {
		for {
			if pos >= len(h) {
				fn(h)
				return
			}
			if h[pos] == 'J' {
				runes := []rune(h)
				for _, r := range cardsPart2 {
					var orig rune
					orig, runes[pos] = runes[pos], r
					nh := hand(runes)
					runes[pos] = orig
					recurse(pos+1, nh)
				}
				break
			}
			pos++
		}
	}
	recurse(0, h)
}

var typTests = []func(hand) bool{
	unknown:      nil,
	highCard:     hand.highCard,
	onePair:      hand.onePair,
	twoPair:      hand.twoPair,
	threeOfAKind: hand.threeOfAKind,
	fullHouse:    hand.fullHouse,
	fourOfAKind:  hand.fourOfAKind,
	fiveOfAKind:  hand.fiveOfAKind,
}

var typCache = map[hand]typ{}

func (h hand) typ() typ {
	if b, ok := typCache[h]; ok {
		return b
	}
	best := unknown
	allHands(h, func(h hand) {
		for cand := fiveOfAKind; cand > best; cand-- {
			if typTests[cand](h) {
				best = cand
				return
			}
		}
	})
	typCache[h] = best
	return best
}

func (h hand) cmp(b hand) int {
	if d := len(h) - len(b); d != 0 {
		return d
	}
	for i := 0; i < len(h); i++ {
		ch := cardValues[rune(h[i])]
		cb := cardValues[rune(b[i])]
		if d := ch - cb; d != 0 {
			return d
		}
	}
	return 0
}

func (h hand) order(b hand) int {
	if d := h.typ() - b.typ(); d != 0 {
		return int(d)
	}
	return h.cmp(b)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	if *second {
		cardValues = buildCardValues(cardsPart2)
		allHands = allHandsPart2
	}

	type bid struct {
		hand   hand
		amount uint64
	}

	var bids []*bid

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		m := cardsRe.FindStringSubmatch(line)
		if len(m) == 0 {
			log.Printf("unmatched cards %q\n", line)
			continue
		}
		cards := hand(m[1])
		//log.Printf("%s -> %s\n", cards, cards.typ())
		amount, err := strconv.ParseUint(m[2], 10, 64)
		if err != nil {
			log.Fatalln(err)
		}
		bids = append(bids, &bid{
			hand:   cards,
			amount: amount,
		})
	}
	if err := sc.Err(); err != nil {
		log.Fatalln(err)
	}

	slices.SortFunc(bids, func(a, b *bid) int {
		return a.hand.order(b.hand)
	})

	var totalWinning uint64

	for i, bid := range bids {
		totalWinning += (uint64(i) + 1) * bid.amount
	}

	fmt.Printf("total winning: %d\n", totalWinning)
}
