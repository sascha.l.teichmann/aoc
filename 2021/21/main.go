package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

var playerRe = regexp.MustCompile(`Player (1|2) starting position: (\d{1,2})`)

func parse(r io.Reader) ([2]int, error) {
	var players [2]int
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		if m := playerRe.FindStringSubmatch(sc.Text()); m != nil {
			player, _ := strconv.Atoi(m[1])
			pos, _ := strconv.Atoi(m[2])
			players[player-1] = pos
		}
	}
	if err := sc.Err(); err != nil {
		return [2]int{}, err
	}

	return players, nil
}

func runDeterministic(players [2]int, debug bool) {
	if debug {
		log.Printf("Player 1 starting position: %d\n", players[0])
		log.Printf("Player 2 starting position: %d\n", players[1])
	}

	eyes := 1
	dice := func() int {
		e := eyes
		if eyes++; eyes > 100 {
			eyes = 1
		}
		return e
	}

	var scores [2]int

	player := 0
	rolled := 0

	for scores[0] < 1000 && scores[1] < 1000 {
		a, b, c := dice(), dice(), dice()
		points := a + b + c
		rolled += 3

		players[player] = (players[player]-1+points)%10 + 1
		scores[player] += players[player]

		if debug {
			log.Printf(
				"Player %d rolls %d+%d+%d and moves to space %d for a total score at %d.\n",
				player+1, a, b, c, players[player], scores[player])
		}

		player = (player + 1) % 2
	}

	if debug {
		log.Printf("Score player 1: %d\n", scores[0])
		log.Printf("Score player 2: %d\n", scores[1])
	}
	var losing int
	if scores[0] < scores[1] {
		losing = scores[0]
	} else {
		losing = scores[1]
	}
	fmt.Printf("Rolled %d * %d = %d\n", rolled, losing, rolled*losing)
}

type packedUniverse uint64

type universe struct {
	player    byte
	scores    [2]byte
	positions [2]byte
	freq      uint64
}

type diceFreq struct {
	sum  byte
	freq byte
}

func (u universe) pack() packedUniverse {
	return packedUniverse(u.player) |
		packedUniverse(u.scores[0])<<1 |
		packedUniverse(u.scores[1])<<(1+5) |
		packedUniverse(u.positions[0])<<(1+5+5) |
		packedUniverse(u.positions[1])<<(1+5+5+4) |
		packedUniverse(u.freq)<<(1+5+5+4+4)
}

func (pu packedUniverse) unpack() universe {
	return universe{
		player: byte(pu & 0b1),
		scores: [2]byte{
			byte((pu >> 1) & 0b11111),
			byte((pu >> (1 + 5)) & 0b11111),
		},
		positions: [2]byte{
			byte((pu >> (1 + 5 + 5)) & 0b1111),
			byte((pu >> (1 + 5 + 5 + 4)) & 0b1111),
		},
		freq: uint64(pu >> (1 + 5 + 5 + 4 + 4)),
	}
}

var diracFreq = func() []diceFreq {
	var freqs []diceFreq
	for a := byte(1); a <= 3; a++ {
		for b := byte(1); b <= 3; b++ {
		loop:
			for c := byte(1); c <= 3; c++ {
				sum := a + b + c
				for i := range freqs {
					if freqs[i].sum == sum {
						freqs[i].freq++
						continue loop
					}
				}
				freqs = append(freqs, diceFreq{sum, 1})
			}
		}
	}
	return freqs
}()

func runDirac(players [2]int, _ bool) {

	var wins [2]uint64

	stack := []packedUniverse{
		universe{
			positions: [2]byte{
				byte(players[0]),
				byte(players[1]),
			},
			freq: 1,
		}.pack(),
	}

	for len(stack) > 0 {
		u := stack[len(stack)-1].unpack()
		stack = stack[:len(stack)-1]

		for _, df := range diracFreq {
			pos := (u.positions[u.player]-1+df.sum)%10 + 1
			score := u.scores[u.player] + pos
			if score >= 21 {
				wins[u.player] += uint64(df.freq) * uint64(u.freq)
				continue
			}
			v := u
			v.positions[v.player] = pos
			v.scores[v.player] = score
			v.player = (v.player + 1) % 2
			v.freq *= uint64(df.freq)
			stack = append(stack, v.pack())
		}
	}

	switch {
	case wins[0] > wins[1]:
		fmt.Printf("Player 1 wins %d universes.\n", wins[0])
	case wins[1] > wins[0]:
		fmt.Printf("Player 2 wins %d universes.\n", wins[1])
	default:
		fmt.Printf("Both players wins %d universes.\n", wins[0])
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()
	players, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		runDirac(players, *debug)
	} else {
		runDeterministic(players, *debug)
	}
}
