package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
)

type dim struct {
	min, max int
}

type flip struct {
	on byte
	p  [3]dim
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func (f *flip) offLimit(limit int) bool {
	for _, d := range f.p {
		if abs(d.min) > limit || abs(d.max) > limit {
			return true
		}
	}
	return false
}

func parse(r io.Reader, limit *int) ([]*flip, error) {
	var prog []*flip

	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		f := new(flip)
		var on string
		if _, err := fmt.Sscanf(line, "%s x=%d..%d,y=%d..%d,z=%d..%d",
			&on,
			&f.p[0].min, &f.p[0].max,
			&f.p[1].min, &f.p[1].max,
			&f.p[2].min, &f.p[2].max,
		); err != nil {
			return nil, err
		}
		if limit != nil && f.offLimit(*limit) {
			continue
		}
		if on == "on" {
			f.on = 1
		}
		f.p[0].max++
		f.p[1].max++
		f.p[2].max++
		prog = append(prog, f)
	}

	return prog, sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

type cube struct {
	w, h, d int
	data    []byte
}

func newCube(w, h, d int) *cube {
	return &cube{
		w:    w,
		h:    h,
		d:    d,
		data: make([]byte, w*h*d),
	}
}

func (c *cube) xy(x, y int) []byte {
	pos := x*c.h*c.d + /* */ y*c.d
	return c.data[pos : pos+c.d]
}

func (c *cube) sum() int {
	sum := 0
	for _, v := range c.data {
		sum += int(v)
	}
	return sum
}

func minMax(prog []*flip) ([3]int, [3]int) {
	mins := [3]int{
		math.MaxInt32,
		math.MaxInt32,
		math.MaxInt32,
	}
	maxs := [3]int{
		math.MinInt32,
		math.MinInt32,
		math.MinInt32,
	}

	for _, f := range prog {
		mins[0] = min(mins[0], f.p[0].min)
		mins[1] = min(mins[1], f.p[1].min)
		mins[2] = min(mins[2], f.p[2].min)
		maxs[0] = max(maxs[0], f.p[0].max)
		maxs[1] = max(maxs[1], f.p[1].max)
		maxs[2] = max(maxs[2], f.p[2].max)
	}
	return mins, maxs
}

func minOffset(prog []*flip, mins [3]int) {
	for _, f := range prog {
		f.p[0] = dim{f.p[0].min - mins[0], f.p[0].max - mins[0]}
		f.p[1] = dim{f.p[1].min - mins[1], f.p[1].max - mins[1]}
		f.p[2] = dim{f.p[2].min - mins[2], f.p[2].max - mins[2]}
	}
}

func part1() {
	limit := 50
	prog, err := parse(os.Stdin, &limit)
	check(err)
	mins, maxs := minMax(prog)

	xd := maxs[0] - mins[0] + 1
	yd := maxs[1] - mins[1] + 1
	zd := maxs[2] - mins[2] + 1

	minOffset(prog, mins)
	c := newCube(xd, yd, zd)
	for _, f := range prog {
		for x := f.p[0].min; x < f.p[0].max; x++ {
			for y := f.p[1].min; y < f.p[1].max; y++ {
				xy := c.xy(x, y)
				for z := f.p[2].min; z < f.p[2].max; z++ {
					xy[z] = f.on
				}
			}
		}
	}
	total := c.sum()
	fmt.Println(total)
}

func uniqueAxis(prog []*flip) [3]int {
	uniq := [3]map[int]struct{}{{}, {}, {}}
	for _, f := range prog {
		for i, d := range f.p {
			uniq[i][d.min] = struct{}{}
			uniq[i][d.max] = struct{}{}
		}
	}
	return [3]int{
		len(uniq[0]) + 1,
		len(uniq[1]) + 1,
		len(uniq[2]) + 1,
	}
}

func part2() {
	prog, err := parse(os.Stdin, nil)
	check(err)
	mins, maxs := minMax(prog)

	minOffset(prog, mins)

	xd := maxs[0] - mins[0]
	yd := maxs[1] - mins[1]
	zd := maxs[2] - mins[2]

	remap := [3][]int{
		make([]int, xd+1),
		make([]int, yd+1),
		make([]int, zd+1),
	}

	uniq := uniqueAxis(prog)
	width := [3][]int{
		make([]int, uniq[0]),
		make([]int, uniq[1]),
		make([]int, uniq[2]),
	}

	for _, f := range prog {
		for i, d := range f.p {
			remap[i][d.min] = 1
			remap[i][d.max] = 1
		}
	}

	for i, rm := range remap {
		t := 0
		for j, v := range rm {
			t += v
			rm[j] = t
			width[i][t]++
		}
	}

	for _, f := range prog {
		for i, d := range f.p {
			f.p[i] = dim{remap[i][d.min], remap[i][d.max]}
		}
	}

	c := newCube(uniq[0], uniq[1], uniq[2])
	for _, f := range prog {
		for x := f.p[0].min; x < f.p[0].max; x++ {
			for y := f.p[1].min; y < f.p[1].max; y++ {
				xy := c.xy(x, y)
				for z := f.p[2].min; z < f.p[2].max; z++ {
					xy[z] = f.on
				}
			}
		}
	}

	total := 0

	var (
		w, h, d    = width[0], width[1], width[2]
		ux, uy, uz = uniq[0], uniq[1], uniq[2]
		_          = d[uz-1]
	)
	_ = w[ux-1]
	for x := 0; x < ux; x++ {
		wx := w[x]
		_ = h[uy-1]
		for y := 0; y < uy; y++ {
			wxy := wx * h[y]
			xy := c.xy(x, y)
			_ = xy[uz-1]
			for z := 0; z < uz; z++ {
				total += wxy * d[z] * int(xy[z])
			}
		}
	}

	fmt.Println(total)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
