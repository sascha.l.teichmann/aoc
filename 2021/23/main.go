package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"regexp"
)

/*
00 01 02 03 04 05 06 07 08 09 10
      11    15    19    23
      12    16    20    24
      13    17    21    25
      14    18    22    26
*/

type world struct {
	depth byte
	paths [][][]byte
}

type state struct {
	rooms []byte
	costs int
}

const empty = 0xff

var linesRe = []*regexp.Regexp{
	regexp.MustCompile(`^###(A|B|C|D)#(A|B|C|D)#(A|B|C|D)#(A|B|C|D)###`),
	regexp.MustCompile(`^  #(A|B|C|D)#(A|B|C|D)#(A|B|C|D)#(A|B|C|D)#`),
}

var costs = [4]int{
	1, 10, 100, 1000,
}

var validFloor = [...]byte{
	0, 1, 3, 5, 7, 9, 10,
}

func newState(depth byte) *state {
	rooms := make([]byte, 11+4*depth)
	for i := range rooms {
		rooms[i] = empty
	}
	return &state{rooms: rooms}
}

func newWorld(depth byte) *world {

	paths := make([][][]byte, 11+4*depth)

	for i := range paths {
		paths[i] = make([][]byte, 11+4*depth)
	}

	for i, p := range paths {
		for j := range p {
			p[j] = path(byte(i), byte(j), depth)
		}
	}

	return &world{
		depth: depth,
		paths: paths,
	}
}

func neighbours(idx, depth byte) []byte {

	if idx < 11 {
		switch idx {
		case 0:
			return []byte{1}
		case 10:
			return []byte{9}
		}
		if idx%2 == 1 {
			return []byte{idx - 1, idx + 1}
		}
		return []byte{idx - 1, idx + 1, 11 + (idx/2-1)*depth}
	}

	p := idx - 11
	g, s := p/depth, p%depth

	switch {
	case s == 0:
		return []byte{(g + 1) * 2, idx + 1}
	case s == depth-1:
		return []byte{idx - 1}
	default:
		return []byte{idx - 1, idx + 1}
	}
}

func path(a, b, depth byte) []byte {

	if a == b {
		return nil
	}

	var out []byte

	var rec func(uint32, byte) bool

	rec = func(v uint32, p byte) bool {
		if p == b {
			return true
		}
		v |= 1 << p
		for _, n := range neighbours(p, depth) {
			if (v&(1<<n)) == 0 && rec(v, n) {
				out = append([]byte{p}, out...)
				return true
			}
		}
		return false
	}

	v := uint32(1) << a
	for _, n := range neighbours(a, depth) {
		if rec(v, n) {
			break
		}
	}

	return out
}

func (w *world) solved(s *state) bool {
	rooms := s.rooms[11:]
	for i := 0; len(rooms) > 0; rooms, i = rooms[w.depth:], i+1 {
		for _, r := range rooms[:w.depth] {
			if r != byte(i) {
				return false
			}
		}
	}
	return true
}

func (s state) clone() *state {
	rooms := make([]byte, len(s.rooms))
	copy(rooms, s.rooms)
	return &state{rooms: rooms, costs: s.costs}
}

func (s *state) free(path []byte) bool {
	for _, p := range path {
		if s.rooms[p] != empty {
			return false
		}
	}
	return true
}

func (w *world) next(s *state) []*state {
	var succs []*state

	move := func(from, to byte, cost int) {
		ns := s.clone()
		rooms := ns.rooms
		rooms[from], rooms[to] = rooms[to], rooms[from]
		ns.costs += cost * costs[rooms[to]]
		succs = append(succs, ns)
	}

	leave := func(idx byte) {
		for _, f := range validFloor {
			if s.rooms[f] != empty {
				continue
			}
			if p := w.paths[idx][f]; s.free(p) {
				move(idx, f, len(p)+1)
			}
		}
	}

	// Move out of garages
	for i := byte(0); i < 4; i++ {
		gidx := byte(11) + i*w.depth
		curr := s.rooms[gidx : gidx+w.depth]
		for j, v := range curr {
			if v == empty {
				continue
			}
			if v != i {
				leave(gidx + byte(j))
				continue
			}
			for _, b := range curr[j+1:] {
				if b != i {
					leave(gidx + byte(j))
					break
				}
			}
		}
	}

	// Move into garages
nextFloor:
	for _, f := range validFloor {
		v := s.rooms[f]
		if v == empty {
			continue
		}
		gidx := 11 + w.depth*v
		p := w.paths[f][gidx]
		if !s.free(p) {
			continue
		}
		curr := s.rooms[gidx : gidx+w.depth]
		first := -1
		for i, w := range curr {
			if w != empty {
				if w == v {
					break
				}
				continue nextFloor
			}
			first = i
		}
		if first == -1 {
			continue
		}
		for _, w := range curr[first+1:] {
			if w != v {
				continue nextFloor
			}
		}
		cost := len(p) + 1 + first
		move(f, gidx+byte(first), cost)
	}

	return succs
}

func (w *world) solve(s *state, debug bool) int {

	best := math.MaxInt32
	open := []*state{s}

	for len(open) > 0 {
		l := len(open)
		s := open[l-1]
		open[l-1] = nil
		open = open[:l-1]
		if w.solved(s) {
			if s.costs < best {
				best = s.costs
				if debug {
					log.Printf("best so far: %d\n", best)
				}
			}
			continue
		}
		for _, n := range w.next(s) {
			if n.costs < best {
				open = append(open, n)
			}
		}
	}
	return best
}

func parse(r io.Reader, garage byte) (*state, error) {
	sc := bufio.NewScanner(r)

	vals := map[byte]byte{
		'A': 0,
		'B': 1,
		'C': 2,
		'D': 3,
	}

	s := newState(garage)

	if garage == 4 {
		rooms := s.rooms[11:]
		rooms[1] = vals['D']
		rooms[2] = vals['D']

		rooms = rooms[garage:]
		rooms[1] = vals['C']
		rooms[2] = vals['B']

		rooms = rooms[garage:]
		rooms[1] = vals['B']
		rooms[2] = vals['A']

		rooms = rooms[garage:]
		rooms[1] = vals['A']
		rooms[2] = vals['C']
	}

	for sc.Scan() {
		line := sc.Text()
		for i, lr := range linesRe {
			m := lr.FindStringSubmatch(line)
			if m == nil {
				continue
			}
			if i == 1 && garage == 4 {
				i += 2
			}
			for j := byte(0); j < 4; j++ {
				s.rooms[11+j*garage+byte(i)] = vals[m[1+j][0]]
			}
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return s, nil
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	garage := byte(2)
	if *second {
		garage = 4
	}

	s, err := parse(os.Stdin, garage)
	if err != nil {
		log.Printf("error: %v\n", err)
	}
	w := newWorld(garage)

	fmt.Printf("cheapest: %d\n", w.solve(s, *debug))
}
