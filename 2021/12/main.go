package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"maps"
	"os"
	"regexp"
	"slices"
	"strings"
)

type (
	cave struct {
		name   string
		big    bool
		twice  bool
		neighs []*cave
	}
	path   []*cave
	visits map[*cave]int
	caves  map[string]*cave
)

var linkRe = regexp.MustCompile(`([a-zA-Z]+)-([a-zA-Z]+)`)

func parse(r io.Reader) (caves, error) {
	caves := caves{}

	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		m := linkRe.FindStringSubmatch(line)
		if m == nil {
			continue
		}
		from, to := m[1], m[2]
		c1 := caves[from]
		if c1 == nil {
			c1 = &cave{
				name: from,
				big:  strings.ToUpper(from) == from,
			}
			caves[from] = c1
		}
		c2 := caves[to]
		if c2 == nil {
			c2 = &cave{
				name: to,
				big:  strings.ToUpper(to) == to,
			}
			caves[to] = c2
		}
		c1.connect(c2)
		c2.connect(c1)
	}
	return caves, sc.Err()
}

func (c *cave) connect(o *cave) {
	for _, x := range c.neighs {
		if x == o {
			return
		}
	}
	c.neighs = append(c.neighs, o)
}

func (p path) String() string {
	var sb strings.Builder
	for i, c := range p {
		if i > 0 {
			sb.WriteByte(',')
		}
		sb.WriteString(c.name)
	}
	return sb.String()
}

func numPathsPart1(caves map[string]*cave) int {
	num := 0
	start, end := caves["start"], caves["end"]

	var recurse func(*cave, map[*cave]int)

	recurse = func(c *cave, visits map[*cave]int) {
		if c == end {
			num++
			return
		}
		for _, n := range c.neighs {
			v := maps.Clone(visits)
			v[c]++
			if n.big || v[n] < 1 {
				recurse(n, v)
			}
		}
	}

	for _, n := range start.neighs {
		recurse(n, map[*cave]int{start: 1})
	}

	return num
}

func numPathsPart2(caves map[string]*cave) int {
	paths := make(map[string]struct{})
	genPath(caves, func(p path) {
		paths[p.String()] = struct{}{}
	})
	return len(paths)
}

func genPath(caves map[string]*cave, fn func(path)) {
	start, end := caves["start"], caves["end"]
	var twice []*cave

	for _, c := range caves {
		if c != start && c != end && !c.big {
			twice = append(twice, c)
		}
	}

	var recurse func(*cave, path, visits)

	recurse = func(c *cave, p path, v visits) {
		if c == end {
			fn(append(slices.Clone(p), end))
			return
		}
		v = maps.Clone(v)
		v[c]++
		for _, n := range c.neighs {
			if n != start && (n.big || (n.twice && v[n] < 2) || v[n] < 1) {
				recurse(n, append(slices.Clone(p), c), v)
			}
		}
	}

	for _, t := range twice {
		t.twice = true
		v := visits{}
		p := path{start}
		for _, n := range start.neighs {
			recurse(n, p, v)
		}
		t.twice = false
	}
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	caves, err := parse(os.Stdin)
	check(err)
	if caves["start"] == nil || caves["end"] == nil {
		log.Fatalln("missing start or end cave.")
	}

	if *second {
		fmt.Printf("Num paths: %d\n", numPathsPart2(caves))
	} else {
		fmt.Printf("Num paths: %d\n", numPathsPart1(caves))
	}
}
