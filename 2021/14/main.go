package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
	"strings"
)

func parse(r io.Reader) (string, map[string][]string, error) {
	sc := bufio.NewScanner(r)
	var template string
	replacements := map[string][]string{}
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if line == "" {
			continue
		}
		if template == "" {
			template = line
			continue
		}
		var pair, insert string
		if _, err := fmt.Sscanf(line, "%2s -> %1s", &pair, &insert); err != nil {
			return "", nil, err
		}
		replacements[pair] = []string{pair[:1] + insert, insert + pair[1:]}
	}
	return template, replacements, nil
}

func count(tmpl string, reps map[string][]string, n int) {

	occurrences := map[string]uint64{}
	for two := tmpl; len(two) >= 2; two = two[1:] {
		occurrences[two[:2]]++
	}

	for i := 0; i < n; i++ {
		noccurrences := map[string]uint64{}
		for k, v := range occurrences {
			rs := reps[k]
			for _, s := range rs {
				noccurrences[s] += v
			}
		}
		occurrences = noccurrences
	}

	result := map[string]uint64{}
	for k, v := range occurrences {
		s := k[:1]
		result[s] += v
	}

	if len(result) < 1 {
		log.Fatalln("result too short")
	}

	values := make([]uint64, 0, len(result))
	for _, v := range result {
		values = append(values, v)
	}

	lowest, highest := slices.Min(values), slices.Max(values)
	fmt.Println(highest - lowest + 1)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	tmpl, reps, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		count(tmpl, reps, 40)
	} else {
		count(tmpl, reps, 10)
	}
}
