package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

type coord [3]int

type scanner struct {
	num         int
	coords      []coord
	projections [][]coord
}

var (
	scannerRe = regexp.MustCompile(`--- scanner (\d+) ---`)
	coordRe   = regexp.MustCompile(`(-?\d+),(-?\d+),(-?\d+)`)
)

var projections = func() [][6]int {

	var sel = [6][3]int{
		{0, 1, 2}, // x, y, z
		{0, 2, 1}, // x, z, z
		{1, 0, 2}, // y, x, z
		{1, 2, 0}, // y, z, x
		{2, 0, 1}, // z, x, y
		{2, 1, 0}, // z, y, x
	}

	proj := make([][6]int, 0, 2*24) // 2*6 * 4
	for _, eyeDir := range []int{-1, +1} {
		for _, s := range sel {
			for _, yDir := range []int{-1, +1} {
				for _, zDir := range []int{-1, +1} {
					p := [6]int{
						s[0], eyeDir,
						s[1], yDir,
						s[2], zDir,
					}
					proj = append(proj, p)
				}
			}
		}
	}
	return proj
}()

func (s *scanner) buildProjections() {

	s.projections = make([][]coord, len(projections))

	for p, proj := range projections {
		out := make([]coord, len(s.coords))
		for i, in := range s.coords {
			out[i] = coord{
				in[proj[0]] * proj[1],
				in[proj[2]] * proj[3],
				in[proj[4]] * proj[5],
			}
		}
		s.projections[p] = out
	}
}

func parse(r io.Reader) ([]*scanner, error) {
	var scanners []*scanner

	var curr *scanner

	in := bufio.NewScanner(r)
	for in.Scan() {
		line := in.Text()
		if m := scannerRe.FindStringSubmatch(line); m != nil {
			num, _ := strconv.Atoi(m[1])
			curr = &scanner{num: num}
			scanners = append(scanners, curr)
			continue
		}
		if m := coordRe.FindStringSubmatch(line); m != nil && curr != nil {
			x, _ := strconv.Atoi(m[1])
			y, _ := strconv.Atoi(m[2])
			z, _ := strconv.Atoi(m[3])
			curr.coords = append(curr.coords, coord{x, y, z})
		}
	}
	if err := in.Err(); err != nil {
		return nil, err
	}

	for _, s := range scanners {
		s.buildProjections()
	}

	return scanners, nil
}

func (c coord) sub(o coord) coord {
	return coord{
		c[0] - o[0],
		c[1] - o[1],
		c[2] - o[2],
	}
}

func (c coord) add(o coord) coord {
	return coord{
		c[0] + o[0],
		c[1] + o[1],
		c[2] + o[2],
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func (c coord) manhattan(o coord) int {
	return abs(c[0]-o[0]) + abs(c[1]-o[1]) + abs(c[2]-o[2])
}

func (s *scanner) fuse(o *scanner, dry bool) *coord {

	hash := make(map[coord]bool, len(s.coords))
	for _, c := range s.coords {
		hash[c] = true
	}

	best := -1
	var bestDelta coord
	var bestProj int

	for _, sc := range s.coords {

		for p, op := range o.projections {

			for _, oc := range op {
				matches := 0
				d := sc.sub(oc)
				for _, op := range op {
					if hash[op.add(d)] {
						matches++
					}
				}
				if matches > best {
					best = matches
					bestDelta = d
					bestProj = p
				}
			}
		}
	}
	if best < 12 {
		return nil
	}

	if !dry {
		for _, p := range o.projections[bestProj] {
			o := p.add(bestDelta)
			if !hash[o] {
				hash[o] = true
				s.coords = append(s.coords, o)
			}
		}
	}

	return &bestDelta
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	scanners, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	n := len(scanners)

	if n < 2 {
		return
	}

	all := scanners[0]
	scanners = scanners[1:]

	positions := make([]coord, n)

	for {
		var later []*scanner
		var found bool

		for len(scanners) > 0 {
			cand := scanners[0]
			scanners = scanners[1:]
			pos := all.fuse(cand, false)
			if pos == nil {
				later = append(later, cand)
			} else {
				positions[cand.num] = *pos
				found = true
			}
		}
		if len(later) == 0 {
			break
		}
		if !found {
			log.Printf("remaining scanners: %d\n", len(later))
			break
		}
		scanners = later
	}

	if !*second {
		fmt.Printf("num beacons: %d\n", len(all.coords))
		return
	}
	/*
		dist := -1
		for i, pos := range positions {
			var d int
			if i > 0 {
				if d = pos.manhattan(positions[i-1]); d > dist {
					dist = d
				}
			}
		}
		fmt.Printf("Largest neighbour distance: %d\n", dist)
	*/

	dist := -1
	for i, p1 := range positions {
		for _, p2 := range positions[i+1:] {
			if d := p1.manhattan(p2); d > dist {
				dist = d
			}
		}
	}
	fmt.Printf("Largest manhattan distance: %d\n", dist)
}
