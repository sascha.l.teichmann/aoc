package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func parseInput(r io.Reader) ([]int, error) {
	sc := bufio.NewScanner(r)
	if !sc.Scan() {
		if err := sc.Err(); err != nil {
			return nil, err
		}
		return nil, errors.New("no input")
	}
	input := strings.Split(sc.Text(), ",")
	positions := make([]int, 0, len(input))
	for _, n := range input {
		pos, err := strconv.Atoi(n)
		if err != nil {
			return nil, err
		}
		positions = append(positions, pos)
	}
	return positions, nil
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func cost(x int) int {
	sum := 0
	extra := 1
	for ; x > 0; x-- {
		sum += extra
		extra++
	}
	return sum
}

func part1() {
	positions, err := parseInput(os.Stdin)
	check(err)
	fuel := math.MaxInt32
	sort.Ints(positions)
	for align := positions[0]; align <= positions[len(positions)-1]; align++ {
		sum := 0
		for _, p := range positions {
			sum += abs(align - p)
		}
		if sum < fuel {
			fuel = sum
		}
	}
	fmt.Printf("best: %d\n", fuel)
}

func part2() {
	positions, err := parseInput(os.Stdin)
	check(err)
	fuel := math.MaxInt32
	sort.Ints(positions)
	for align := positions[0]; align <= positions[len(positions)-1]; align++ {
		sum := 0
		for _, p := range positions {
			sum += cost(abs(p - align))
		}
		if sum < fuel {
			fuel = sum
		}
	}
	fmt.Printf("best: %d\n", fuel)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
