package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

var bits = map[byte]string{
	'0': "0000",
	'1': "0001",
	'2': "0010",
	'3': "0011",
	'4': "0100",
	'5': "0101",
	'6': "0110",
	'7': "0111",
	'8': "1000",
	'9': "1001",
	'A': "1010",
	'B': "1011",
	'C': "1100",
	'D': "1101",
	'E': "1110",
	'F': "1111",
}

func parse(r io.Reader) (string, error) {
	var stream strings.Builder
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		for _, b := range sc.Bytes() {
			stream.WriteString(bits[b])
		}
	}
	return stream.String(), sc.Err()
}

func indent(n int) string {
	return strings.Repeat("  ", n)
}

func packetType(id int64) string {
	switch id {
	case 0:
		return "sum"
	case 1:
		return "product"
	case 2:
		return "minimum"
	case 3:
		return "maximum"
	case 4:
		return "literal"
	case 5:
		return "greater than"
	case 6:
		return "less than"
	case 7:
		return "equal to"
	default:
		return "UNKNOWN"
	}
}

func decode(stream string, debug bool) (versionSum int64, decoded int64) {

	bits := func(n int) string {
		b := stream[:n]
		stream = stream[n:]
		return b
	}

	number := func(n int) int64 {
		v, _ := strconv.ParseInt(bits(n), 2, 64)
		return v
	}

	values := [][]int64{{}}

	frame := func() {
		values = append(values, []int64{})
	}

	store := func(value int64) {
		n := len(values)
		values[n-1] = append(values[n-1], value)
	}

	down := func(fn func([]int64) int64) func() {
		return func() {
			n := len(values)
			value := fn(values[n-1])
			values[n-1] = nil
			values[n-2] = append(values[n-2], value)
			values = values[:n-1]
		}
	}

	operators := [...]func(){
		// sum
		0: down(func(values []int64) int64 {
			var s int64
			for _, x := range values {
				s += x
			}
			return s
		}),
		// product
		1: down(func(values []int64) int64 {
			s := int64(1)
			for _, x := range values {
				s *= x
			}
			return s
		}),
		// min
		2: down(func(values []int64) int64 {
			s := int64(math.MaxInt64)
			for _, x := range values {
				if x < s {
					s = x
				}
			}
			return s
		}),
		// max
		3: down(func(values []int64) int64 {
			s := int64(math.MinInt64)
			for _, x := range values {
				if x > s {
					s = x
				}
			}
			return s
		}),
		// 4: literal
		// gt
		5: down(func(values []int64) int64 {
			if len(values) < 2 {
				panic("gt needs two values")
			}
			if values[0] > values[1] {
				return 1
			}
			return 0
		}),
		// lt
		6: down(func(values []int64) int64 {
			if len(values) < 2 {
				panic("lt needs two values")
			}
			if values[0] < values[1] {
				return 1
			}
			return 0
		}),
		// eq
		7: down(func(values []int64) int64 {
			if len(values) < 2 {
				panic("eq needs two values")
			}
			if values[0] == values[1] {
				return 1
			}
			return 0
		}),
	}

	out := func(string, ...any) {}
	if debug {
		out = log.Printf
	}

	var parser func(int)

	parser = func(depth int) {
		version := number(3)
		out("%sversion: %d\n", indent(depth), version)

		versionSum += version

		id := number(3)
		out("%sid: %d %s\n", indent(depth), id, packetType(id))

		if id == 4 {
			var valueS strings.Builder
			for {
				group := bits(5)
				valueS.WriteString(group[1:])
				if group[0] == '0' {
					break
				}
			}
			value, _ := strconv.ParseInt(valueS.String(), 2, 64)
			out("%sliteral value: %d\n", indent(depth), value)
			store(value)
			return
		}

		// operator
		if id >= int64(len(operators)) {
			panic(fmt.Sprintf("unknown operator: %d", id))
		}

		lengthTypeID := bits(1)[0]
		out("%slength type ID: %c\n", indent(depth), lengthTypeID)

		frame()

		if lengthTypeID == '0' {

			totalLengthBits := number(15)
			out("%stotal length in bits: %d\n", indent(depth), totalLengthBits)

			rest := len(stream[totalLengthBits:])
			for len(stream) > rest {
				parser(depth + 1)
			}

		} else {
			numSubPackets := number(11)
			out("%snumber of sub-packets: %d\n", indent(depth), numSubPackets)
			for i := int64(0); i < numSubPackets; i++ {
				parser(depth + 1)
			}
		}
		operators[id]()
	}

	parser(0)

	out("version sum: %d\n", versionSum)

	for _, value := range values[0] {
		decoded = value
	}
	return
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	stream, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	versionSum, decoded := decode(stream, *debug)
	if *second {
		fmt.Printf("decoded: %d\n", decoded)
	} else {
		fmt.Printf("version sum: %d\n", versionSum)
	}
}
