package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type grid [][]byte

func parse(r io.Reader) (grid, error) {
	sc := bufio.NewScanner(r)
	var g grid
	for lineNo := 1; sc.Scan(); lineNo++ {
		row := bytes.Clone(sc.Bytes())
		for i, c := range row {
			if c < '0' || c > '9' {
				return nil, fmt.Errorf("invalid input in line %d", lineNo)
			}
			row[i] -= '0'
		}
		g = append(g, row)
	}
	return g, sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func (g grid) String() string {
	var sb strings.Builder
	for j, row := range g {
		if j > 0 {
			sb.WriteByte('\n')
		}
		for _, x := range row {
			sb.WriteByte('0' + x)
		}
	}
	return sb.String()
}

func (g grid) incr(i, j int) bool {
	if j < 0 || j >= len(g) {
		return false
	}
	row := g[j]
	if i < 0 || i >= len(row) {
		return false
	}
	row[i]++
	return row[i] == 10
}

func (g grid) index(i, j int) int {
	return j*len(g[0]) + i
}

func (g grid) step() int {

	// increase
	for _, row := range g {
		for i := range row {
			row[i]++
		}
	}

	w, h := len(g[0]), len(g)

	var stack []int
	already := make([]bool, w*h)

	incrNeigh := func(i, j int) {
		for k := -1; k <= 1; k++ {
			for l := -1; l <= 1; l++ {
				if g.incr(i+l, j+k) {
					if idx := g.index(i+l, j+k); !already[idx] {
						already[idx] = true
						stack = append(stack, idx)
					}
				}
			}
		}
	}

	for {
		for j, row := range g {
			for i, x := range row {
				if x >= 10 {
					if idx := g.index(i, j); !already[idx] {
						stack = append(stack, idx)
						already[idx] = true
					}
				}
			}
		}
		if len(stack) == 0 {
			break
		}
		for len(stack) > 0 {
			idx := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			incrNeigh(idx%w, idx/h)
		}
	}

	flashes := 0

	// reset
	for _, row := range g {
		for i, x := range row {
			if x >= 10 {
				row[i] = 0
				flashes++
			}
		}
	}

	return flashes
}

func part1(debug bool) {
	g, err := parse(os.Stdin)
	check(err)
	flashes := 0
	if debug {
		log.Println("Before any steps:")
		log.Printf("%s\n\n", g)
	}
	for i := 1; i <= 100; i++ {
		flashes += g.step()
		if debug {
			log.Printf("After step %d:\n", i)
			log.Printf("%s\n\n", g)
		}
	}
	fmt.Printf("flashes: %d\n", flashes)
}

func part2(_ bool) {
	g, err := parse(os.Stdin)
	check(err)
	for step := 1; ; step++ {
		if g.step() == len(g)*len(g[0]) {
			fmt.Printf("All flashes after %d steps.\n", step)
			return
		}
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	if *second {
		part2(*debug)
	} else {
		part1(*debug)
	}
}
