package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
	"strings"
)

var uniqueLengths = map[int]int{
	2: 1,
	3: 7,
	4: 4,
	7: 8,
}

func parseInput(r io.Reader, fn func([]string, []string)) error {
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		input, output, ok := strings.Cut(line, "|")
		if !ok {
			return fmt.Errorf("no '|' in line %d: %q", lineNo, line)
		}
		fn(strings.Fields(input), strings.Fields(output))
	}
	return sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	total := 0
	check(parseInput(os.Stdin, func(_, output []string) {
		for _, word := range output {
			if _, ok := uniqueLengths[len(word)]; ok {
				total++
			}
		}
	}))
	fmt.Printf("sum of easy identifiables: %d\n", total)
}

func inWord(in, word string) bool {
	for _, r := range in {
		if !strings.ContainsRune(word, r) {
			return false
		}
	}
	return true
}

func inValues[M ~map[K]V, K, V comparable](m M, v V) bool {
	for _, w := range m {
		if w == v {
			return true
		}
	}
	return false
}

func valueKey[M ~map[K]V, K, V comparable](m M) map[V]K {
	n := map[V]K{}
	for k, v := range m {
		n[v] = k
	}
	return n
}

func pow10(n int) int {
	p := 1
	for ; n > 0; n-- {
		p *= 10
	}
	return p
}

func normalize(words []string) {
	for i, word := range words {
		rword := []rune(word)
		slices.Sort(rword)
		words[i] = string(rword)
	}
}

func part2() {
	total := 0
	check(parseInput(os.Stdin, func(input, output []string) {
		// Make input and output word wise comparable.
		normalize(input)
		normalize(output)

		deduced := map[int]string{}
		// First deduce the digits with unique lengths.
		for _, word := range input {
			if m, ok := uniqueLengths[len(word)]; ok {
				deduced[m] = word
			}
		}
		// Find 6
		for _, word := range input {
			if len(word) == 6 && !inWord(deduced[1], word) {
				deduced[6] = word
				//fmt.Println("6:", word)
				break
			}
		}
		// Find 0
		for _, word := range input {
			if len(word) == 6 && !inWord(deduced[4], word) && !inValues(deduced, word) {
				deduced[0] = word
				//fmt.Println("0:", word)
			}
		}
		// Find 9
		for _, word := range input {
			if len(word) == 6 && !inValues(deduced, word) {
				deduced[9] = word
				//fmt.Println("9:", word)
				break
			}
		}
		// Find 5
		for _, word := range input {
			if len(word) == 5 && inWord(word, deduced[6]) {
				deduced[5] = word
				//fmt.Println("5:", word)
				break
			}
		}
		// Find 3
		for _, word := range input {
			if len(word) == 5 && inWord(word, deduced[9]) && !inValues(deduced, word) {
				deduced[3] = word
				//fmt.Println("3:", word)
				break
			}
		}
		// Find 2
		for _, word := range input {
			if len(word) == 5 && !inValues(deduced, word) {
				deduced[2] = word
				//fmt.Println("2:", word)
				break
			}
		}
		// Flip key/value -> value/key and decode.
		code := valueKey(deduced)
		value := 0
		for j, word := range output {
			value += code[word] * pow10(len(output)-j-1)
		}
		total += value
	}))
	fmt.Printf("sum of output values: %d\n", total)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
