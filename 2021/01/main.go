package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func parse(r io.Reader, fn func(int)) error {
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		var value int
		if _, err := fmt.Sscanf(line, "%d", &value); err != nil {
			return fmt.Errorf("cannot parse line %d: %q %w", lineNo, line, err)
		}
		fn(value)
	}
	return sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	first, last, incrs := true, 0, 0

	check(parse(os.Stdin, func(depth int) {
		if first {
			first = false
		} else if depth > last {
			incrs++
		}
		last = depth
	}))
	fmt.Printf("increments: %d\n", incrs)
}

func part2() {
	var window [3]int
	line, last, incrs := 0, 0, 0
	check(parse(os.Stdin, func(depth int) {
		window[line%3] = depth
		if line >= 3 {
			sum := window[0] + window[1] + window[2]
			if sum > last {
				incrs++
			}
			last = sum
		}
		line++
	}))
	fmt.Printf("increments: %d\n", incrs)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
