package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

type pair struct {
	left  *pair
	right *pair
	value int
}

type parseError error

func parseNumber(s string) (p *pair, err error) {

	defer func() {
		if x := recover(); x != nil {
			if e, ok := x.(parseError); ok {
				err = e
			} else {
				panic(x)
			}
		}
	}()

	var back []byte

	lex := func() (byte, bool) {
		if n := len(back); n > 0 {
			c := back[n-1]
			back = back[:n-1]
			return c, true
		}
		if s == "" {
			return 0, false
		}
		c := s[0]
		s = s[1:]
		return c, true
	}

	push := func(b byte) { back = append(back, b) }

	ahead := func() byte {
		c, ok := lex()
		if !ok {
			panic(parseError(errors.New("too short")))
		}
		push(c)
		return c
	}

	expect := func(c byte) {
		if r, ok := lex(); !ok || r != c {
			panic(parseError(fmt.Errorf("expected '%c'", c)))
		}
	}

	literal := func() *pair {
		var b strings.Builder
		for {
			c, ok := lex()
			if !ok {
				panic(parseError(errors.New("too short")))
			}
			if '0' <= c && c <= '9' {
				b.WriteByte(c)
			} else {
				push(c)
				break
			}
		}
		v, err := strconv.Atoi(b.String())
		if err != nil {
			panic(parseError(err))
		}
		return &pair{value: v}
	}

	var rec func() *pair

	rec = func() *pair {

		expect('[')

		var p pair

		if c := ahead(); c == '[' {
			p.left = rec()
		} else {
			p.left = literal()
		}

		expect(',')

		if c := ahead(); c == '[' {
			p.right = rec()
		} else {
			p.right = literal()
		}

		expect(']')

		return &p
	}

	return rec(), nil
}

func parse(r io.Reader) ([]*pair, error) {
	var numbers []*pair

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		number, err := parseNumber(scanner.Text())
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, number)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return numbers, nil
}

func (p *pair) String() string {
	var b strings.Builder
	var rec func(*pair)
	rec = func(c *pair) {
		if c.left == nil {
			b.WriteString(strconv.Itoa(c.value))
			return
		}
		b.WriteByte('[')
		rec(c.left)
		b.WriteByte(',')
		rec(c.right)
		b.WriteByte(']')
	}
	rec(p)
	return b.String()
}

func (p *pair) magnitude() int {
	var rec func(*pair) int
	rec = func(c *pair) int {
		if c.left == nil {
			return c.value
		}
		return 3*rec(c.left) + 2*rec(c.right)
	}
	return rec(p)
}

func (p *pair) add(o *pair) *pair {
	return &pair{left: p.clone(), right: o.clone()}
}

func (p *pair) flat() []*pair {
	var ps []*pair
	var rec func(*pair)
	rec = func(c *pair) {
		if c.left == nil {
			ps = append(ps, c)
			return
		}
		rec(c.left)
		ps = append(ps, c)
		rec(c.right)
	}
	rec(p)
	return ps
}

func (p *pair) clone() *pair {

	var rec func(*pair) *pair
	rec = func(x *pair) *pair {
		if x == nil {
			return nil
		}
		return &pair{
			left:  rec(x.left),
			right: rec(x.right),
			value: x.value,
		}
	}
	return rec(p)
}

func (p *pair) spl() {
	if p.value < 10 {
		log.Println("Already split?")
		return
	}
	//fmt.Printf("split: %s\n", p)
	l := p.value / 2
	r := p.value - l
	p.left = &pair{value: l}
	p.right = &pair{value: r}
}

func (p *pair) split() bool {
	var rec func(*pair, int) bool

	rec = func(c *pair, depth int) bool {
		if c.left == nil {
			if c.value >= 10 {
				c.spl()
				return true
			}
			return false
		}
		if rec(c.left, depth+1) {
			return true
		}
		return rec(c.right, depth+1)
	}
	return rec(p, 0)
}

func (p *pair) exp(root *pair) {
	//fmt.Printf("explode: %s\n", p)
	lv := p.left.value
	rv := p.right.value
	p.left = nil
	p.right = nil
	p.value = 0

	plain := root.flat()
	idx := -1
	for i, x := range plain {
		if x == p {
			idx = i
			break
		}
	}
	if idx == -1 {
		panic("should not happen")
	}

	for lidx := idx - 1; lidx >= 0; lidx-- {
		pi := plain[lidx]
		if pi.left == nil {
			pi.value += lv
			break
		}
	}
	for ridx := idx + 1; ridx < len(plain); ridx++ {
		pi := plain[ridx]
		if pi.left == nil {
			pi.value += rv
			break
		}
	}
}

func (p *pair) explode() bool {

	var rec func(*pair, int) bool

	rec = func(c *pair, depth int) bool {
		if c.left == nil {
			return false
		}
		if depth > 3 && c.left.left == nil {
			c.exp(p)
			return true
		}
		if rec(c.left, depth+1) {
			return true
		}
		return rec(c.right, depth+1)
	}
	return rec(p, 0)
}

func (p *pair) reduce() {
	for {
		if p.explode() {
			continue
		}
		if !p.split() {
			break
		}
	}
}

func sum(numbers []*pair) *pair {
	if len(numbers) == 0 {
		return nil
	}

	result := numbers[0]
	if len(numbers) > 1 {
		for _, number := range numbers[1:] {
			result = result.add(number)
			result.reduce()
		}
	}
	return result
}

func maxMagnitude(numbers []*pair) int {
	max := math.MinInt32
	for i, ni := range numbers {
		for j, nj := range numbers {
			if i == j {
				continue
			}
			n := ni.add(nj)
			n.reduce()
			if mag := n.magnitude(); mag > max {
				max = mag
			}
		}
	}
	return max
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()
	numbers, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if *second {
		fmt.Printf("Max magnitude: %d\n", maxMagnitude(numbers))
	} else {
		result := sum(numbers)
		if *debug {
			log.Printf("Result: %s\n", result)
		}
		if result == nil {
			log.Fatalln("summing failed")
		}
		fmt.Printf("Magnitude: %d\n", result.magnitude())
	}
}
