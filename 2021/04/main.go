package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type marks uint32

type board struct {
	cells [5 * 5]int
	marks marks
}

func (m marks) mark(i, j int) marks {
	return m | 1<<(j*5+i)
}

func (m marks) hasBingo() bool {
	for _, b := range bingos {
		if m&b == b {
			return true
		}
	}
	return false
}

var bingos = func() []marks {
	var ms []marks

	for i := 0; i < 5; i++ {
		var m marks
		for j := 0; j < 5; j++ {
			m = m.mark(i, j)
		}
		ms = append(ms, m)
	}

	for j := 0; j < 5; j++ {
		var m marks
		for i := 0; i < 5; i++ {
			m = m.mark(i, j)
		}
		ms = append(ms, m)
	}
	/* Diagonal
	var m marks
	for i := 0; i < 5; i++ {
		m = m.mark(i, i)
	}
	ms = append(ms, m)
	m = 0
	for i := 0; i < 5; i++ {
		m = m.mark(4-i, i)
	}
	ms = append(ms, m)
	*/

	return ms
}()

func readBoard(scanner *bufio.Scanner) (*board, error) {
	var b board
	for j := 0; j < 5; j++ {
		if !scanner.Scan() {
			return nil, errors.New("not enough lines")
		}
		fields := strings.Fields(scanner.Text())
		if len(fields) < 5 {
			return nil, errors.New("not enough fields")
		}
		for i := 0; i < 5; i++ {
			var err error
			if b.cells[j*5+i], err = strconv.Atoi(fields[i]); err != nil {
				return nil, err
			}
		}
	}
	return &b, nil
}

func (b *board) mark(v int) bool {
	for i, x := range b.cells {
		if x == v {
			b.marks |= 1 << i
			return true
		}
	}
	return false
}

func (b *board) sumUnmarked() int {
	var sum int
	mask := marks(1)
	for i := 0; i < 25; i, mask = i+1, mask<<1 {
		if b.marks&mask == 0 {
			sum += b.cells[i]
		}
	}
	return sum
}

func parseInput(r io.Reader) ([]int, []*board, error) {
	sc := bufio.NewScanner(r)
	if !sc.Scan() {
		return nil, nil, errors.New("no draws")
	}
	numbers := strings.Split(sc.Text(), ",")
	nums := make([]int, 0, len(numbers))
	for _, n := range numbers {
		num, err := strconv.Atoi(n)
		if err != nil {
			return nil, nil, err
		}
		nums = append(nums, num)
	}
	var boards []*board
	for sc.Scan() {
		b, err := readBoard(sc)
		if err != nil {
			return nil, nil, err
		}
		boards = append(boards, b)
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}
	return nums, boards, nil
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	nums, boards, err := parseInput(os.Stdin)
	check(err)
	for _, draw := range nums {
		for _, b := range boards {
			if b.mark(draw) {
				if !b.marks.hasBingo() {
					continue
				}
				sum := b.sumUnmarked()
				fmt.Printf("%d * %d = %d\n", draw, sum, sum*draw)
				return
			}
		}
	}
}

func part2() {
	nums, boards, err := parseInput(os.Stdin)
	check(err)
	won := make([]bool, len(boards))
	for _, draw := range nums {
		for j, b := range boards {
			if b.mark(draw) && b.marks.hasBingo() {
				if !won[j] {
					count := 0
					for _, w := range won {
						if w {
							count++
						}
					}
					if count == len(boards)-1 {
						sum := b.sumUnmarked()
						fmt.Printf("%d * %d = %d\n", draw, sum, sum*draw)
						return
					}
				}
				won[j] = true
			}
		}
	}
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	if *second {
		part2()
	} else {
		part1()
	}
}
