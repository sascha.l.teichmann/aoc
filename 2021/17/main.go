package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
)

type area struct {
	x1 int
	y1 int
	x2 int
	y2 int
}

func parse(r io.Reader) (*area, error) {
	sc := bufio.NewScanner(r)
	var a *area
	if sc.Scan() {
		a = new(area)
		if _, err := fmt.Sscanf(sc.Text(),
			"target area: x=%d..%d, y=%d..%d",
			&a.x1, &a.x2, &a.y1, &a.y2,
		); err != nil {
			return nil, err
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	if a == nil {
		return nil, errors.New("no area given")
	}
	return a, nil
}

func (a *area) inside(x, y int) bool {
	return a.x1 <= x && x <= a.x2 && a.y1 <= y && y <= a.y2
}

func (a area) simulate() (int, int) {
	bestY := math.MinInt32
	hits := 0

	for sxv := 1; sxv <= a.x2; sxv++ {
	yVel:
		for syv := a.y1; syv <= 1000; syv++ {
			px, py := 0, 0
			vx, vy := sxv, syv
			maxY := math.MinInt32
			for py >= a.y1 {
				if py > maxY {
					maxY = py
				}
				if a.inside(px, py) {
					hits++
					if maxY > bestY {
						bestY = maxY
					}
					continue yVel
				}
				px += vx
				py += vy
				if vx > 0 {
					vx--
				} else if vx < 0 {
					vx++
				}
				vy--
			}
		}
	}
	return hits, bestY
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	a, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *debug {
		log.Printf("area [(%d, %d) - (%d, %d)]\n", a.x1, a.y1, a.x2, a.y2)
	}

	hits, bestY := a.simulate()

	if *second {
		fmt.Printf("hits: %d\n", hits)
	} else {
		fmt.Printf("best: %d\n", bestY)
	}
}
