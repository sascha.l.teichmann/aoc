package main

import (
	"bufio"
	"cmp"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
)

type terrain [][]int

func parse(r io.Reader) (terrain, error) {
	var terr terrain
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Bytes()
		row := make([]int, 0, len(line)+2)
		row = append(row, 10)
		for _, r := range line {
			if r < '0' || r > '9' {
				return nil, fmt.Errorf("invalid input in line %d", lineNo)
			}
			row = append(row, int(r-'0'))
		}
		row = append(row, 10)
		if len(terr) == 0 {
			fill := make([]int, len(row))
			for i := range fill {
				fill[i] = 10
			}
			terr = append(terr, fill)
		}
		terr = append(terr, row)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	if len(terr) > 0 {
		fill := make([]int, len(terr[0]))
		for i := range fill {
			fill[i] = 10
		}
		terr = append(terr, fill)
	}
	return terr, nil
}

func (terr terrain) riskLevel() int {
	sum := 0
	w := len(terr[0]) - 2
	h := len(terr) - 2
	for j := 1; j <= h; j++ {
		tj := terr[j]
		for i := 1; i <= w; i++ {
			v := tj[i]
			if terr[j-1][i] > v && terr[j+1][i] > v && tj[i-1] > v && tj[i+1] > v {
				sum += 1 + int(v)
			}
		}
	}
	return sum
}

func (terr terrain) clone() terrain {
	dst := make(terrain, len(terr))
	for i, src := range terr {
		dst[i] = slices.Clone(src)
	}
	return dst
}

func (terr terrain) riskpoints(fn func(int, int)) {
	w := len(terr[0]) - 2
	h := len(terr) - 2
	for j := 1; j <= h; j++ {
		tj := terr[j]
		for i := 1; i <= w; i++ {
			v := tj[i]
			if terr[j-1][i] > v && terr[j+1][i] > v && tj[i-1] > v && tj[i+1] > v {
				fn(i, j)
			}
		}
	}
}

func (terr terrain) fill(i, j, value int) {
	if j < 0 || j >= len(terr) {
		return
	}
	row := terr[j]
	if i < 0 || i >= len(row) {
		return
	}
	if v := row[i]; v >= 9 || v == value {
		return
	}
	row[i] = value
	terr.fill(i-1, j, value)
	terr.fill(i+1, j, value)
	terr.fill(i, j-1, value)
	terr.fill(i, j+1, value)
}

func (terr terrain) count(value int) int {
	sum := 0
	w := len(terr[0]) - 2
	h := len(terr) - 2
	for j := 1; j <= h; j++ {
		tj := terr[j]
		for i := 1; i <= w; i++ {
			if tj[i] == value {
				sum++
			}
		}
	}
	return sum
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	terr, err := parse(os.Stdin)
	check(err)
	fmt.Printf("risk level: %d\n", terr.riskLevel())
}

func part2() {
	terr, err := parse(os.Stdin)
	check(err)
	var sizes []int
	terr.riskpoints(func(i, j int) {
		x := terr.clone()
		x.fill(i, j, -1)
		c := x.count(-1)
		//fmt.Printf("%d %d: %d\n", i, j, c)
		sizes = append(sizes, c)
	})

	slices.SortFunc(sizes, func(a, b int) int { return cmp.Compare(b, a) })

	prod := 1
	for _, v := range sizes[:3] {
		prod *= v
	}
	fmt.Printf("product: %d\n", prod)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
