package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

type point struct {
	x, y int
}

type points map[point]int

func (ps points) count() int {
	count := 0
	for _, v := range ps {
		if v >= 2 {
			count++
		}
	}
	return count
}

func parseInput(r io.Reader, fn func(int, int, int, int)) error {
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		var x1, y1, x2, y2 int
		if _, err := fmt.Sscanf(line,
			"%d,%d -> %d,%d",
			&x1, &y1, &x2, &y2,
		); err != nil {
			return err
		}
		fn(x1, y1, x2, y2)
	}
	return sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1(debug bool) {
	points := points{}
	check(parseInput(os.Stdin, func(x1, y1, x2, y2 int) {
		if x1 > x2 {
			x1, x2 = x2, x1
		}
		if y1 > y2 {
			y1, y2 = y2, y1
		}

		if !(x1 == x2 || y1 == y2) {
			return
		}

		if debug {
			log.Printf("%d,%d -> %d,%d\n", x1, y1, x2, y2)
		}

		for y := y1; y <= y2; y++ {
			for x := x1; x <= x2; x++ {
				points[point{x, y}]++
			}
		}
	}))
	fmt.Printf("points: %d\n", points.count())
}

func part2(_ bool) {
	points := points{}
	check(parseInput(os.Stdin, func(x1, y1, x2, y2 int) {
		var dx, dy int
		if x2 > x1 {
			dx = 1
		} else if x2 < x1 {
			dx = -1
		}
		if y2 > y1 {
			dy = 1
		} else if y2 < y1 {
			dy = -1
		}

		x, y := x1, y1

		for {
			points[point{x, y}]++
			if x == x2 && y == y2 {
				break
			}
			x += dx
			y += dy
		}
	}))
	fmt.Printf("points: %d\n", points.count())
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()
	if *second {
		part2(*debug)
	} else {
		part1(*debug)
	}

}
