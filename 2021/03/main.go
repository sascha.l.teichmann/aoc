package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func parseBinary(r io.Reader) ([]int, int, error) {
	var values []int
	length := -1
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		value, err := strconv.ParseInt(line, 2, 32)
		if err != nil {
			return nil, 0, fmt.Errorf("line %d: %q %w", lineNo, line, err)
		}
		if length == -1 {
			length = len(line)
		}
		values = append(values, int(value))
	}
	if err := sc.Err(); err != nil {
		return nil, 0, err
	}
	return values, length, nil
}

func countZeros(values []int, pos int) int {
	zeros := 0
	mask := 1 << pos
	for _, v := range values {
		if v&mask == 0 {
			zeros++
		}
	}
	return zeros
}

func part1(_ bool) {
	values, length, err := parseBinary(os.Stdin)
	check(err)
	gamma, epsilon := 0, 0

	for pos := length - 1; pos >= 0; pos-- {
		zeros := countZeros(values, pos)
		ones := len(values) - zeros
		if zeros < ones {
			gamma |= 1 << pos
		} else {
			epsilon |= 1 << pos
		}
	}
	product := gamma * epsilon
	fmt.Printf("%[1]b (%[1]d) * %[2]b (%[2]d) = %d\n", gamma, epsilon, product)
}

func selector(cond func(int, int) bool) func(int, int) func(int) bool {
	return func(a, b int) func(int) bool {
		if cond(a, b) {
			return func(v int) bool { return v != 0 }
		}
		return func(v int) bool { return v == 0 }
	}
}

func part2(debug bool) {
	values, length, err := parseBinary(os.Stdin)
	check(err)

	filter := func(toKeep func(int, int) func(int) bool) (int, bool) {
		remaining := values

		for pos := length - 1; pos >= 0 && len(remaining) > 0; pos-- {
			if debug {
				log.Printf("len remaining %d\n", len(remaining))
			}
			if len(remaining) < 2 {
				return remaining[0], true
			}
			zs := countZeros(remaining, pos)
			os := len(remaining) - zs
			keep := toKeep(zs, os)
			var nremaining []int
			for _, r := range remaining {
				if keep(r & (1 << pos)) {
					nremaining = append(nremaining, r)
				}
			}
			remaining = nremaining
			if debug {
				for _, v := range remaining {
					log.Printf("%b %[1]d\n", v)
				}
			}
		}
		if len(remaining) != 1 {
			return 0, false
		}
		return remaining[0], true
	}

	var (
		o2, okO2   = filter(selector(func(zs, os int) bool { return os >= zs }))
		co2, okCO2 = filter(selector(func(zs, os int) bool { return zs > os }))
	)
	if !okO2 || !okCO2 {
		log.Fatalf("cannot find o2 (%t) or co2 (%t) generator rating", okO2, okCO2)
	}

	product := o2 * co2
	fmt.Printf("%[1]b (%[1]d) * %[2]b (%[2]d) = %d\n", o2, co2, product)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()
	if *second {
		part2(*debug)
	} else {
		part1(*debug)
	}
}
