package main

import (
	"bufio"
	"bytes"
	"container/heap"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
)

type grid struct {
	data   []byte
	width  uint32
	height uint32
}

type queueItem struct {
	index  uint32
	dist   int
	qIndex int
}

type queue []*queueItem

func (q queue) Less(i, j int) bool {
	return q[i].dist < q[j].dist
}

func (q queue) Len() int {
	return len(q)
}

func (q queue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
	q[i].qIndex = i
	q[j].qIndex = j
}

func (q *queue) Push(x interface{}) {
	n := len(*q)
	item := x.(*queueItem)
	item.qIndex = n
	*q = append(*q, item)
}

func (q *queue) Pop() interface{} {
	old := *q
	n := len(old)
	x := old[n-1]
	old[n-1] = nil
	x.qIndex = -1
	*q = old[:n-1]
	return x
}

func parse(r io.Reader) (*grid, error) {
	var lines [][]byte
	width := 0
	y := 0
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := bytes.Clone(sc.Bytes())
		for i, v := range line {
			if v < '0' || v > '9' {
				return nil, fmt.Errorf("invalid input in line %d", lineNo)
			}
			line[i] = v - '0'
		}
		lines = append(lines, line)
		if len(line) > width {
			width = len(line)
		}
		y++
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}

	height := len(lines)
	data := make([]byte, width*height)
	pos := data

	for _, line := range lines {
		copy(pos, line)
		pos = pos[width:]
	}

	g := &grid{
		data:   data,
		width:  uint32(width),
		height: uint32(height),
	}

	return g, nil
}

func (g *grid) enlarge() *grid {
	ndata := make([]byte, 25*len(g.data))

	dst := ndata
	for j := byte(0); j < 5; j++ {
		src := g.data
		for k := uint32(0); k < g.height; k++ {
			for i := byte(0); i < 5; i++ {
				for l := range dst[:g.width] {
					x := src[l] + i + j
					if x > 9 {
						x -= 9
					}
					dst[l] = x
				}
				dst = dst[g.width:]
			}
			src = src[g.width:]
		}
	}

	return &grid{
		data:   ndata,
		width:  5 * g.width,
		height: 5 * g.height,
	}
}

func (g *grid) index(x, y uint32) uint32 {
	return y*g.width + x
}

func (g *grid) neighbors(idx uint32, fn func(uint32)) {
	x, y := idx%g.width, idx/g.width
	if x > 0 {
		fn(g.index(x-1, y))
	}
	if x < g.width-1 {
		fn(g.index(x+1, y))
	}
	if y > 0 {
		fn(g.index(x, y-1))
	}
	if y < g.height-1 {
		fn(g.index(x, y+1))
	}
}

func (g *grid) lowestRisk() int {

	/*
		prev := make([]int, len(g.data))
		for i := range prev {
			prev[i] = -1
		}
	*/

	q := make(queue, len(g.data))

	dists := make([]*queueItem, len(g.data))

	start := &queueItem{index: 0, dist: 0, qIndex: 0}
	q[0] = start
	dists[0] = start

	for i := 1; i < len(g.data); i++ {
		it := &queueItem{
			index:  uint32(i),
			dist:   math.MaxInt32,
			qIndex: i,
		}
		q[i] = it
		dists[i] = it
	}
	heap.Init(&q)

	for q.Len() > 0 {
		min := heap.Pop(&q).(*queueItem)

		g.neighbors(min.index, func(nidx uint32) {
			d := int(g.data[nidx])
			nd := dists[nidx]
			if ndist := min.dist + d; ndist < nd.dist {
				nd.dist = ndist
				if nd.qIndex != -1 {
					heap.Fix(&q, nd.qIndex)
				}
				//prev[nidx] = int(min.index)
			}
		})
	}

	return dists[len(dists)-1].dist
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	g, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		g = g.enlarge()
	}
	fmt.Println(g.lowestRisk())
}
