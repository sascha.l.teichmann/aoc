package main

const sourceTmpl = `package main

import (
	"fmt"
	"flag"
	"log"
	"math"
	"runtime"
	"regexp"
	"sync"
)

type alu struct {
	w int
	x int
	y int
	z int

	input []byte
}

type number []byte

func (n number) String() string {
	s := make([]byte, len(n))
	for i, v := range n {
		s[i] = '0' + v
	}
	return string(s)
}

func (a *alu) inp() int {
	x := a.input[0]
	a.input = a.input[1:]
	return int(x)
}

func (a *alu) valid() bool {
	return a.z == 0
}

func (a *alu) run() {
	var w, x, y, z int
{{- range .Instructions }}
	{{ . }}
{{- end }}
	a.w, a.x, a.y, a.z = w, x, y, z
}

func (n number) set(x int64) {
	for pos := len(n) - 1; pos >= 0; pos-- {
		n[pos] = byte(x%9) + 1
		x /= 9
	}
}

func fromString(s string) int64 {
	scale, x := int64(1), int64(0)
	for pos := len(s) - 1; pos >= 0; pos-- {
		x += int64(s[pos]-'1') * scale
		scale *= 9
	}
	return x
}

func upWorker(
	wg *sync.WaitGroup,
	found chan struct{},
	ranges <-chan [2]int64,
	solution *int64,
) {
	defer wg.Done()
	model := make(number, 14)
	var a alu
	for r := range ranges {
		for i := r[0]; i <= r[1]; i++ {
			model.set(i)
			a = alu{input: model}
			a.run()
			if a.valid() {
				*solution = i
				found <- struct{}{}
				return
			}
		}
	}
}

func downWorker(
	wg *sync.WaitGroup,
	found chan struct{},
	ranges <-chan [2]int64,
	solution *int64,
) {
	defer wg.Done()
	model := make(number, 14)
	var a alu
	for r := range ranges {
		for i := r[0]; i >= r[1]; i-- {
			model.set(i)
			a = alu{input: model}
			a.run()
			if a.valid() {
				*solution = i
				found <- struct{}{}
				return
			}
		}
	}
}

func main() {

	var (
		startV = flag.String("start", "", "Start value")
		second = flag.Bool("second", false, "second puzzle")
	)
	flag.Parse()

	up := *second

	var (
		n         = runtime.NumCPU()
		ranges    = make(chan [2]int64)
		found     = make(chan struct{}, n)
		solutions = make([]int64, n)
	)
	for i := range solutions {
		if up {
			solutions[i] = math.MaxInt64
		} else {
			solutions[i] = math.MinInt64
		}
	}

	var wg sync.WaitGroup

	for i := 0; i < n; i++ {
		i := i
		wg.Add(1)
		if up {
			go upWorker(&wg, found, ranges, &solutions[i])
		} else {
			go downWorker(&wg, found, ranges, &solutions[i])
		}
	}

	const chunkSize = 5_000_000

	model := make(number, 14)

	var start int64

	if *startV != "" {
		match, err := regexp.MatchString("[1-9]{14}", *startV)
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}
		if !match {
			log.Fatalln("start value is invalid.")
		}
		start = fromString(*startV)
	} else if up {
		start = 0
	} else {
		start = int64(math.Pow(9, 14)) - 1
	}

	if up {
		max := int64(math.Pow(9, 14)) - 1
	searchUp:
		for checked := int64(0); start <= max; start += chunkSize {
			end := start + (chunkSize - 1)
			if end > max {
				end = max
			}
			checked += end - start + 1
			model.set(end)
			fmt.Printf("checked: %d [%s]\n", checked, model)
			select {
			case ranges <- [2]int64{start, end}:
			case <-found:
				break searchUp
			}
		}
	} else {
	searchDown:
		for checked := int64(0); start >= 0; start -= chunkSize {
			end := start - (chunkSize - 1)
			if end < 0 {
				end = 0
			}
			checked += start - end + 1
			model.set(start)
			fmt.Printf("checked: %d [%s]\n", checked, model)
			select {
			case ranges <- [2]int64{start, end}:
			case <-found:
				break searchDown
			}
		}
	}
	close(ranges)
	wg.Wait()

	var solution int64

	if up {
		solution = math.MaxInt64
		for _, v := range solutions {
			if v < solution {
				solution = v
			}
		}
		if solution == math.MaxInt64 {
			log.Fatalln("No solution found")
		}
	} else {
		solution = math.MinInt64
		for _, v := range solutions {
			if v > solution {
				solution = v
			}
		}
		if solution == math.MinInt64 {
			log.Fatalln("No solution found")
		}
	}

	model.set(solution)
	fmt.Printf("solution: %s\n", model)
}
`
