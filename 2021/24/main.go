package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"go/format"
	"io"
	"log"
	"math"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"sync"
	"text/template"
)

type alu struct {
	registers [4]int

	input []byte
}

type instruction interface {
	execute(*alu)
}

type program []instruction

type lineCompiler struct {
	expr    *regexp.Regexp
	compile func([]string) instruction
}

type (
	inpInstr         int
	addInstr         [2]int
	mulInstr         [2]int
	divInstr         [2]int
	modInstr         [2]int
	eqlInstr         [2]int
	neqlInstr        [2]int
	assignInstr      [2]int
	addConstInstr    [2]int
	mulConstInstr    [2]int
	divConstInstr    [2]int
	modConstInstr    [2]int
	eqlConstInstr    [2]int
	assignConstInstr [2]int
)

type number []byte

func (a *alu) inp() int {
	x := a.input[0]
	a.input = a.input[1:]
	return int(x)
}

func (a *alu) valid() bool {
	return a.registers[3] == 0
}

func (a *alu) run(prg program) {
	for _, instr := range prg {
		instr.execute(a)
	}
}

func regIndex(s string) int {
	switch s {
	case "w":
		return 0
	case "x":
		return 1
	case "y":
		return 2
	case "z":
		return 3
	}
	panic("should not happen")
}

func regName(i int) string {
	switch i {
	case 0:
		return "w"
	case 1:
		return "x"
	case 2:
		return "y"
	case 3:
		return "z"
	}
	panic("should not happen")
}

func (ii inpInstr) execute(a *alu) {
	a.registers[int(ii)] = a.inp()
}

func (ii inpInstr) String() string {
	return fmt.Sprintf("%s = a.inp()", regName(int(ii)))
}

func (ai addInstr) execute(a *alu) {
	a.registers[ai[0]] += a.registers[ai[1]]
}

func (ai addInstr) String() string {
	return fmt.Sprintf("%s += %s", regName(ai[0]), regName(ai[1]))
}

func (mi mulInstr) execute(a *alu) {
	a.registers[mi[0]] *= a.registers[mi[1]]
}

func (mi mulInstr) String() string {
	return fmt.Sprintf("%s *= %s", regName(mi[0]), regName(mi[1]))
}

func (di divInstr) execute(a *alu) {
	a.registers[di[0]] /= a.registers[di[1]]
}

func (di divInstr) String() string {
	return fmt.Sprintf("%s /= %s", regName(di[0]), regName(di[1]))
}
func (mi modInstr) execute(a *alu) {
	a.registers[mi[0]] %= a.registers[mi[1]]
}

func (mi modInstr) String() string {
	return fmt.Sprintf("%s %%= %s", regName(mi[0]), regName(mi[1]))
}

func (ei eqlInstr) execute(a *alu) {
	if a.registers[ei[0]] == a.registers[ei[1]] {
		a.registers[ei[0]] = 1
	} else {
		a.registers[ei[0]] = 0
	}
}

func (ei eqlInstr) String() string {
	return fmt.Sprintf(`if %[1]s == %[2]s {
		%[1]s = 1
	} else {
		%[1]s = 0
	}`, regName(ei[0]), regName(ei[1]))
}

func (nei neqlInstr) execute(a *alu) {
	if a.registers[nei[0]] != a.registers[nei[1]] {
		a.registers[nei[0]] = 1
	} else {
		a.registers[nei[0]] = 0
	}
}

func (nei neqlInstr) String() string {
	return fmt.Sprintf(`if %[1]s != %[2]s {
		%[1]s = 1
	} else {
		%[1]s = 0
	}`, regName(nei[0]), regName(nei[1]))
}

func (ai assignInstr) execute(a *alu) {
	a.registers[ai[0]] = a.registers[ai[1]]
}

func (ai assignInstr) String() string {
	return fmt.Sprintf("%s = %s", regName(ai[0]), regName(ai[1]))
}

func (aci addConstInstr) execute(a *alu) {
	a.registers[aci[0]] += aci[1]
}

func (aci addConstInstr) String() string {
	return fmt.Sprintf("%s += %d", regName(aci[0]), aci[1])
}

func (mci mulConstInstr) execute(a *alu) {
	a.registers[mci[0]] *= mci[1]
}

func (mci mulConstInstr) String() string {
	return fmt.Sprintf("%s *= %d", regName(mci[0]), mci[1])
}

func (dci divConstInstr) execute(a *alu) {
	a.registers[dci[0]] /= dci[1]
}

func (dci divConstInstr) String() string {
	return fmt.Sprintf("%s /= %d", regName(dci[0]), dci[1])
}

func (mci modConstInstr) execute(a *alu) {
	a.registers[mci[0]] %= mci[1]
}

func (mci modConstInstr) String() string {
	return fmt.Sprintf("%s %%= %d", regName(mci[0]), mci[1])
}

func (eci eqlConstInstr) execute(a *alu) {
	if a.registers[eci[0]] == eci[1] {
		a.registers[eci[0]] = 1
	} else {
		a.registers[eci[0]] = 0
	}
}

func (eci eqlConstInstr) String() string {
	return fmt.Sprintf(`if %[1]s == %[2]d {
		%[1]s = 1
	} else {
		%[1]s = 0
	}`, regName(eci[0]), eci[1])
}

func (aci assignConstInstr) execute(a *alu) {
	a.registers[aci[0]] = aci[1]
}

func (aci assignConstInstr) String() string {
	return fmt.Sprintf("%s = %d", regName(aci[0]), aci[1])
}

var (
	inpRe      = regexp.MustCompile(`inp (w|x|y|z)`)
	addRe      = regexp.MustCompile(`add (w|x|y|z) (w|x|y|z)`)
	mulRe      = regexp.MustCompile(`mul (w|x|y|z) (w|x|y|z)`)
	divRe      = regexp.MustCompile(`div (w|x|y|z) (w|x|y|z)`)
	modRe      = regexp.MustCompile(`mod (w|x|y|z) (w|x|y|z)`)
	eqlRe      = regexp.MustCompile(`eql (w|x|y|z) (w|x|y|z)`)
	addConstRe = regexp.MustCompile(`add (w|x|y|z) (-?\d+)`)
	mulConstRe = regexp.MustCompile(`mul (w|x|y|z) (-?\d+)`)
	divConstRe = regexp.MustCompile(`div (w|x|y|z) (-?\d+)`)
	modConstRe = regexp.MustCompile(`mod (w|x|y|z) (-?\d+)`)
	eqlConstRe = regexp.MustCompile(`eql (w|x|y|z) (-?\d+)`)
)

var lineCompilers = []*lineCompiler{
	{inpRe, func(m []string) instruction {
		return inpInstr(regIndex(m[1]))
	}},
	{addRe, func(m []string) instruction {
		ri1 := regIndex(m[1])
		ri2 := regIndex(m[2])
		return addInstr{ri1, ri2}
	}},
	{mulRe, func(m []string) instruction {
		ri1 := regIndex(m[1])
		ri2 := regIndex(m[2])
		return mulInstr{ri1, ri2}
	}},
	{divRe, func(m []string) instruction {
		ri1 := regIndex(m[1])
		ri2 := regIndex(m[2])
		return divInstr{ri1, ri2}
	}},
	{modRe, func(m []string) instruction {
		ri1 := regIndex(m[1])
		ri2 := regIndex(m[2])
		return modInstr{ri1, ri2}
	}},
	{eqlRe, func(m []string) instruction {
		ri1 := regIndex(m[1])
		ri2 := regIndex(m[2])
		return eqlInstr{ri1, ri2}
	}},
	{addConstRe, func(m []string) instruction {
		ri := regIndex(m[1])
		val, _ := strconv.Atoi(m[2])
		return addConstInstr{ri, val}
	}},
	{mulConstRe, func(m []string) instruction {
		ri := regIndex(m[1])
		val, _ := strconv.Atoi(m[2])
		if val == 0 {
			return assignConstInstr{ri, 0}
		}
		return mulConstInstr{ri, val}
	}},
	{divConstRe, func(m []string) instruction {
		ri := regIndex(m[1])
		val, _ := strconv.Atoi(m[2])
		return divConstInstr{ri, val}
	}},
	{modConstRe, func(m []string) instruction {
		ri := regIndex(m[1])
		val, _ := strconv.Atoi(m[2])
		return modConstInstr{ri, val}
	}},
	{eqlConstRe, func(m []string) instruction {
		ri := regIndex(m[1])
		val, _ := strconv.Atoi(m[2])
		return eqlConstInstr{ri, val}
	}},
}

func (n number) String() string {
	s := make([]byte, len(n))
	for i, v := range n {
		s[i] = '0' + v
	}
	return string(s)
}

func parse(r io.Reader) (program, error) {
	sc := bufio.NewScanner(r)
	var prg program
next:
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		for _, lc := range lineCompilers {
			if m := lc.expr.FindStringSubmatch(line); m != nil {
				prg = append(prg, lc.compile(m))
				continue next
			}
		}
		return nil,
			fmt.Errorf("cannot compile '%s':%d", line, lineNo)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return prg, nil
}

func (prg program) rewriteNotEqual() program {
	nprg := make(program, 0, len(prg))
	count := 0
	for _, instr := range prg {
		if n := len(nprg); n > 0 {
			x := nprg[n-1]
			prev, ok := x.(eqlInstr)
			if !ok {
				nprg = append(nprg, instr)
				continue
			}
			curr, ok := instr.(eqlConstInstr)
			if !ok || curr[1] != 0 || curr[0] != prev[0] {
				nprg = append(nprg, instr)
				continue
			}
			nprg[n-1] = neqlInstr{prev[0], prev[1]}
			count++
			continue
		}
		nprg = append(nprg, instr)
	}
	log.Printf("rewrote a==b; a==0 -> a != b instructions: %d\n", count)
	return nprg
}

func (prg program) assignZero() program {
	nprg := make(program, 0, len(prg))
	count := 0
nextInstr:
	for _, instr := range prg {
		if n := len(nprg); n > 0 {
			x := nprg[n-1]
			prev, ok := x.(assignConstInstr)
			if !ok || prev[1] != 0 {
				nprg = append(nprg, instr)
				continue nextInstr
			}
			switch ninstr := instr.(type) {
			case addConstInstr:
				if ninstr[0] != prev[0] { // different registers
					nprg = append(nprg, instr)
					continue nextInstr
				}
				nprg[n-1] = assignConstInstr{ninstr[0], ninstr[1]}
				count++
				continue nextInstr

			case addInstr:
				if ninstr[0] != prev[0] { // different registers
					nprg = append(nprg, instr)
					continue nextInstr
				}
				nprg[n-1] = assignInstr{ninstr[0], ninstr[1]}
				count++
				continue nextInstr
			}
		}
		nprg = append(nprg, instr)
	}
	log.Printf("rewrote x=0; x+=c -> x=c instructions: %d\n", count)
	return nprg
}

func (prg program) removeDivOne() program {
	nprg := make(program, 0, len(prg))
	count := 0
	for _, instr := range prg {
		if di, ok := instr.(divConstInstr); ok && di[1] == 1 {
			count++
			continue
		}
		nprg = append(nprg, instr)
	}
	log.Printf("removed /= 1 instructions: %d\n", count)
	return nprg
}

func (prg program) optimze() program {
	log.Printf("instructions before: %d\n", len(prg))
	prg = prg.removeDivOne()
	prg = prg.assignZero()
	prg = prg.rewriteNotEqual()
	log.Printf("instructions after: %d\n", len(prg))
	return prg
}

func (n number) set(x int64) {
	for pos := len(n) - 1; pos >= 0; pos-- {
		n[pos] = byte(x%9) + 1
		x /= 9
	}
}

func fromString(s string) int64 {
	scale, x := int64(1), int64(0)
	for pos := len(s) - 1; pos >= 0; pos-- {
		x += int64(s[pos]-'1') * scale
		scale *= 9
	}
	return x
}

func upWorker(
	wg *sync.WaitGroup,
	found chan struct{},
	ranges <-chan [2]int64,
	prg program,
	solution *int64,
) {
	defer wg.Done()
	model := make(number, 14)
	var a alu
	for r := range ranges {
		for i := r[0]; i <= r[1]; i++ {
			model.set(i)
			a = alu{input: model}
			a.run(prg)
			if a.valid() {
				*solution = i
				found <- struct{}{}
				return
			}
		}
	}
}

func downWorker(
	wg *sync.WaitGroup,
	found chan struct{},
	ranges <-chan [2]int64,
	prg program,
	solution *int64,
) {
	defer wg.Done()
	model := make(number, 14)
	var a alu
	for r := range ranges {
		for i := r[0]; i >= r[1]; i-- {
			model.set(i)
			a = alu{input: model}
			a.run(prg)
			if a.valid() {
				*solution = i
				found <- struct{}{}
				return
			}
		}
	}
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {

	var (
		compile = flag.Bool("compile", false, "Generate Go program")
		optimze = flag.Bool("optimize", true, "Optimize the code")
		startV  = flag.String("start", "", "Start value")
		second  = flag.Bool("second", false, "second puzzle")
	)
	flag.Parse()

	up := *second

	prg, err := parse(os.Stdin)
	check(err)

	if *optimze {
		prg = prg.optimze()
	}

	if *compile {
		tmpl, err := template.New("source").Parse(sourceTmpl)
		check(err)
		data := struct {
			Instructions program
		}{
			Instructions: prg,
		}
		var source bytes.Buffer
		check(tmpl.Execute(&source, &data))
		formatted, err := format.Source(source.Bytes())
		check(err)
		_, err = io.Copy(os.Stdout, bytes.NewReader(formatted))
		check(err)
		return
	}

	var (
		n         = runtime.NumCPU()
		ranges    = make(chan [2]int64)
		found     = make(chan struct{}, n)
		solutions = make([]int64, n)
	)
	for i := range solutions {
		if up {
			solutions[i] = math.MaxInt64
		} else {
			solutions[i] = math.MinInt64
		}
	}

	var wg sync.WaitGroup

	for i := 0; i < n; i++ {
		i := i
		wg.Add(1)
		if up {
			go upWorker(&wg, found, ranges, prg, &solutions[i])
		} else {
			go downWorker(&wg, found, ranges, prg, &solutions[i])
		}
	}

	const chunkSize = 1_000_000

	model := make(number, 14)

	var start int64

	if *startV != "" {
		match, err := regexp.MatchString("[1-9]{14}", *startV)
		check(err)
		if !match {
			log.Fatalln("start value is invalid.")
		}
		start = fromString(*startV)
	} else if up {
		start = 0
	} else {
		start = int64(math.Pow(9, 14)) - 1
	}

	if up {
		max := int64(math.Pow(9, 14)) - 1
	searchUp:
		for checked := int64(0); start <= max; start += chunkSize {
			end := start + (chunkSize - 1)
			if end > max {
				end = max
			}
			checked += end - start + 1
			model.set(end)
			fmt.Printf("checked: %d [%s]\n", checked, model)
			select {
			case ranges <- [2]int64{start, end}:
			case <-found:
				break searchUp
			}
		}
	} else {
	searchDown:
		for checked := int64(0); start >= 0; start -= chunkSize {
			end := start - (chunkSize - 1)
			if end < 0 {
				end = 0
			}
			checked += start - end + 1
			model.set(start)
			fmt.Printf("checked: %d [%s]\n", checked, model)
			select {
			case ranges <- [2]int64{start, end}:
			case <-found:
				break searchDown
			}
		}
	}
	close(ranges)
	wg.Wait()

	var solution int64

	if up {
		solution = math.MaxInt64
		for _, v := range solutions {
			if v < solution {
				solution = v
			}
		}
		if solution == math.MaxInt64 {
			log.Fatalln("No solution found")
		}
	} else {
		solution = math.MinInt64
		for _, v := range solutions {
			if v > solution {
				solution = v
			}
		}
		if solution == math.MinInt64 {
			log.Fatalln("No solution found")
		}
	}

	model.set(solution)
	fmt.Printf("solution: %s\n", model)
}
