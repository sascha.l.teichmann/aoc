package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
}

type fold struct {
	xAxis bool
	value int
}

var pointRe = regexp.MustCompile(`^(\d+),(\d+)$`)
var foldRe = regexp.MustCompile(`^fold along (x|y)=(\d+)$`)

func parse(r io.Reader) (map[point]struct{}, []fold, error) {
	points := map[point]struct{}{}
	var folds []fold

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		if m := pointRe.FindStringSubmatch(line); m != nil {
			x, _ := strconv.Atoi(m[1])
			y, _ := strconv.Atoi(m[2])
			points[point{x, y}] = struct{}{}
			continue
		}
		if m := foldRe.FindStringSubmatch(line); m != nil {
			xAxis := m[1] == "x"
			value, _ := strconv.Atoi(m[2])
			folds = append(folds, fold{xAxis, value})
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, nil, err
	}

	return points, folds, nil
}

func render(points map[point]struct{}) string {
	if len(points) == 0 {
		return ""
	}
	minX, maxX := math.MaxInt32, math.MinInt32
	minY, maxY := math.MaxInt32, math.MinInt32
	for p := range points {
		if p.x < minX {
			minX = p.x
		}
		if p.x > maxX {
			maxX = p.x
		}
		if p.y < minY {
			minY = p.y
		}
		if p.y > maxY {
			maxY = p.y
		}
	}
	var sb strings.Builder
	for y := minY; y <= maxY; y++ {
		if y != minY {
			sb.WriteByte('\n')
		}
		for x := minX; x <= maxX; x++ {
			if _, ok := points[point{x, y}]; ok {
				sb.WriteRune('\u2587')
			} else {
				sb.WriteByte(' ')
			}
		}
	}
	return sb.String()
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()

	points, folds, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if !*second && len(folds) > 1 {
		folds = folds[:1]
	}

	for _, f := range folds {
		npoints := map[point]struct{}{}
		if f.xAxis {
			for p := range points {
				if p.x < f.value {
					npoints[p] = struct{}{}
				} else {
					x := f.value - (p.x - f.value)
					npoints[point{x, p.y}] = struct{}{}
				}
			}
		} else {
			for p := range points {
				if p.y < f.value {
					npoints[p] = struct{}{}
				} else {
					y := f.value - (p.y - f.value)
					npoints[point{p.x, y}] = struct{}{}
				}
			}
		}
		points = npoints
	}
	fmt.Printf("Number points: %d\n", len(points))
	if *second {
		fmt.Printf("%s\n", render(points))
	}
}
