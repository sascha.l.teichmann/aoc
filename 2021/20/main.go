package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type image struct {
	data   []byte
	back   byte
	width  int
	height int
}

func parse(r io.Reader) (string, *image, error) {

	var (
		state   int
		b       strings.Builder
		replace string
		lines   []string
		width   int
	)

	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if state == 0 {
			if line == "" {
				replace = b.String()
				b.Reset()
				state = 1
			} else {
				b.WriteString(line)
			}
			continue
		}
		if w := len(line); w > width {
			width = w
		}
		lines = append(lines, line)
	}
	if err := sc.Err(); err != nil {
		return "", nil, err
	}

	height := len(lines)
	data := make([]byte, width*height)
	for i, ofs := 0, 0; i < height; i, ofs = i+1, ofs+width {
		copy(data[ofs:], lines[i])
	}
	img := &image{
		data:   data,
		back:   '.',
		width:  width,
		height: height,
	}
	return replace, img, nil
}

func (img *image) pixel(x, y int) byte {
	if x < 0 || x >= img.width || y < 0 || y >= img.height {
		return img.back
	}
	return img.data[y*img.width+x]
}

func (img *image) convolve(kernal func([]byte) byte) *image {
	inp := make([]byte, 9)
	data := make([]byte, img.width*img.height)
	for y := 0; y < img.height; y++ {
		for x := 0; x < img.width; x++ {
			for i, k := -1, 0; i <= 1; i++ {
				for j := -1; j <= 1; j, k = j+1, k+1 {
					inp[k] = img.pixel(x+j, y+i)
				}
			}
			data[y*img.width+x] = kernal(inp)
		}
	}
	for i := range inp {
		inp[i] = img.back
	}
	back := kernal(inp)
	return &image{
		data:   data,
		width:  img.width,
		back:   back,
		height: img.height,
	}
}

func (img *image) buffer(n int) *image {
	nw, nh := img.width+2*n, img.height+2*n
	data := make([]byte, nw*nh)
	for i := range data {
		data[i] = img.back
	}
	for i, ofs, nofs := 0, 0, (n*nw)+n; i < img.height; i, ofs, nofs = i+1, ofs+img.width, nofs+nw {
		copy(data[nofs:], img.data[ofs:ofs+img.width])
	}
	return &image{
		data:   data,
		back:   img.back,
		width:  nw,
		height: nh,
	}
}

func (img *image) String() string {
	var b strings.Builder
	for i, ofs := 0, 0; i < img.height; i, ofs = i+1, ofs+img.width {
		if i > 0 {
			b.WriteByte('\n')
		}
		b.Write(img.data[ofs : ofs+img.width])
	}
	return b.String()
}

func (img *image) count(b byte) int {
	var c int
	for _, x := range img.data {
		if x == b {
			c++
		}
	}
	return c
}

func main() {
	loops := flag.Int("loops", -1, "loops to run")
	second := flag.Bool("second", false, "second puzzle")
	debug := flag.Bool("debug", false, "debug output")
	flag.Parse()

	if *loops == -1 {
		if *second {
			*loops = 50
		} else {
			*loops = 2
		}
	}

	replace, img, err := parse(os.Stdin)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *debug {
		log.Printf("width: %d height: %d\n", img.width, img.height)
	}

	nimg := img.buffer(*loops + 2)
	for i := 1; i <= *loops; i++ {
		nimg = nimg.convolve(func(inp []byte) byte {
			idx := 0
			for i, b := range inp {
				if b == '#' {
					idx |= 1 << (8 - i)
				}
			}
			return replace[idx]
		})
		if *debug {
			log.Printf("After %d enhancement(s):\n", i)
			log.Println(nimg)
		}
	}
	lit := nimg.count('#')
	fmt.Printf("lit pixels: %d\n", lit)
}
