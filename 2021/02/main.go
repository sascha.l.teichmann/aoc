package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

func parse(r io.Reader, fn func(string, int)) error {
	sc := bufio.NewScanner(r)
	for lineNo := 1; sc.Scan(); lineNo++ {
		line := sc.Text()
		var operation string
		var value int
		if _, err := fmt.Sscanf(line, "%s %d", &operation, &value); err != nil {
			return fmt.Errorf("cannot parse line %d: %q %w", lineNo, line, err)
		}
		fn(operation, value)
	}
	return sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	var pos, depth int
	check(parse(os.Stdin, func(operation string, value int) {
		switch operation {
		case "forward":
			pos += value
		case "down":
			depth += value
		case "up":
			depth -= value
		default:
			log.Printf("unknown operation: %q\n", operation)
		}
	}))
	fmt.Printf("result: %d\n", pos*depth)
}

func part2() {
	var pos, depth, aim int
	check(parse(os.Stdin, func(operation string, value int) {
		switch operation {
		case "forward":
			pos += value
			depth += value * aim
		case "down":
			aim += value
		case "up":
			aim -= value
		default:
			log.Printf("unknown operation: %q\n", operation)
		}
	}))
	fmt.Printf("result: %d\n", pos*depth)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
