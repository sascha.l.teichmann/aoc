# Solutions for Advent of Code 2021

To compile the example you need at least Go 1.21.

All solutions take their input from STDIN and writes the results
to STDOUT. Adding the flag `-second` will print the
solution of the second puzzle of the day. For example:

```
go run 01/main.go < /location/of/your/aoc2021/01/input
go run 01/main.go -second < /location/of/your/aoc2021/01/input
```

**Warning**: The solution of day [24](./24) is a blunt brute force one.
So execute with care. When used to generate a Go program with the
`-compile` flag the generated program take ~19.5h on an AMD Ryzen 7 3700X (8C/16T)
to issue the solution of the first part.
