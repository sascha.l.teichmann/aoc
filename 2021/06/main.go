package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

func parseInput(r io.Reader, fn func(int) error) error {
	sc := bufio.NewScanner(r)
	if !sc.Scan() {
		if err := sc.Err(); err != nil {
			return err
		}
		return errors.New("no fishes")
	}
	fs := strings.Split(sc.Text(), ",")
	for _, f := range fs {
		v, err := strconv.Atoi(f)
		if err != nil {
			return err
		}
		if err := fn(v); err != nil {
			return err
		}
	}
	return nil
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	var fishes []byte
	check(parseInput(os.Stdin, func(fish int) error {
		fishes = append(fishes, byte(fish))
		return nil
	}))
	for day := 1; day <= 80; day++ {
		var nfishes []byte
		for i, v := range fishes {
			var n byte
			switch v {
			case 0:
				n = 6
				nfishes = append(nfishes, 8)
			default:
				n = v - 1
			}
			fishes[i] = n
		}
		fishes = append(fishes, nfishes...)
		//fmt.Printf("%d days: %d %v\n", day, len(fishes), fishes)
	}
	fmt.Printf("after 80 days: %d\n", len(fishes))
}

func part2() {
	var ages [9]uint64
	check(parseInput(os.Stdin, func(age int) error {
		if age < 0 || age >= len(ages) {
			return fmt.Errorf("out of age range: %d", age)
		}
		ages[age]++
		return nil
	}))
	var sum uint64
	for day := 1; day <= 256; day++ {
		six := ages[0]
		for i := 0; i < 8; i++ {
			ages[i] = ages[i+1]
		}
		ages[6] += six
		ages[8] = six
		sum = 0
		for _, v := range ages {
			sum += v
		}
	}
	fmt.Printf("after 256 days: %d\n", sum)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}
}
