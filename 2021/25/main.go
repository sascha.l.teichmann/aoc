package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
)

type grid struct {
	data   []byte
	width  int
	height int
}

func parse(r io.Reader) (*grid, error) {

	sc := bufio.NewScanner(r)

	var lines []string
	var width int

	for sc.Scan() {
		line := sc.Text()
		if w := len(line); w > width {
			width = w
		}
		lines = append(lines, line)
	}

	if err := sc.Err(); err != nil {
		return nil, err
	}

	height := len(lines)
	data := make([]byte, width*height)
	for pos, i := data, 0; len(pos) > 0; pos, i = pos[width:], i+1 {
		copy(pos, lines[i])
	}

	return &grid{
		data:   data,
		width:  width,
		height: height,
	}, nil
}

func (g *grid) advance() bool {
	tmp := make([]byte, len(g.data))
	w, h := g.width, g.height
	changed := false

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			idx0 := i*w + (j+w-1)%w
			idx1 := i*w + j
			idx2 := i*w + (j+1)%w
			if g.data[idx1] == '>' && g.data[idx2] == '.' {
				tmp[idx1] = '.'
				changed = true
			} else if g.data[idx0] == '>' && g.data[idx1] == '.' {
				tmp[idx1] = '>'
			} else {
				tmp[idx1] = g.data[idx1]
			}
		}
	}

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			idx0 := ((i+h-1)%h)*w + j
			idx1 := i*w + j
			idx2 := ((i+1)%h)*w + j
			if tmp[idx1] == 'v' && tmp[idx2] == '.' {
				g.data[idx1] = '.'
				changed = true
			} else if tmp[idx0] == 'v' && tmp[idx1] == '.' {
				g.data[idx1] = 'v'
			} else {
				g.data[idx1] = tmp[idx1]
			}
		}
	}

	return changed
}

/*
func (g *grid) String() string {
	var b strings.Builder
	for s := g.data; len(s) > 0; s = s[g.width:] {
		if b.Len() > 0 {
			b.WriteByte('\n')
		}
		b.Write(s[:g.width])
	}
	return b.String()
}
*/

func main() {
	g, err := parse(os.Stdin)
	if err != nil {
		log.Printf("error: %v\n", err)
	}

	//fmt.Println(g)
	//fmt.Println("---------------")
	count := 1
	for ; g.advance(); count++ {
		//fmt.Printf("%d\n", count+1)
		//fmt.Println(g)
	}

	fmt.Printf("no changes after: %d\n", count)
}
