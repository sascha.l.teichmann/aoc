package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"slices"
	"strings"
)

func parse(r io.Reader) ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	return lines, sc.Err()
}

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1() {
	lines, err := parse(os.Stdin)
	check(err)
	rep := strings.NewReplacer(
		`()`, ``,
		`[]`, ``,
		`{}`, ``,
		`<>`, ``)
	points := 0
all:
	for _, line := range lines {
		nline := rep.Replace(line)
		for nline != line {
			line, nline = nline, rep.Replace(nline)
		}
		for _, r := range nline {
			switch r {
			case ')':
				points += 3
				continue all
			case ']':
				points += 57
				continue all
			case '}':
				points += 1197
				continue all
			case '>':
				points += 25137
				continue all
			}
		}
	}
	fmt.Printf("points: %d\n", points)
}

func reverse(s string) string {
	r := []rune(s)
	slices.Reverse(r)
	return string(r)
}

func part2() {
	lines, err := parse(os.Stdin)
	check(err)
	del := strings.NewReplacer(
		`()`, ``,
		`[]`, ``,
		`{}`, ``,
		`<>`, ``)
	mirror := strings.NewReplacer(
		`(`, `)`,
		`[`, `]`,
		`{`, `}`,
		`<`, `>`)
	var scores []int
all:
	for _, line := range lines {
		nline := del.Replace(line)
		for nline != line {
			line, nline = nline, del.Replace(nline)
		}
		for _, r := range nline {
			switch r {
			case ')', ']', '}', '>':
				continue all
			}
		}
		m := mirror.Replace(reverse(nline))
		score := 0
		for _, r := range m {
			var p int
			switch r {
			case ')':
				p = 1
			case ']':
				p = 2
			case '}':
				p = 3
			case '>':
				p = 4
			}
			score = score*5 + p
		}
		//log.Printf("%s %s - %d\n", nline, m, score)
		scores = append(scores, score)
	}
	slices.Sort(scores)
	median := scores[len(scores)/2]
	fmt.Printf("score: %d\n", median)
}

func main() {
	second := flag.Bool("second", false, "second puzzle")
	flag.Parse()
	if *second {
		part2()
	} else {
		part1()
	}

}
