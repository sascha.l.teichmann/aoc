package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

var bracketRe = regexp.MustCompile(`\[([^\]]+)\]`)

func isABBA(s string) bool {
	return s[0] == s[3] && s[1] == s[2] && s[0] != s[1]
}

func isBAB(s string) bool {
	return s[0] == s[2] && s[0] != s[1]
}

func part1(lines []string) {
	sum := 0
nextLine:
	for _, line := range lines {
		if m := bracketRe.FindAllStringSubmatch(line, -1); m != nil {
			for _, subnet := range m {
				for _, part := range subnet[1:] {
					for ; len(part) >= 4; part = part[1:] {
						if isABBA(part) {
							continue nextLine
						}
					}
				}
			}
		}
		parts := bracketRe.Split(line, -1)
		for _, part := range parts {
			for ; len(part) >= 4; part = part[1:] {
				if isABBA(part) {
					sum++
					continue nextLine
				}
			}
		}
	}
	fmt.Println(sum)
}

func part2(lines []string) {
	sum := 0
nextLine:
	for _, line := range lines {
		babs := map[string]bool{}
		if m := bracketRe.FindAllStringSubmatch(line, -1); m != nil {
			for _, subnet := range m {
				for _, part := range subnet[1:] {
					for ; len(part) >= 3; part = part[1:] {
						if isBAB(part) {
							babs[part[:3]] = true
						}
					}
				}
			}
		}
		parts := bracketRe.Split(line, -1)
		for _, part := range parts {
			for ; len(part) >= 3; part = part[1:] {
				if isBAB(part) {
					if key := string([]byte{part[1], part[0], part[1]}); babs[key] {
						sum++
						continue nextLine
					}
				}
			}
		}
	}
	fmt.Println(sum)
}

func load() ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
