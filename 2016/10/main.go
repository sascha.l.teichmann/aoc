package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

type receiver interface {
	receive(int)
}

type bot struct {
	chips []int
	low   receiver
	high  receiver
}

type output struct {
	slots []int
}

func (b *bot) receive(chip int) {
	b.chips = append(b.chips, chip)
}

func (b *bot) ready() bool {
	return b.low != nil && b.high != nil && len(b.chips) > 1
}

func (b *bot) act() {
	l := min(b.chips[0], b.chips[1])
	h := max(b.chips[0], b.chips[1])
	b.chips = b.chips[:0]
	b.low.receive(l)
	b.high.receive(h)
}

func (o *output) receive(chip int) {
	o.slots = append(o.slots, chip)
}

type system struct {
	outputs map[int]*output
	bots    map[int]*bot
}

type instr func(*system)

type instrBuilder struct {
	pattern *regexp.Regexp
	build   func([]string) (instr, error)
}

var instrBuilders = []instrBuilder{
	{regexp.MustCompile(
		`value (\d+) goes to bot (\d+)`), valueToBot},
	{regexp.MustCompile(
		`bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)`), giveAway},
}

func valueToBot(m []string) (instr, error) {
	value, err := strconv.Atoi(m[1])
	if err != nil {
		return nil, err
	}
	bot, err := strconv.Atoi(m[2])
	if err != nil {
		return nil, err
	}
	return func(s *system) {
		s.bot(bot).receive(value)
	}, nil
}

func giveAway(m []string) (instr, error) {
	bot, err := strconv.Atoi(m[1])
	if err != nil {
		return nil, err
	}

	lowR := m[2]

	low, err := strconv.Atoi(m[3])
	if err != nil {
		return nil, err
	}

	highR := m[4]

	high, err := strconv.Atoi(m[5])
	if err != nil {
		return nil, err
	}

	return func(s *system) {
		b := s.bot(bot)
		b.low = s.receiver(lowR, low)
		b.high = s.receiver(highR, high)
	}, nil
}

func (s *system) receiver(kind string, n int) receiver {
	switch kind {
	case "bot":
		return s.bot(n)
	case "output":
		return s.output(n)
	default:
		panic(fmt.Sprintf("unknown receiver %q", kind))
	}
}

func (s *system) bot(n int) *bot {
	if b := s.bots[n]; b != nil {
		return b
	}
	b := new(bot)
	s.bots[n] = b
	return b
}

func (s *system) output(n int) *output {
	if o := s.outputs[n]; o != nil {
		return o
	}
	o := new(output)
	s.outputs[n] = o
	return o
}

func load() ([]instr, error) {
	var instrs []instr
	sc := bufio.NewScanner(os.Stdin)
nextInstr:
	for sc.Scan() {
		line := sc.Text()
		for _, builder := range instrBuilders {
			if m := builder.pattern.FindStringSubmatch(line); m != nil {
				in, err := builder.build(m)
				if err != nil {
					return nil, err
				}
				instrs = append(instrs, in)
				continue nextInstr
			}
		}
		log.Printf("unknown instruction: %q\n", line)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return instrs, nil
}

func (s *system) runReady(check func(int, *bot) bool) bool {
again:
	for n, b := range s.bots {
		if b.ready() {
			if check != nil && !check(n, b) {
				return false
			}
			b.act()
			goto again
		}
	}
	return true
}

func newSystem() *system {
	return &system{
		bots:    map[int]*bot{},
		outputs: map[int]*output{},
	}
}

func part1Bot(n int, b *bot) bool {
	l := min(b.chips[0], b.chips[1])
	h := max(b.chips[0], b.chips[1])
	if l == 17 && h == 61 {
		fmt.Println(n)
		return false
	}
	return true
}

func part1(instrs []instr) {
	s := newSystem()
	for _, instr := range instrs {
		instr(s)
		if !s.runReady(part1Bot) {
			break
		}
	}
}

func part2(instrs []instr) {
	s := newSystem()
	for _, instr := range instrs {
		instr(s)
		if !s.runReady(nil) {
			break
		}
	}
	prod := 1
	for k := range 3 {
		if o := s.outputs[k]; o != nil && len(o.slots) > 0 {
			prod *= o.slots[0]
		}
	}
	fmt.Println(prod)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	instrs, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(instrs)
	} else {
		part1(instrs)
	}
}
