package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type step struct {
	left  bool
	delta int
}

var dirs = [][2]int{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func part1(steps []step) {
	px, py := 0, 0
	dir := 0
	for _, s := range steps {
		if s.left {
			dir = (dir + len(dirs) - 1) % len(dirs)
		} else {
			dir = (dir + 1) % len(dirs)
		}
		px += dirs[dir][0] * s.delta
		py += dirs[dir][1] * s.delta
	}
	if px < 0 {
		px = -px
	}
	if py < 0 {
		py = -py
	}
	fmt.Println(px + py)
}

func part2(steps []step) {
	px, py := 0, 0
	dir := 0
	visited := map[[2]int]int{}
nextStep:
	for _, s := range steps {
		if s.left {
			dir = (dir + len(dirs) - 1) % len(dirs)
		} else {
			dir = (dir + 1) % len(dirs)
		}
		for range s.delta {
			px += dirs[dir][0]
			py += dirs[dir][1]
			key := [2]int{px, py}
			if visited[key]++; visited[key] > 1 {
				break nextStep
			}
		}
	}
	if px < 0 {
		px = -px
	}
	if py < 0 {
		py = -py
	}
	fmt.Println(px + py)
}

func load() ([]step, error) {
	sRe, err := regexp.Compile(`(R|L)(\d+).*`)
	if err != nil {
		return nil, err
	}
	var steps []step
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		parts := strings.Fields(line)
		for _, part := range parts {
			m := sRe.FindStringSubmatch(part)
			if m == nil {
				continue
			}
			var left bool
			switch m[1] {
			case "L":
				left = true
			case "R":
				left = false
			}
			delta, err := strconv.Atoi(m[2])
			if err != nil {
				return nil, err
			}
			steps = append(steps, step{left: left, delta: delta})
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return steps, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	steps, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(steps)
	} else {
		part1(steps)
	}
}
