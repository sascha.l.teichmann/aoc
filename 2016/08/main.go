package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	width  = 50
	height = 6
)

type display [width * height]bool

type instr func(*display)

type instrBuilder struct {
	pattern *regexp.Regexp
	builder func([]string) (instr, error)
}

func inside(x, y int) bool {
	return x >= 0 && x < width && y >= 0 && y < height
}

func (d *display) set(x, y int, on bool) {
	if inside(x, y) {
		d[y*width+x] = on
	}
}

func (d *display) get(x, y int) bool {
	if inside(x, y) {
		return d[y*width+x]
	}
	return false
}

func (d *display) lit() int {
	count := 0
	for _, on := range d {
		if on {
			count++
		}
	}
	return count
}

func buildRect(m []string) (instr, error) {
	w, err := strconv.Atoi(m[1])
	if err != nil {
		return nil, err
	}
	h, err := strconv.Atoi(m[2])
	if err != nil {
		return nil, err
	}
	return func(d *display) {
		for y := range h {
			for x := range w {
				d.set(x, y, true)
			}
		}
	}, nil
}

func buildRotateColumn(m []string) (instr, error) {
	col, err := strconv.Atoi(m[1])
	if err != nil {
		return nil, err
	}
	by, err := strconv.Atoi(m[2])
	if err != nil {
		return nil, err
	}
	return func(d *display) {
		for range by {
			t := d.get(col, height-1)
			for y := height - 1; y > 0; y-- {
				d.set(col, y, d.get(col, y-1))
			}
			d.set(col, 0, t)
		}
	}, nil
}

func buildRotateRow(m []string) (instr, error) {
	row, err := strconv.Atoi(m[1])
	if err != nil {
		return nil, err
	}
	by, err := strconv.Atoi(m[2])
	if err != nil {
		return nil, err
	}
	return func(d *display) {
		for range by {
			t := d.get(width-1, row)
			for x := width - 1; x > 0; x-- {
				d.set(x, row, d.get(x-1, row))
			}
			d.set(0, row, t)
		}
	}, nil
}

var instrBuilders = []instrBuilder{
	{regexp.MustCompile(`rect (\d+)x(\d+)`), buildRect},
	{regexp.MustCompile(`rotate column x=(\d+) by (\d+)`), buildRotateColumn},
	{regexp.MustCompile(`rotate row y=(\d+) by (\d+)`), buildRotateRow},
}

func load() ([]instr, error) {
	var instrs []instr
	sc := bufio.NewScanner(os.Stdin)
nextLine:
	for sc.Scan() {
		line := sc.Text()
		for _, ib := range instrBuilders {
			if m := ib.pattern.FindStringSubmatch(line); m != nil {
				instr, err := ib.builder(m)
				if err != nil {
					return nil, err
				}
				instrs = append(instrs, instr)
				continue nextLine
			}
		}
		log.Printf("unknown instruction %q\n", line)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return instrs, nil
}

func (d *display) show() {
	var b strings.Builder
	for p := d[:]; len(p) > 0; p = p[width:] {
		b.Reset()
		for _, on := range p[:width] {
			if on {
				b.WriteRune('\u2588')
			} else {
				b.WriteByte(' ')
			}
		}
		fmt.Println(b.String())
	}
}

func (d *display) execute(instrs []instr) {
	for _, instr := range instrs {
		instr(d)
	}
}

func part1(instrs []instr) {
	var d display
	d.execute(instrs)
	fmt.Println(d.lit())
}

func part2(instrs []instr) {
	var d display
	d.execute(instrs)
	d.show()
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	instrs, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(instrs)
	} else {
		part1(instrs)
	}
}
