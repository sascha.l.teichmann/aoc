package main

import (
	"bufio"
	"cmp"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

var checkRe = regexp.MustCompile(`(\d+)\[([a-z]+)\]`)

func realRooms(rooms []string, fn func(names []string, sector int)) {
	for _, room := range rooms {
		parts := strings.Split(room, "-")
		if len(parts) < 2 {
			log.Printf("invalid line: %q\n", room)
			continue
		}
		m := checkRe.FindStringSubmatch(parts[len(parts)-1])
		if m == nil {
			log.Printf("no checksum found: %q\n", parts[len(parts)-1])
			continue
		}
		names := parts[:len(parts)-1]
		count := map[rune]int{}
		for _, name := range names {
			for _, r := range name {
				count[r]++
			}
		}
		ordered := make([]rune, 0, len(count))
		for k := range count {
			ordered = append(ordered, k)
		}
		slices.SortFunc(ordered, func(a, b rune) int {
			return cmp.Or(cmp.Compare(count[b], count[a]), cmp.Compare(a, b))
		})
		if len(ordered) > 5 {
			ordered = ordered[:5]
		}
		if m[2] == string(ordered) {
			v, _ := strconv.Atoi(m[1])
			fn(names, v)
		}
	}
}

func part1(rooms []string) {
	sum := 0
	realRooms(rooms, func(_ []string, sector int) {
		sum += sector
	})
	fmt.Println(sum)
}

func part2(rooms []string) {
	realRooms(rooms, func(names []string, sector int) {
		var b strings.Builder
		for i, name := range names {
			if i > 0 {
				b.WriteByte(' ')
			}
			b.WriteString(strings.Map(func(r rune) rune {
				switch {
				case 'a' <= r && r <= 'z':
					return 'a' + rune((int(r-'a')+sector)%26)
				default:
					return r
				}
			}, name))
		}
		if n := strings.ToLower(b.String()); strings.Contains(n, "north") &&
			strings.Contains(n, "pole") &&
			strings.Contains(n, "object") {
			fmt.Println(sector)
		}
	})
}

func load() ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
