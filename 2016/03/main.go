package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func part1(tris [][3]int) {
	valid := 0
nextTri:
	for _, t := range tris {
		for i := range 3 {
			s := 0
			for j := range 3 {
				if i != j {
					s += t[j]
				}
			}
			if s <= t[i] {
				continue nextTri
			}
		}
		valid++
	}
	fmt.Println(valid)
}

func part2(tris [][3]int) {
	n := len(tris) % 2
	ntris := make([][3]int, 0, len(tris))
	for i := 0; i < len(tris)-n; i += 3 {
		ntris = append(ntris, [3]int{tris[i][0], tris[i+1][0], tris[i+2][0]})
		ntris = append(ntris, [3]int{tris[i][1], tris[i+1][1], tris[i+2][1]})
		ntris = append(ntris, [3]int{tris[i][2], tris[i+1][2], tris[i+2][2]})
	}
	part1(ntris)
}

func load() ([][3]int, error) {
	var tris [][3]int
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		fields := strings.Fields(line)
		if len(fields) < 3 {
			return nil, errors.New("too few fields")
		}
		var t [3]int
		for i := 0; i < 3; i++ {
			x, err := strconv.Atoi(fields[i])
			if err != nil {
				return nil, err
			}
			t[i] = x
		}
		tris = append(tris, t)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return tris, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	tris, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(tris)
	} else {
		part1(tris)
	}
}
