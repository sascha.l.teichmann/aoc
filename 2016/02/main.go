package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

var numbers = map[[2]int]rune{
	{0, 0}: '1',
	{1, 0}: '2',
	{2, 0}: '3',
	{0, 1}: '4',
	{1, 1}: '5',
	{2, 1}: '6',
	{0, 2}: '7',
	{1, 2}: '8',
	{2, 2}: '9',
}

var deltas = map[rune][2]int{
	'U': {0, -1},
	'D': {0, +1},
	'R': {+1, 0},
	'L': {-1, 0},
}

var matrix = "  1  " +
	" 234 " +
	"56789" +
	" ABC " +
	"  D  "

func part1(lines []string) {
	var b strings.Builder
	p := [2]int{1, 1}
	for _, line := range lines {
		for _, r := range line {
			delta := deltas[r]
			p[0] = max(0, min(2, p[0]+delta[0]))
			p[1] = max(0, min(2, p[1]+delta[1]))
		}
		b.WriteRune(numbers[p])
	}
	fmt.Println(b.String())
}

func part2(lines []string) {
	var b strings.Builder
	p := [2]int{0, 2}
	for _, line := range lines {
		for _, r := range line {
			delta := deltas[r]
			nx, ny := p[0]+delta[0], p[1]+delta[1]
			if nx < 0 || nx > 4 || ny < 0 || ny > 4 || matrix[ny*5+nx] == ' ' {
				continue
			}
			p[0] = nx
			p[1] = ny
		}
		b.WriteByte(matrix[p[1]*5+p[0]])
	}
	fmt.Println(b.String())
}

func load() ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
