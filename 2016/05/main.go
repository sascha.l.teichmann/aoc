package main

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func part1(lines []string) {
	for _, line := range lines {
		var pw []byte
	hashing:
		for i := 0; ; i++ {
			hash := md5.New()
			fmt.Fprintf(hash, "%s%d", line, i)
			sum := hash.Sum(nil)
			h := hex.EncodeToString(sum)
			if strings.HasPrefix(h, "00000") {
				pw = append(pw, h[5])
				if len(pw) == 8 {
					break hashing
				}
			}
		}
		fmt.Println(string(pw))
	}
}

func part2(lines []string) {
	for _, line := range lines {
		pw := make([]byte, 8)
		mask := 0
	hashing:
		for i := 0; ; i++ {
			hash := md5.New()
			fmt.Fprintf(hash, "%s%d", line, i)
			sum := hash.Sum(nil)
			h := hex.EncodeToString(sum)
			if strings.HasPrefix(h, "00000") {
				pos := h[5] - '0'
				if pos < 0 || pos > 7 || pw[pos] != 0 {
					continue
				}
				pw[pos] = h[6]
				if mask |= 1 << pos; mask == 0xff {
					break hashing
				}
			}
		}
		fmt.Println(string(pw))
	}
}

func load() ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
