package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"strings"
)

func count(lines []string) []map[rune]int {
	var freq []map[rune]int
	for _, line := range lines {
		i := 0
		for _, r := range line {
			if len(freq) < i+1 {
				freq = append(freq, map[rune]int{})
			}
			freq[i][r]++
			i++
		}
	}
	return freq
}

func part1(lines []string) {
	var b strings.Builder
	freq := count(lines)
	for _, f := range freq {
		var r rune
		var m int
		for k, v := range f {
			if v > m {
				m = v
				r = k
			}
		}
		b.WriteRune(r)
	}
	fmt.Println(b.String())
}

func part2(lines []string) {
	var b strings.Builder
	freq := count(lines)
	for _, f := range freq {
		var r rune
		m := math.MaxInt
		for k, v := range f {
			if v < m {
				m = v
				r = k
			}
		}
		b.WriteRune(r)
	}
	fmt.Println(b.String())
}

func load() ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
