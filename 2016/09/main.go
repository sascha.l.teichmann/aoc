package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
)

var repeatRe = regexp.MustCompile(`\((\d+)x(\d+)\)`)

func part1(data []byte) {
	var out bytes.Buffer
	for rest := data; len(rest) > 0; {
		if m := repeatRe.FindSubmatchIndex(rest); m == nil {
			out.Write(rest)
			rest = nil
		} else {
			before := rest[:m[0]]
			out.Write(before)
			after := rest[m[1]:]
			n1, _ := strconv.Atoi(string(rest[m[2]:m[3]]))
			n2, _ := strconv.Atoi(string(rest[m[4]:m[5]]))
			for range n2 {
				out.Write(after[:n1])
			}
			rest = after[n1:]
		}
	}
	fmt.Println(out.Len())
}

func part2(data []byte) {
	var decompress func(input []byte) []byte
	decompress = func(rest []byte) []byte {
		var out bytes.Buffer
		for len(rest) > 0 {
			if m := repeatRe.FindSubmatchIndex(rest); m == nil {
				out.Write(rest)
				rest = nil
			} else {
				before := rest[:m[0]]
				out.Write(before)
				after := rest[m[1]:]
				n1, _ := strconv.Atoi(string(rest[m[2]:m[3]]))
				n2, _ := strconv.Atoi(string(rest[m[4]:m[5]]))
				rep := decompress(after[:n1])
				for range n2 {
					out.Write(rep)
				}
				rest = after[n1:]
			}
		}
		return out.Bytes()
	}
	out := decompress(data)
	fmt.Println(len(out))
}

func load() ([]byte, error) {
	data, err := io.ReadAll(os.Stdin)
	if err != nil {
		return nil, err
	}
	return bytes.TrimSpace(data), nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	data, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(data)
	} else {
		part1(data)
	}
}
