package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

var (
	north = [2]int{0, -1}
	east  = [2]int{+1, 0}
	south = [2]int{0, +1}
	west  = [2]int{-1, 0}
)

var turn = map[[2]int][2]int{
	north: east,
	east:  south,
	south: west,
	west:  north,
}

func start(raster [][]byte) (guard [2]int) {
	for ; guard[1] < len(raster); guard[1]++ {
		if x := slices.Index(raster[guard[1]], '^'); x >= 0 {
			guard[0] = x
			return
		}
	}
	return
}

func part1(raster [][]byte) {
	width, height := len(raster[0]), len(raster)

	dir := north
	guard := start(raster)

	visited := map[[2]int]bool{guard: true}

	for {
		n := [2]int{guard[0] + dir[0], guard[1] + dir[1]}
		if n[0] < 0 || n[0] >= width || n[1] < 0 || n[1] >= height {
			break
		}
		if raster[n[1]][n[0]] == '#' {
			dir = turn[dir]
			continue
		}
		guard = n
		visited[guard] = true
	}

	fmt.Println(len(visited))
}

func part2(raster [][]byte) {
	width, height := len(raster[0]), len(raster)

	dir := north
	s := start(raster)
	guard := s

	visited := map[[2]int]bool{guard: true}
	var trace [][2]int

	for {
		n := [2]int{guard[0] + dir[0], guard[1] + dir[1]}
		if n[0] < 0 || n[0] >= width || n[1] < 0 || n[1] >= height {
			break
		}
		if raster[n[1]][n[0]] == '#' {
			dir = turn[dir]
			continue
		}
		guard = n
		visited[guard] = true
		trace = append(trace, guard)
	}

	loopLen := len(trace) * 2

	obstacles := map[[2]int]bool{}

	for _, ob := range trace {
		if ob == s {
			continue
		}
		raster[ob[1]][ob[0]] = '#'
		dir = north
		guard := s
		for i := 0; i < loopLen; i++ {
			n := [2]int{guard[0] + dir[0], guard[1] + dir[1]}
			if n[0] < 0 || n[0] >= width || n[1] < 0 || n[1] >= height {
				goto restore
			}
			if raster[n[1]][n[0]] == '#' {
				dir = turn[dir]
				continue
			}
			guard = n
		}
		obstacles[ob] = true
	restore:
		raster[ob[1]][ob[0]] = '.'
	}
	fmt.Println(len(obstacles))
}

func load() ([][]byte, error) {
	var raster [][]byte
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		raster = append(raster, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return raster, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	raster, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(raster)
	} else {
		part1(raster)
	}
}
