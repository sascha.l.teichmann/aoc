package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
)

type raster [][]byte
type pt [2]int

func load() (raster, error) {
	var r raster
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		r = append(r, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return r, nil
}

func (r raster) size() (int, int) {
	return len(r[0]), len(r)
}

func (r raster) valid(p pt) bool {
	return p[0] >= 0 && p[0] < len(r[0]) && p[1] >= 0 && p[1] < len(r)
}

func (r raster) get(p pt) byte {
	return r[p[1]][p[0]]
}

func (p pt) add(q pt) pt {
	return pt{p[0] + q[0], p[1] + q[1]}
}

var dirs = []pt{
	{0, -1},
	{+1, 0},
	{0, +1},
	{-1, 0},
}

func part1(r raster) {

	var recurse func(p pt, visited map[pt]bool) int
	recurse = func(p pt, visited map[pt]bool) int {
		v := r.get(p)
		if v == '9' {
			return 1
		}
		sum := 0
		for _, d := range dirs {
			if n := p.add(d); r.valid(n) && !visited[n] && r.get(n) == v+1 {
				visited[n] = true
				sum += recurse(n, visited)
			}
		}
		return sum
	}

	sum := 0
	width, height := r.size()

	for y := range height {
		for x := range width {
			p := pt{x, y}
			if r.get(p) == '0' {
				visited := map[pt]bool{}
				sum += recurse(p, visited)
			}
		}
	}
	fmt.Println(sum)
}

func part2(r raster) {
	heads := map[pt]int{}

	var recurse, descent func(p pt)
	recurse = func(p pt) {
		if r.get(p) == '0' {
			heads[p]++
			return
		}
		descent(p)
	}
	descent = func(p pt) {
		v := r.get(p)
		for _, d := range dirs {
			if n := p.add(d); r.valid(n) && r.get(n) == v-1 {
				recurse(n)
			}
		}
	}

	width, height := r.size()

	for y := range height {
		for x := range width {
			p := pt{x, y}
			if r.get(p) == '9' {
				descent(p)
			}
		}
	}

	sum := 0
	for _, rating := range heads {
		sum += rating
	}
	fmt.Println(sum)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	r, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(r)
	} else {
		part1(r)
	}
}
