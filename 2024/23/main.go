package main

import (
	"bufio"
	"cmp"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strings"
)

func buildGraph(pairs [][2]string) map[string][]string {
	graph := map[string][]string{}
	for _, pair := range pairs {
		graph[pair[0]] = append(graph[pair[0]], pair[1])
		graph[pair[1]] = append(graph[pair[1]], pair[0])
	}
	return graph
}

func part1(pairs [][2]string) {
	graph := buildGraph(pairs)
	unique := map[[3]string]bool{}
	add := func(keys ...string) {
		slices.Sort(keys)
		unique[*(*[3]string)(keys)] = true
	}
	for k, ns1 := range graph {
		for _, n1 := range ns1 {
			ns2 := graph[n1]
			for _, n2 := range ns2 {
				if n2 == k {
					continue
				}
				ns3 := graph[n2]
				for _, n3 := range ns3 {
					if n3 == k {
						add(k, n1, n2)
					}
				}
			}
		}
	}
	count := 0
	for key := range unique {
		for _, s := range key {
			if s[0] == 't' {
				count++
				break
			}
		}
	}
	fmt.Println(count)
}

func part2(pairs [][2]string) {
	graph := buildGraph(pairs)
	if len(graph) == 0 {
		return
	}
	var sets [][]string
	for k := range graph {
		var nsets [][]string
	nextSet:
		for _, set := range sets {
			for _, n := range set {
				if !slices.Contains(graph[n], k) {
					continue nextSet
				}
			}
			ns := append([]string{k}, set...)
			nsets = append(nsets, ns)
		}
		sets = append(sets, nsets...)
		sets = append(sets, []string{k})
	}
	slices.SortFunc(sets, func(a, b []string) int {
		return cmp.Compare(len(b), len(a))
	})
	slices.Sort(sets[0])
	fmt.Println(strings.Join(sets[0], ","))
}

var pairRe = regexp.MustCompile(`([a-z]{2})-([a-z]{2})`)

func load() ([][2]string, error) {
	var pairs [][2]string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if m := pairRe.FindStringSubmatch(line); m != nil {
			pairs = append(pairs, [2]string{m[1], m[2]})
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return pairs, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	pairs, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(pairs)
	} else {
		part1(pairs)
	}
}
