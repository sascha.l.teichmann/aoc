package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func memoize(fn func(int, int) int) func(int, int) int {
	cache := make(map[[2]int]int)
	return func(a, b int) int {
		k := [2]int{a, b}
		if v, ok := cache[k]; ok {
			return v
		}
		v := fn(a, b)
		cache[k] = v
		return v
	}
}

func blinkN(stones []int, blinks int) {
	var recurse func(stone, blinks int) int
	recurse = func(stone, blinks int) int {
		if blinks == 0 {
			return 1
		}
		if stone == 0 {
			return recurse(1, blinks-1)
		}
		num := strconv.Itoa(stone)
		if l := len(num); l%2 == 0 {
			first, _ := strconv.Atoi(num[:l/2])
			second, _ := strconv.Atoi(num[l/2:])
			return recurse(first, blinks-1) + recurse(second, blinks-1)
		}
		return recurse(stone*2024, blinks-1)
	}
	recurse = memoize(recurse)
	sum := 0
	for _, stone := range stones {
		sum += recurse(stone, blinks)
	}
	fmt.Println(sum)
}

func load() ([]int, error) {
	var stones []int
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		fields := strings.Fields(line)
		for _, field := range fields {
			v, err := strconv.Atoi(field)
			if err != nil {
				return nil, err
			}
			stones = append(stones, v)
		}
	}
	return stones, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	stones, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		blinkN(stones, 75)
	} else {
		blinkN(stones, 25)
	}
}
