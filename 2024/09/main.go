package main

import (
	"bufio"
	"bytes"
	"cmp"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

func showBlocks(blocks []int) {
	for _, b := range blocks {
		if b == -1 {
			fmt.Print(".")
		} else {
			fmt.Printf("%d", b)
		}
	}
	fmt.Println()
}

func uncompress(disk []byte) []int {
	var blocks []int
	id, free := 0, false
	for _, v := range disk {
		n := v - '0'
		c := -1
		if !free {
			c, id = id, id+1
		}
		for range n {
			blocks = append(blocks, c)
		}
		free = !free
	}
	return blocks
}

func compact1(blocks []int) {
	free := -1
	for i, id := range blocks {
		if id == -1 {
			free = i
			break
		}
	}
	if free == -1 {
		return
	}
	end := -1
	for i := len(blocks) - 1; i >= 0; i-- {
		if blocks[i] != -1 {
			end = i
			break
		}
	}
	if end == -1 {
		return
	}
	for free < end {
		blocks[free], blocks[end] = blocks[end], -1
		for end--; end >= 0; end-- {
			if blocks[end] != -1 {
				break
			}
		}
		if end < 0 {
			break
		}
		for free++; free < len(blocks); free++ {
			if blocks[free] == -1 {
				break
			}
		}
		if end >= len(blocks) {
			break
		}
	}
}

func freeSpans(blocks []int) [][2]int {
	var free [][2]int
	start := -1
	for i, b := range blocks {
		if b == -1 {
			if start == -1 {
				start = i
			}
		} else {
			if start != -1 {
				free = append(free, [2]int{start, i - 1})
				start = -1
			}
		}
	}
	if start != -1 {
		free = append(free, [2]int{start, len(blocks) - 1})
		start = -1
	}
	return free
}

func compact2(blocks []int) {

	var files [][3]int
	id, start := -1, -1
	for i, b := range blocks {
		if b != id {
			if id != -1 {
				files = append(files, [3]int{id, start, i - 1})
			}
			id = b
			start = i
		}
	}
	if id != -1 {
		files = append(files, [3]int{id, start, len(blocks) - 1})
	}
	//fmt.Printf("%+v\n", files)
	slices.SortFunc(files, func(a, b [3]int) int {
		return cmp.Compare(a[0], b[0])
	})

	//showBlocks(blocks)
	for i := len(files) - 1; i >= 0; i-- {
		file := files[i]
		needed := file[2] - file[1] + 1
		//fmt.Println(needed)
		spans := freeSpans(blocks)
		var free *[2]int
		for j := range spans {
			f := &spans[j]
			if f[1]-f[0]+1 >= needed {
				free = f
				break
			}
		}
		if free == nil || free[0] > file[1] {
			//fmt.Printf("not found %d\n", file[0])
			continue
		}
		// write file in free position
		for j, p := 0, free[0]; j < needed; j++ {
			blocks[j+p] = file[0]
		}
		// empty old block
		for p := file[1]; p <= file[2]; p++ {
			blocks[p] = -1
		}
	}
}

func checksum(blocks []int) int {
	sum := 0
	for i, b := range blocks {
		if b != -1 {
			sum += i * b
		}
	}
	return sum
}

func compactDisks(disks [][]byte, compact func([]int)) {
	for _, disk := range disks {
		// decompress blocks
		blocks := uncompress(disk)
		//showBlocks(blocks)
		compact(blocks)
		//showBlocks(blocks)
		fmt.Println(checksum(blocks))
	}
}

func part1(disks [][]byte) {
	compactDisks(disks, compact1)
}

func part2(disks [][]byte) {
	compactDisks(disks, compact2)
}

func load() ([][]byte, error) {
	var disks [][]byte
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		disks = append(disks, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return disks, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	disks, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(disks)
	} else {
		part1(disks)
	}
}
