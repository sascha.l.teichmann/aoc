package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
)

type raster [][]byte

type pt struct {
	x int
	y int
}

func (r raster) size() (int, int) {
	return len(r[0]), len(r)
}

func (r raster) valid(p pt) bool {
	return p.x >= 0 && p.y >= 0 && p.y < len(r) && p.x < len(r[0])
}

func (r raster) get(p pt) byte {
	return r[p.y][p.x]
}

func (r raster) set(p pt, v byte) {
	r[p.y][p.x] = v
}

func (r raster) find(v byte) (pt, bool) {
	for y, line := range r {
		if x := bytes.IndexByte(line, v); x != -1 {
			return pt{x, y}, true
		}
	}
	return pt{}, false
}

func (p pt) add(q pt) pt {
	return pt{x: p.x + q.x, y: p.y + q.y}
}

func (p pt) neg() pt {
	return pt{-p.y, -p.y}
}

func (p pt) scale(n int) pt {
	return pt{x: n * p.x, y: n * p.y}
}

func (p pt) gps() int {
	return p.y*100 + p.x
}

func (r raster) gps(marker byte) int {
	sum := 0
	width, height := r.size()
	for y := range height {
		for x := range width {
			p := pt{x, y}
			if r.get(p) == marker {
				sum += p.gps()
			}
		}
	}
	return sum
}

var dirs = map[byte]pt{
	'^': {0, -1},
	'>': {+1, 0},
	'v': {0, +1},
	'<': {-1, 0},
}

func (r raster) push(p, dir pt) bool {
	n := p.add(dir)
	if !r.valid(n) {
		return false
	}
	switch r.get(n) {
	case '.':
		r.set(n, 'O')
		return true
	case 'O':
		return r.push(n, dir)
	default:
		return false
	}
}

func part1(r raster, moves []byte) {
	robot, ok := r.find('@')
	if !ok {
		log.Println("robot not found")
		return
	}
	for _, move := range moves {
		dir := dirs[move]
		n := robot.add(dir)
		if !r.valid(n) {
			continue
		}
		if v := r.get(n); v == '.' || (v == 'O' && r.push(n, dir)) {
			r.set(n, '@')
			r.set(robot, '.')
			robot = n
		}
	}
	fmt.Println(r.gps('O'))
}

var replace = map[byte][]byte{
	'#': []byte(`##`),
	'O': []byte(`[]`),
	'.': []byte(`..`),
	'@': []byte(`@.`),
}

func (r raster) scale() raster {
	out := make(raster, 0, len(r))
	for _, row := range r {
		nrow := make([]byte, 0, 2*len(row))
		for _, v := range row {
			nrow = append(nrow, replace[v]...)
		}
		out = append(out, nrow)
	}
	return out
}

func (r raster) show() {
	for _, row := range r {
		fmt.Println(string(row))
	}
}

func (r raster) horizontalPushAllowed(p, dir pt) bool {
	if !r.valid(p) {
		return false
	}
	switch r.get(p) {
	case '.':
		return true
	case '[', ']':
		return r.horizontalPushAllowed(p.add(dir.scale(2)), dir)
	default:
		return false
	}
}

func (r raster) collectHorizontalBoxes(p, dir pt) []pt {
	var boxes []pt
loop:
	for r.valid(p) {
		switch r.get(p) {
		case '[':
			boxes = append(boxes, p)
		case ']':
			boxes = append(boxes, pt{p.x - 1, p.y})
		default:
			break loop
		}
		p = p.add(dir.scale(2))
	}
	return boxes
}

func (r raster) pushHorizontal(p, dir pt) bool {
	if !r.horizontalPushAllowed(p, dir) {
		return false
	}
	boxes := r.collectHorizontalBoxes(p, dir)
	r.moveBoxes(boxes, dir)
	return true
}

func (r raster) verticalPushAllowed(p, dir pt) bool {
	if r.get(p) == ']' {
		p.x--
	}
	for dx := 0; dx <= 1; dx++ {
		n := p.add(dir)
		n.x += dx
		if !r.valid(n) {
			return false
		}
		switch r.get(n) {
		case '#':
			return false
		case '[', ']':
			if !r.verticalPushAllowed(n, dir) {
				return false
			}
		}
	}
	return true
}

func (r raster) collectVerticalBoxes(p, dir pt) []pt {
	boxes := map[pt]bool{}
	var recurse func(p pt)
	recurse = func(p pt) {
		if !r.valid(p) {
			return
		}
		boxes[p] = true
		n1 := p.add(dir)

		if r.valid(n1) {
			switch r.get(n1) {
			case '.':
				m := n1.add(pt{+1, 0})
				if r.valid(m) && r.get(m) == '[' {
					recurse(m)
				}
			case '[':
				recurse(n1)
			case ']':
				recurse(n1.add(pt{-1, 0}))
				m := n1.add(pt{+1, 0})
				if r.valid(m) && r.get(m) == '[' {
					recurse(m)
				}
			}
		}
	}
	recurse(p)
	bs := make([]pt, 0, len(boxes))
	for p := range boxes {
		bs = append(bs, p)
	}
	return bs
}

func (r raster) moveBoxes(boxes []pt, dir pt) {
	// remove old boxes
	for _, box := range boxes {
		r.set(box, '.')
		r.set(pt{box.x + 1, box.y}, '.')
	}
	// draw new boxes
	for _, box := range boxes {
		box = box.add(dir)
		r.set(box, '[')
		r.set(pt{box.x + 1, box.y}, ']')
	}
}

func (r raster) pushVertical(p, dir pt) bool {
	v := r.get(p)
	if v != '[' && v != ']' {
		return false
	}
	if !r.verticalPushAllowed(p, dir) {
		return false
	}
	var boxes []pt
	if v == ']' {
		boxes = r.collectVerticalBoxes(pt{p.x - 1, p.y}, dir)
	} else {
		boxes = r.collectVerticalBoxes(p, dir)
	}
	r.moveBoxes(boxes, dir)
	return true
}

func part2(r raster, moves []byte) {
	r = r.scale()
	robot, ok := r.find('@')
	if !ok {
		log.Println("robot not found")
		return
	}
	fmt.Println("Initial state:")
	r.show()
	fmt.Println()
	for _, move := range moves {
		fmt.Printf("Move %c:\n", move)
		dir := dirs[move]
		n := robot.add(dir)
		if !r.valid(n) {
			r.show()
			continue
		}
		v := r.get(n)
		if v == '#' {
			//r.show()
			continue
		}
		if v == '.' {
			r.set(n, '@')
			r.set(robot, '.')
			robot = n
			//r.show()
			continue
		}
		switch move {
		case '<', '>':
			if r.pushHorizontal(n, dir) {
				r.set(n, '@')
				r.set(robot, '.')
				robot = n
			}
		case '^', 'v':
			if r.pushVertical(n, dir) {
				r.set(n, '@')
				r.set(robot, '.')
				robot = n
			}
		}
		r.show()
	}
	fmt.Println("Final state:")
	r.show()
	fmt.Println(r.gps('['))
}

var (
	rasterRe = regexp.MustCompile(`([#.O@]+)`)
	movesRe  = regexp.MustCompile(`([v\^><]+)`)
)

func load() (raster, []byte, error) {
	var r raster
	var moves []byte
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Bytes()
		if len(line) == 0 {
			continue
		}
		if m := rasterRe.FindSubmatch(line); m != nil {
			r = append(r, bytes.Clone(m[1]))
			continue
		}
		if m := movesRe.FindSubmatch(line); m != nil {
			moves = append(moves, m[1]...)
			continue
		}
		log.Printf("ignoring line %q\n", string(line))
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}
	return r, moves, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	r, moves, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(r, moves)
	} else {
		part1(r, moves)
	}
}
