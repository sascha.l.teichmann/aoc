package main

// This code is machine generated. Modify with care.

func compiledQuine(registers [3]int, code []byte) bool {
	out := code
	a, b, c := registers[0], registers[1], registers[2]
	_, _, _ = a, b, c
label0:
	a /= 1 << 3

	if len(out) == 0 || out[0] != byte(a%8) {
		return false
	}
	out = out[1:]

	if a != 0 {
		goto label0
	}
	return len(out) == 0
}
