package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

func execute(registers [3]int, code []byte, output []byte) []byte {
	for ip := 0; ip >= 0 && ip < len(code); ip += 2 {
		opcode := code[ip]
		literal := int(code[ip+1])
		combo := literal
		switch combo {
		case 4:
			combo = registers[0]
		case 5:
			combo = registers[1]
		case 6:
			combo = registers[2]
		case 7:
			//log.Println("reserved operand")
		}
		switch opcode {
		case 0: // adv
			registers[0] /= 1 << combo
		case 1: // bxl
			registers[1] ^= literal
		case 2: // bst
			registers[1] = combo % 8
		case 3: // jnz
			if registers[0] != 0 {
				ip = literal - 2
			}
		case 4: // bxc
			registers[1] ^= registers[2]
		case 5: // out
			output = append(output, byte(combo%8))
		case 6: // bdv
			registers[1] = registers[0] / (1 << combo)
		case 7: // cdv
			registers[2] = registers[0] / (1 << combo)
		}
	}
	return output
}

func part1(registers [3]int, code []byte) {
	log.Printf("registers: %v\n", registers)
	log.Printf("code: %v\n", code)
	for i, d := range execute(registers, code, nil) {
		if i > 0 {
			fmt.Print(",")
		}
		fmt.Printf("%d", d)
	}
	fmt.Println()
}

func quine(registers [3]int, code []byte) bool {
	out := code
	for ip := 0; ip >= 0 && ip < len(code); ip += 2 {
		opcode := code[ip]
		literal := int(code[ip+1])
		combo := literal
		switch combo {
		case 4:
			combo = registers[0]
		case 5:
			combo = registers[1]
		case 6:
			combo = registers[2]
		case 7:
			//log.Println("reserved operand")
		}
		switch opcode {
		case 0: // adv
			registers[0] /= 1 << combo
		case 1: // bxl
			registers[1] ^= literal
		case 2: // bst
			registers[1] = combo % 8
		case 3: // jnz
			if registers[0] != 0 {
				ip = literal - 2
			}
		case 4: // bxc
			registers[1] ^= registers[2]
		case 5: // out
			if len(out) == 0 || out[0] != byte(combo%8) {
				return false
			}
			out = out[1:]
		case 6: // bdv
			registers[1] = registers[0] / (1 << combo)
		case 7: // cdv
			registers[2] = registers[0] / (1 << combo)
		}
	}
	return len(out) == 0
}

func part2(registers [3]int, code []byte, compiled bool) {

	ranges := make(chan int)

	var minMu sync.Mutex
	minimal := math.MaxInt64

	var wg sync.WaitGroup

	found := func(v int) {
		minMu.Lock()
		defer minMu.Unlock()
		if v < minimal {
			minimal = v
		}
	}

	hasSolution := func() bool {
		minMu.Lock()
		defer minMu.Unlock()
		return minimal != math.MaxInt64
	}

	solution := func() int {
		minMu.Lock()
		defer minMu.Unlock()
		return minimal
	}

	const size = 1_000_000

	eval := quine
	if compiled {
		eval = compiledQuine
	}

	worker := func() {
		defer wg.Done()
		for r := range ranges {
			for end := r + size; r < end; r++ {
				if eval([3]int{r, registers[1], registers[2]}, code) {
					found(r)
					break
				}
			}
		}
	}

	for range max(1, runtime.NumCPU()) {
		wg.Add(1)
		go worker()
	}

	for r := 1; !hasSolution(); r += size {
		ranges <- r
	}
	close(ranges)
	wg.Wait()
	fmt.Println(solution())
}

var (
	registerRe = regexp.MustCompile(`Register (A|B|C): (\d+)`)
	programRe  = regexp.MustCompile(`Program: ([\d,]+)`)
)

func load() ([3]int, []byte, error) {
	var registers [3]int
	var code []byte
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if m := registerRe.FindStringSubmatch(line); m != nil {
			v, _ := strconv.Atoi(m[2])
			switch m[1] {
			case "A":
				registers[0] = v
			case "B":
				registers[1] = v
			case "C":
				registers[2] = v
			}
			continue
		}
		if m := programRe.FindStringSubmatch(line); m != nil {
			fields := strings.Split(m[1], ",")
			for _, field := range fields {
				if field == "" {
					continue
				}
				v, _ := strconv.Atoi(field)
				code = append(code, byte(v))
			}
			continue
		}
	}
	if err := sc.Err(); err != nil {
		return [3]int{}, nil, err
	}
	return registers, code, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	compiled := flag.Bool("compiled", false, "use compiled")
	flag.Parse()
	registers, code, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		start := time.Now()
		part2(registers, code, *compiled)
		log.Printf("took: %v\n", time.Since(start))
	} else {
		part1(registers, code)
	}
}
