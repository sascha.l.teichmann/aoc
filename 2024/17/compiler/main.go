package main

import (
	"bufio"
	"bytes"
	"fmt"
	"go/format"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	header = `package main
// This code is machine generated. Modify with care.

func compiledQuine(registers [3]int, code []byte) bool {
	out := code
	a, b, c := registers[0], registers[1], registers[2]
	_, _, _ = a, b, c
`
	footer = `return len(out) == 0
}
`
)

var programRe = regexp.MustCompile(`Program: ([\d,]+)`)

func combo(v int) string {
	switch v {
	case 0, 1, 2, 3:
		return strconv.Itoa(v)
	case 4:
		return "a"
	case 5:
		return "b"
	case 6:
		return "c"
	default:
		return "77777777"
	}
}

var replaceLabelsRe = regexp.MustCompile(`\$label(\d+)`)

func compile(r io.Reader, w io.Writer) error {

	var out bytes.Buffer

	out.WriteString(header)

	usedLabels := map[int]bool{}

	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		m := programRe.FindStringSubmatch(line)
		if m == nil {
			continue
		}
		var codes []int
		for _, instr := range strings.Split(m[1], ",") {
			if instr == "" {
				continue
			}
			code, _ := strconv.Atoi(instr)
			codes = append(codes, code)
		}
		for ip := 0; ip < len(codes)-1; ip += 2 {
			fmt.Fprintf(&out, "$label%d\n", ip)
			opcode := codes[ip]
			switch opcode {
			case 0:
				fmt.Fprintf(&out, "a /= 1 << %s\n", combo(codes[ip+1]))
			case 1:
				fmt.Fprintf(&out, "b ^= %d\n", codes[ip+1])
			case 2:
				fmt.Fprintf(&out, "b = %s %% 8\n", combo(codes[ip+1]))
			case 3:
				fmt.Fprintf(&out, "if a != 0 {\ngoto label%d\n}\n", codes[ip+1])
				usedLabels[codes[ip+1]] = true
			case 4:
				fmt.Fprintln(&out, "b ^= c")
			case 5:
				fmt.Fprintf(&out, "if len(out) == 0 || out[0] != byte(%s %% 8) {\n",
					combo(codes[ip+1]))
				fmt.Fprintf(&out, "return false\n}\nout = out[1:]\n")
			case 6:
				fmt.Fprintf(&out, "b = a / (1 << %s)\n", combo(codes[ip+1]))
			case 7:
				fmt.Fprintf(&out, "c = a / (1 << %s)\n", combo(codes[ip+1]))
			}
		}
	}
	if err := sc.Err(); err != nil {
		return err
	}

	out.WriteString(footer)

	src := out.Bytes()

	src = replaceLabelsRe.ReplaceAllFunc(src, func(b []byte) []byte {
		m := replaceLabelsRe.FindSubmatch(b)
		label, _ := strconv.Atoi(string(m[1]))
		if usedLabels[label] {
			return []byte(fmt.Sprintf("label%d:", label))
		}
		return nil
	})

	formatted, err := format.Source(src)
	if err != nil {
		return err
	}

	_, err = w.Write(formatted)
	return err
}

func main() {
	if err := compile(os.Stdin, os.Stdout); err != nil {
		log.Fatalln(err)
	}
}
