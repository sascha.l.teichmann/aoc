package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

func part1(rules map[int][]int, updates [][]int) {
	sum := 0
nextUpdate:
	for _, update := range updates {
		printed := map[int]bool{}
		check := func(page int) bool {
			rs := rules[page]
			if len(rs) == 0 {
				return true
			}
			for _, after := range rs {
				if printed[after] {
					return false
				}
			}
			return true
		}
		for _, page := range update {
			if !check(page) {
				continue nextUpdate
			}
			printed[page] = true
		}
		sum += update[len(update)/2]
	}
	fmt.Println(sum)
}

func part2(rules map[int][]int, updates [][]int) {
	sum := 0
	for _, update := range updates {
		before := slices.Clone(update)
		slices.SortStableFunc(update, func(a, b int) int {
			if slices.Contains(rules[a], b) {
				return -1
			}
			if slices.Contains(rules[b], a) {
				return +1
			}
			return 0
		})
		if !slices.Equal(before, update) {
			sum += update[len(update)/2]
		}
	}
	fmt.Println(sum)
}

func load() (map[int][]int, [][]int, error) {
	rules := map[int][]int{}
	var updates [][]int

	ruleRe, err := regexp.Compile(`(\d+)\|(\d+)`)
	if err != nil {
		return nil, nil, err
	}

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if line = strings.TrimSpace(line); line == "" {
			continue
		}
		if m := ruleRe.FindStringSubmatch(line); m != nil {
			p1, err := strconv.Atoi(m[1])
			if err != nil {
				return nil, nil, err
			}
			p2, err := strconv.Atoi(m[2])
			if err != nil {
				return nil, nil, err
			}
			rules[p1] = append(rules[p1], p2)
			continue
		}

		ps := strings.Split(line, ",")
		pages := make([]int, 0, len(ps))
		for _, s := range ps {
			p, err := strconv.Atoi(s)
			if err != nil {
				return nil, nil, err
			}
			pages = append(pages, p)
		}
		updates = append(updates, pages)
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}

	return rules, updates, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	rules, updates, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(rules, updates)
	} else {
		part1(rules, updates)
	}
}
