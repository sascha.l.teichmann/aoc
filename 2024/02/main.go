package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func isSave(report []int) bool {
	sign := 0
	for i := 1; i < len(report); i++ {
		d := report[i-1] - report[i]
		if d == 0 || abs(d) > 3 {
			return true
		}
		switch {
		case sign == 0:
			sign = d
		case sign < 0:
			if d > 0 {
				return true
			}
		default:
			if d < 0 {
				return true
			}
		}
	}
	return false
}

func printSave(reports [][]int, fn func([]int) bool) {
	save := 0
	for _, report := range reports {
		if !fn(report) {
			save++
		}
	}
	fmt.Println(save)
}

func part1(reports [][]int) {
	printSave(reports, isSave)
}

func leaveOneOut(orig []int, fn func([]int) bool) bool {
	if len(orig) == 0 {
		return true
	}
	tmp := make([]int, len(orig)-1)
	for i := range orig {
		pos := 0
		for j, v := range orig {
			if j == i {
				continue
			}
			tmp[pos] = v
			pos++
		}
		if !fn(tmp) {
			return false
		}
	}
	return true
}

func part2(reports [][]int) {
	printSave(reports, func(report []int) bool {
		return leaveOneOut(report, isSave)
	})
}

func load() ([][]int, error) {
	sc := bufio.NewScanner(os.Stdin)
	var reports [][]int
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if line == "" {
			continue
		}
		fields := strings.Fields(line)
		var report []int
		for _, field := range fields {
			v, err := strconv.Atoi(field)
			if err != nil {
				return nil, err
			}
			report = append(report, v)
		}
		reports = append(reports, report)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return reports, nil
}

func main() {
	second := flag.Bool("second", false, "solve part 2")
	flag.Parse()
	reports, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(reports)
	} else {
		part1(reports)
	}
}
