package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"sync"
)

type machine struct {
	a     [2]int
	b     [2]int
	prize [2]int
}

var (
	aRe     = regexp.MustCompile(`Button A: X\+(\d+), Y\+(\d+)`)
	bRe     = regexp.MustCompile(`Button B: X\+(\d+), Y\+(\d+)`)
	prizeRe = regexp.MustCompile(`Prize: X=(\d+), Y=(\d+)`)
)

func load() ([]machine, error) {
	var machines []machine
	sc := bufio.NewScanner(os.Stdin)
	var current machine
	for sc.Scan() {
		line := sc.Text()
		if m := aRe.FindStringSubmatch(line); m != nil {
			current.a[0], _ = strconv.Atoi(m[1])
			current.a[1], _ = strconv.Atoi(m[2])
			continue
		}
		if m := bRe.FindStringSubmatch(line); m != nil {
			current.b[0], _ = strconv.Atoi(m[1])
			current.b[1], _ = strconv.Atoi(m[2])
			continue
		}
		if m := prizeRe.FindStringSubmatch(line); m != nil {
			current.prize[0], _ = strconv.Atoi(m[1])
			current.prize[1], _ = strconv.Atoi(m[2])
			machines = append(machines, current)
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return machines, nil
}

func add(p, q [2]int) [2]int {
	return [2]int{p[0] + q[0], p[1] + q[1]}
}

func memoize(fn func(int, int) int) func(int, int) int {
	cache := make(map[[2]int]int)
	return func(a, b int) int {
		k := [2]int{a, b}
		if v, ok := cache[k]; ok {
			return v
		}
		v := fn(a, b)
		cache[k] = v
		return v
	}
}

func part1(machines []machine) {

	total := 0

	for i := range machines {
		m := &machines[i]

		maxA := m.prize[0]/m.a[0] + 1
		maxB := m.prize[0]/m.b[0] + 1

		minV := math.MaxInt32

	nextA:
		for a := 1; a <= maxA; a++ {
			for b := 1; b <= maxB; b++ {
				x := a*m.a[0] + b*m.b[0]
				y := a*m.a[1] + b*m.b[1]
				if x > m.prize[0] || y > m.prize[1] {
					continue nextA
				}

				if x == m.prize[0] && y == m.prize[1] {
					if n := 3*a + b; n < minV {
						minV = n
					}
					continue nextA
				}
			}
		}
		if minV < math.MaxInt32 {
			total += minV
		}
	}
	fmt.Println(total)
}

func part2(machines []machine) {

	var totalMu sync.Mutex
	total := 0

	jobs := make(chan *machine)

	var wg sync.WaitGroup

	worker := func() {
		defer wg.Done()
		for m := range jobs {
			prizeX := m.prize[0] + 10_000_000_000_000
			prizeY := m.prize[1] + 10_000_000_000_000

			// prizeX = a*m.a[0] + b*m.b[0]
			// b = (prizeX - a*m.a[0])/m.b[0]

			maxA1 := prizeX/m.a[0] + 1
			maxA2 := prizeY/m.a[1] + 1
			maxA := min(maxA1, maxA2)
			minV := math.MaxInt64
			fmt.Println(maxA, maxA2)

			for a := 1; a <= maxA; a++ {
				if a%1_000_000_000 == 0 {
					fmt.Println(a)
				}
				b := (prizeX - a*m.a[0]) / m.b[0]
				b2 := (prizeY - a*m.a[1]) / m.b[1]
				if b != b2 {
					continue
				}
				x := a*m.a[0] + b*m.b[0]
				if x != prizeX {
					continue
				}
				y := a*m.a[1] + b*m.b[1]
				if y != prizeY {
					continue
				}
				if n := 3*a + b; n < minV {
					minV = n
				}
			}

			if minV < math.MaxInt64 {
				totalMu.Lock()
				total += minV
				totalMu.Unlock()
			}
		}
	}

	for n := max(1, min(len(machines), runtime.NumCPU())); n > 0; n-- {
		wg.Add(1)
		go worker()
	}

	for i := range machines {
		jobs <- &machines[i]
	}
	close(jobs)
	wg.Wait()

	fmt.Println(total)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	machines, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(machines)
	} else {
		part1(machines)
	}
}
