package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
)

func nextNumber(n uint64) uint64 {
	a := n * 64
	n ^= a
	n %= 16777216
	b := n / 32
	n ^= b
	n %= 16777216
	c := n * 2048
	n ^= c
	n %= 16777216
	return n
}

func part1(numbers []uint64) {
	sum := uint64(0)
	for _, number := range numbers {
		n := number
		for range 2000 {
			n = nextNumber(n)
		}
		sum += n
	}
	fmt.Println(sum)
}

func buildWindow(prices []byte, index int) ([4]int8, byte) {
	var window [4]int8
	for i := range window {
		window[i] = int8(prices[index+i]) - int8(prices[index+i-1])
	}
	return window, prices[index+3]
}

func part2(numbers []uint64) {
	prices := make([][]byte, 0, len(numbers))
	for _, number := range numbers {
		n := number
		ps := make([]byte, 1, 2001)
		ps[0] = byte(n % 10)
		for range 2000 {
			n = nextNumber(n)
			ps = append(ps, byte(n%10))
		}
		prices = append(prices, ps)
	}

	windows := make([]map[[4]int8]byte, len(prices))
	for i, ps := range prices {
		windows[i] = make(map[[4]int8]byte)
		for j := 1; j < len(ps)-3; j++ {
			win, p := buildWindow(ps, j)
			if _, ok := windows[i][win]; !ok {
				windows[i][win] = p
			}
		}
	}

	highest := 0
	checked := map[[4]int8]bool{}
	for _, window := range windows {
		for win := range window {
			if checked[win] {
				continue
			}
			sum := 0
			for _, other := range windows {
				sum += int(other[win])
			}
			if sum > highest {
				highest = sum
			}
			checked[win] = true
		}
	}
	fmt.Println(highest)
}

func load() ([]uint64, error) {
	var numbers []uint64
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		number, err := strconv.ParseUint(line, 10, 64)
		if err != nil {
			return nil, err
		}
		numbers = append(numbers, number)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return numbers, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	numbers, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(numbers)
	} else {
		part1(numbers)
	}
}
