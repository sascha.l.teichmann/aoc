package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

type raster [][]byte

type pt struct{ x, y int }

func (r raster) find(v byte) (pt, bool) {
	for y, row := range r {
		if x := slices.Index(row, v); x != -1 {
			return pt{x, y}, true
		}
	}
	return pt{}, false
}

func (r raster) valid(p pt) bool {
	x, y := p.x, p.y
	return x >= 0 && y >= 0 && y < len(r) && x < len(r[y])
}

func (r raster) get(p pt) byte {
	return r[p.y][p.x]
}

func (p pt) add(q pt) pt {
	return pt{p.x + q.x, p.y + q.y}
}

var dirs = []pt{
	{0, -1},
	{+1, 0},
	{0, +1},
	{-1, 0},
}

func (r raster) tracePath(start, end pt) ([]pt, map[pt]int) {
	trace := []pt{start}
	visted := map[pt]int{start: 0}
next:
	for curr := start; curr != end; {
		for _, d := range dirs {
			n := curr.add(d)
			if r.valid(n) && (r.get(n) == '.' || r.get(n) == 'E') && visted[n] == 0 {
				visted[n] = len(trace)
				trace = append(trace, n)
				curr = n
				continue next
			}
		}
		break
	}
	return trace, visted
}

func part1(r raster) {
	start, ok := r.find('S')
	if !ok {
		log.Println("no start found")
		return
	}
	end, ok := r.find('E')
	if !ok {
		log.Println("no end found")
		return
	}

	trace, visted := r.tracePath(start, end)

	log.Printf("len trace: %d\n", len(trace))
	saves := map[int]int{}
	for i, p := range trace {
		for _, d := range dirs {
			n := p.add(d)
			if !r.valid(n) || r.get(n) != '#' {
				continue
			}
			m := n.add(d)
			if !r.valid(m) {
				continue
			}
			if track, ok := visted[m]; ok && track > i {
				saves[track-i]++
			}
		}
	}
	sum := 0
	for i, s := range saves {
		if i-2 >= 100 {
			sum += s
		}
	}
	fmt.Println(sum)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func part2(r raster) {
	start, ok := r.find('S')
	if !ok {
		log.Println("no start found")
		return
	}
	end, ok := r.find('E')
	if !ok {
		log.Println("no end found")
		return
	}

	trace, visted := r.tracePath(start, end)

	const radius = 20

	sum := 0

	for dy := -radius; dy <= radius; dy++ {
		for dx := -radius; dx <= radius; dx++ {
			dist := abs(dx) + abs(dy)
			if dist < 2 || dist > radius {
				continue
			}
			d := pt{dx, dy}
			for i, p := range trace {
				n := p.add(d)
				if v, ok := visted[n]; ok && v-i-dist >= 100 {
					sum++
				}

			}
		}
	}
	fmt.Println(sum)
}

func load() (raster, error) {
	var r raster
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		r = append(r, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return r, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	r, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(r)
	} else {
		part1(r)
	}
}
