package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

var opsPart1 = []func(a, b int) int{
	func(a, b int) int { return a + b },
	func(a, b int) int { return a * b },
}

var opsPart2 = []func(a, b int) int{
	func(a, b int) int { return a + b },
	func(a, b int) int { return a * b },
	func(a, b int) int {
		s := fmt.Sprintf("%d%d", a, b)
		c, _ := strconv.Atoi(s)
		return c
	},
}

func check(eq []int, ops []func(a, b int) int) bool {
	var recurse func(before int, rest []int) bool
	recurse = func(before int, rest []int) bool {
		if len(rest) == 0 {
			return eq[0] == before
		}
		for _, op := range ops {
			if recurse(op(before, rest[0]), rest[1:]) {
				return true
			}
		}
		return false
	}
	return recurse(eq[1], eq[2:])
}

func run(equations [][]int, ops []func(a, b int) int) {
	sum := 0
	for _, eq := range equations {
		if check(eq, ops) {
			sum += eq[0]
		}
	}
	fmt.Println(sum)
}

func part1(equations [][]int) { run(equations, opsPart1) }
func part2(equations [][]int) { run(equations, opsPart2) }

func load() ([][]int, error) {
	sc := bufio.NewScanner(os.Stdin)
	var equations [][]int
	for sc.Scan() {
		line := sc.Text()
		result, operants, ok := strings.Cut(line, ":")
		if !ok {
			continue
		}
		ops := strings.Fields(operants)
		eq := make([]int, 1, len(ops)+1)
		r, err := strconv.Atoi(result)
		if err != nil {
			return nil, err
		}
		eq[0] = r
		for _, op := range ops {
			o, err := strconv.Atoi(op)
			if err != nil {
				return nil, err
			}
			eq = append(eq, o)
		}
		equations = append(equations, eq)
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return equations, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	equations, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(equations)
	} else {
		part1(equations)
	}
}
