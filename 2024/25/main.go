package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
)

func part1(keys, locks [][5]byte) {
	fit := 0
	for _, key := range keys {
	nextLock:
		for _, lock := range locks {
			for i, kv := range key {
				if kv+lock[i] > 5 {
					continue nextLock
				}
			}
			fit++
		}
	}
	fmt.Println(fit)
}

func part2(_, _ [][5]byte) {
	fmt.Println("Nothing to do")
}

func load() ([][5]byte, [][5]byte, error) {
	var keys, locks [][5]byte
	sc := bufio.NewScanner(os.Stdin)
	var block []string

	parseBlock := func() {
		if len(block) == 0 {
			return
		}
		var current [5]byte
		for i := 1; i < min(6, len(block)); i++ {
			line := block[i]
			for j := range min(5, len(line)) {
				if line[j] == '#' {
					current[j]++
				}
			}
		}
		if block[0] == "#####" {
			locks = append(locks, current)
		} else {
			keys = append(keys, current)
		}
		block = nil
	}

	for sc.Scan() {
		if line := sc.Text(); line == "" {
			parseBlock()
		} else {
			block = append(block, line)
		}
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}
	parseBlock()

	return keys, locks, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	keys, locks, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(keys, locks)
	} else {
		part1(keys, locks)
	}
}
