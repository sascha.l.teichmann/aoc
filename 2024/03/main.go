package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var (
	mulRe = regexp.MustCompile(`mul\((\d{1,3}),(\d{1,3})\)`)
	doRe  = regexp.MustCompile(`(do|don't)\(\)`)
)

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func part1(input string) {
	sum := 0
	for _, m := range mulRe.FindAllStringSubmatch(input, -1) {
		a, err := strconv.Atoi(m[1])
		check(err)
		b, err := strconv.Atoi(m[2])
		sum += a * b
	}
	fmt.Println(sum)
}

func part2(input string) {
	rest := input
	var parts []string
	enabled := true
	for {
		fields := doRe.Split(rest, 2)
		if len(fields) < 2 {
			break
		}
		lead := len(fields[0])
		if enabled {
			parts = append(parts, fields[0])
		}
		enabled = strings.HasPrefix(rest[lead:], "do()")
		rest = fields[1]
	}
	if enabled {
		parts = append(parts, rest)
	}
	part1(strings.Join(parts, ""))
}

func main() {
	second := flag.Bool("second", false, "runs part 2")
	flag.Parse()
	input, err := io.ReadAll(os.Stdin)
	check(err)
	if *second {
		part2(string(input))
	} else {
		part1(string(input))
	}
}
