package main

import (
	"bufio"
	"cmp"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
)

func part1(lines [][2]int) {
	indices1 := make([]int, len(lines))
	indices2 := make([]int, len(lines))
	for i := range lines {
		indices1[i] = i
		indices2[i] = i
	}
	slices.SortFunc(indices1, func(a, b int) int {
		return cmp.Compare(lines[a][0], lines[b][0])
	})
	slices.SortFunc(indices2, func(a, b int) int {
		return cmp.Compare(lines[a][1], lines[b][1])
	})
	sum := 0
	for i := range lines {
		d := lines[indices1[i]][0] - lines[indices2[i]][1]
		if d < 0 {
			d = -d
		}
		sum += d
	}
	fmt.Println(sum)
}

func part2(lines [][2]int) {
	counts := make(map[int]int, len(lines))
	for _, pair := range lines {
		counts[pair[1]]++
	}
	sum := 0
	for _, pair := range lines {
		sum += pair[0] * counts[pair[0]]
	}
	fmt.Println(sum)
}

func load() ([][2]int, error) {
	sc := bufio.NewScanner(os.Stdin)
	var lines [][2]int
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if line == "" {
			continue
		}
		fields := strings.Fields(line)
		if len(fields) < 2 {
			return nil, errors.New("not enough fields")
		}
		n1, err1 := strconv.Atoi(fields[0])
		n2, err2 := strconv.Atoi(fields[1])
		if err := errors.Join(err1, err2); err != nil {
			return nil, err
		}
		lines = append(lines, [2]int{n1, n2})
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

func main() {
	second := flag.Bool("second", false, "solve part 2")
	flag.Parse()
	lines, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(lines)
	} else {
		part1(lines)
	}
}
