package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
)

func memoize[T any](fn func(string) T) func(string) T {
	cache := map[string]T{}
	return func(s string) T {
		if v, ok := cache[s]; ok {
			return v
		}
		v := fn(s)
		cache[s] = v
		return v
	}
}

func first(patterns []string) map[byte][]string {
	hash := map[byte][]string{}
	for _, pattern := range patterns {
		hash[pattern[0]] = append(hash[pattern[0]], pattern)
	}
	return hash
}

func part1(patterns, designs []string) {
	hash := first(patterns)

	var constructable func(string) bool
	constructable = func(rest string) bool {
		if rest == "" {
			return true
		}
		for _, check := range hash[rest[0]] {
			if strings.HasPrefix(rest, check) && constructable(rest[len(check):]) {
				return true
			}
		}
		return false
	}
	constructable = memoize(constructable)

	sum := 0
	for _, design := range designs {
		if constructable(design) {
			sum++
		}
	}
	fmt.Println(sum)
}

func part2(patterns, designs []string) {
	hash := first(patterns)

	var constructable func(string) int
	constructable = func(rest string) int {
		if rest == "" {
			return 1
		}
		sum := 0
		for _, check := range hash[rest[0]] {
			if strings.HasPrefix(rest, check) {
				sum += constructable(rest[len(check):])
			}
		}
		return sum
	}
	constructable = memoize(constructable)

	sum := 0
	for _, design := range designs {
		sum += constructable(design)
	}
	fmt.Println(sum)
}

func load() ([]string, []string, error) {
	var patterns, designs []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if line == "" {
			continue
		}
		if strings.ContainsRune(line, ',') {
			for _, pattern := range strings.Split(line, ",") {
				pattern = strings.TrimSpace(pattern)
				patterns = append(patterns, pattern)
			}
		} else {
			designs = append(designs, line)
		}
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}
	return patterns, designs, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	patterns, designs, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(patterns, designs)
	} else {
		part1(patterns, designs)
	}
}
