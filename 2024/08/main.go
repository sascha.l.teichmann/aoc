package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
)

type grid [][]byte

func (g grid) antennas() map[byte][][2]int {
	ants := map[byte][][2]int{}
	for y, line := range g {
		for x, v := range line {
			if v != '.' && v != '#' {
				ants[v] = append(ants[v], [2]int{x, y})
			}
		}
	}
	return ants
}

func (g grid) inside(p [2]int) bool {
	return len(g) > 0 && p[0] >= 0 && p[0] < len(g[0]) && p[1] >= 0 && p[1] < len(g)
}

func (g grid) get(p [2]int) byte {
	return g[p[1]][p[0]]
}

func part1(g grid) {
	ants := g.antennas()
	count := 0
	for _, list := range ants {
		if len(list) < 2 {
			continue
		}
		for i, p1 := range list {
			for _, p2 := range list[i+1:] {
				dx := p2[0] - p1[0]
				dy := p2[1] - p1[1]
				p3 := [2]int{p2[0] + dx, p2[1] + dy}
				p4 := [2]int{p1[0] - dx, p1[1] - dy}
				if g.inside(p3) && g.get(p3) == '.' {
					count++
				}
				if g.inside(p4) && g.get(p4) == '.' {
					count++
				}
			}
		}
	}
	fmt.Println(count)
}

func part2(g grid) {
	ants := g.antennas()
	anti := map[[2]int]bool{}
	for _, list := range ants {
		if len(list) < 2 {
			continue
		}
		for i, p1 := range list {
			for _, p2 := range list[i+1:] {
				dx := p2[0] - p1[0]
				dy := p2[1] - p1[1]
				for p := p2; g.inside(p); p[0], p[1] = p[0]+dx, p[1]+dy {
					anti[p] = true
				}
				for p := p1; g.inside(p); p[0], p[1] = p[0]-dx, p[1]-dy {
					anti[p] = true
				}
			}
		}
	}
	fmt.Println(len(anti))
}

func load() (grid, error) {
	var g grid
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		g = append(g, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return g, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	g, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(g)
	} else {
		part1(g)
	}
}
