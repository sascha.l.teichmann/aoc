package main

import (
	"bufio"
	"cmp"
	"container/list"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"slices"
	"strconv"
)

type pt struct{ x, y int16 }

type costsPt struct {
	p     pt
	costs int32
}

func (p pt) add(q pt) pt {
	return pt{p.x + q.x, p.y + q.y}
}

func abs(x int16) int16 {
	if x < 0 {
		return -x
	}
	return x
}

func (p pt) dist(q pt) int16 {
	return abs(p.x-q.x) + abs(p.y-q.y)
}

var dirs = []pt{
	{0, -1},
	{+1, 0},
	{0, +1},
	{-1, 0},
}

func simulate(corrupted []pt, size int16) int32 {
	broken := map[pt]bool{}
	visited := map[pt]bool{}

	aim := pt{size - 1, size - 1}

	succs := func(p pt, fn func(pt)) {
		var next []pt
		for _, d := range dirs {
			n := p.add(d)
			if n.x < 0 || n.y < 0 || n.x >= size || n.y >= size {
				continue
			}
			if !broken[n] && !visited[n] {
				next = append(next, n)
			}
		}
		slices.SortFunc(next, func(a, b pt) int {
			da := aim.dist(a)
			db := aim.dist(b)
			return cmp.Compare(da, db)
		})
		for _, n := range next {
			fn(n)
		}
	}

	for _, p := range corrupted {
		broken[p] = true
	}

	open := list.New()
	open.PushBack(&costsPt{})
	visited[pt{}] = true

	for open.Len() > 0 {
		current := open.Remove(open.Front()).(*costsPt)
		if current.p == aim {
			return current.costs
		}
		succs(current.p, func(n pt) {
			visited[n] = true
			open.PushBack(&costsPt{
				p:     n,
				costs: current.costs + 1,
			})
		})
	}
	return -1
}

func part1(corrupted []pt, size int16, num int) {
	num = min(len(corrupted), num)
	fmt.Println(simulate(corrupted[:num], size))
}

func part2(corrupted []pt, size int16) {
	i := len(corrupted)
	for ; i >= 0; i-- {
		if simulate(corrupted[:i], size) != -1 {
			break
		}
	}
	if i < len(corrupted) {
		p := corrupted[i]
		fmt.Printf("%d,%d\n", p.x, p.y)
	}
}

var coordRe = regexp.MustCompile(`(\d+),(\d+)`)

func load() ([]pt, error) {
	var corrupted []pt
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if m := coordRe.FindStringSubmatch(line); m != nil {
			x, _ := strconv.Atoi(m[1])
			y, _ := strconv.Atoi(m[2])
			corrupted = append(corrupted, pt{int16(x), int16(y)})
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return corrupted, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	size := flag.Int("size", 71, "grid size")
	num := flag.Int("num", 1024, "number of corrupted lines")
	flag.Parse()
	corrupted, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(corrupted, int16(*size))
	} else {
		part1(corrupted, int16(*size), *num)
	}
}
