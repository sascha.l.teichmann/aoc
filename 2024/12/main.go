package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

type raster [][]byte

type pt [2]int

func (r raster) valid(p pt) bool {
	return p[0] >= 0 && p[1] >= 0 && p[1] < len(r) && p[0] < len(r[p[1]])
}

func (r raster) get(p pt) byte {
	return r[p[1]][p[0]]
}

func (r raster) size() (int, int) {
	return len(r[0]), len(r)
}

func (p pt) add(q pt) pt {
	return pt{p[0] + q[0], p[1] + q[1]}
}

func (p pt) neg() pt {
	return pt{-p[0], -p[1]}
}

var dirs = []pt{
	{0, -1},
	{+1, 0},
	{0, +1},
	{-1, 0},
}

func load() (raster, error) {
	var r raster
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		r = append(r, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return r, nil
}

func part1(r raster) {
	width, height := r.size()
	visited := map[pt]bool{}
	sum := 0
	for y := range height {
		for x := range width {
			p := pt{x, y}
			if visited[p] {
				continue
			}
			v := r.get(p)
			area, fence := 1, 0
			var recurse func(p pt)
			recurse = func(p pt) {
				for _, d := range dirs {
					n := p.add(d)
					if !r.valid(n) || r.get(n) != v {
						fence++
						continue
					}
					if visited[n] {
						continue
					}
					area++
					visited[n] = true
					recurse(n)
				}
			}
			visited[p] = true
			recurse(p)
			sum += area * fence
		}
	}
	fmt.Println(sum)
}

func countSides(fence map[pt][]pt) int {
	count := 0
	for d, segs := range fence {
		forw := pt{d[1], d[0]}
		back := forw.neg()
		deltas := []pt{forw, back}
		visited := make([]bool, len(segs))
		for i, seg := range segs {
			if visited[i] {
				continue
			}
			visited[i] = true
			count++
			for _, delta := range deltas {
				for p := seg.add(delta); ; p = p.add(delta) {
					idx := slices.Index(segs, p)
					if idx == -1 || visited[idx] {
						break
					}
					visited[idx] = true
				}
			}
		}
	}
	return count
}

func part2(r raster) {
	width, height := r.size()
	visited := map[pt]bool{}
	sum := 0
	for y := range height {
		for x := range width {
			p := pt{x, y}
			if visited[p] {
				continue
			}
			v := r.get(p)
			area := 1
			fence := map[pt][]pt{}
			var recurse func(p pt)
			recurse = func(p pt) {
				for _, d := range dirs {
					n := p.add(d)
					if !r.valid(n) || r.get(n) != v {
						fence[d] = append(fence[d], n)
						continue
					}
					if visited[n] {
						continue
					}
					area++
					visited[n] = true
					recurse(n)
				}
			}
			visited[p] = true
			recurse(p)
			sides := countSides(fence)
			sum += area * sides
		}
	}
	fmt.Println(sum)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	r, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(r)
	} else {
		part1(r)
	}
}
