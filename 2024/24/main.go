package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"maps"
	"os"
	"regexp"
	"slices"
	"strconv"
	"strings"
)

type gate struct {
	in1, in2 string
	op       string
	out      string
}

func (g *gate) operate(a, b int64) int64 {
	switch g.op {
	case "AND":
		return a & b
	case "OR":
		return a | b
	case "XOR":
		return a ^ b
	}
	panic("unknown operator")
}

func part1(wires map[string]int64, gates []*gate) {
	for len(gates) > 0 {
		g := gates[0]
		gates[0] = nil
		gates = gates[1:]
		a, ok1 := wires[g.in1]
		b, ok2 := wires[g.in2]
		if ok1 && ok2 {
			wires[g.out] = g.operate(a, b)
		} else {
			gates = append(gates, g)
		}
	}
	var bits []string
	for _, k := range slices.Sorted(maps.Keys(wires)) {
		if k[0] == 'z' {
			bit := "0"
			if wires[k] != 0 {
				bit = "1"
			}
			bits = append([]string{bit}, bits...)
		}
	}
	out, _ := strconv.ParseInt(strings.Join(bits, ""), 2, 64)
	fmt.Println(out)
}

func isTerm(s string) bool {
	return s[0] == 'x' || s[0] == 'y' || s[0] == 'z'
}

func part2(_ map[string]int64, gates []*gate) {
	highest := "z00"
	for _, g := range gates {
		if g.out[0] == 'z' {
			h, _ := strconv.ParseInt(highest[1:], 10, 64)
			n, _ := strconv.ParseInt(g.out[1:], 10, 64)
			if n > h {
				highest = g.out
			}
		}
	}
	wrong := map[string]bool{}
	for _, g := range gates {
		if g.out[0] == 'z' && g.op != "XOR" && g.out != highest {
			wrong[g.out] = true
		}
		if g.op == "XOR" && !isTerm(g.in1) && !isTerm(g.in2) && !isTerm(g.out) {
			wrong[g.out] = true
		}
		if g.op == "AND" && !("x00" == g.in1 || "x00" == g.in2) {
			for _, g2 := range gates {
				if (g.out == g2.in1 || g.out == g2.in2) && g2.op != "OR" {
					wrong[g.out] = true
				}
			}
		}
		if g.op == "XOR" {
			for _, g2 := range gates {
				if (g.out == g2.in1 || g.out == g2.in2) && g2.op == "OR" {
					wrong[g.out] = true
				}
			}
		}
	}
	fmt.Println(strings.Join(slices.Sorted(maps.Keys(wrong)), ","))
}

var setRe = regexp.MustCompile(`([[:alnum:]]{3}): (0|1)`)
var opRe = regexp.MustCompile(`([[:alnum:]]{3}) (AND|OR|XOR) ([[:alnum:]]{3}) -> ([[:alnum:]]{3})`)

func load() (map[string]int64, []*gate, error) {
	wires := map[string]int64{}
	var gates []*gate

	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if line == "" {
			continue
		}
		if m := setRe.FindStringSubmatch(line); m != nil {
			wires[m[1]], _ = strconv.ParseInt(m[2], 10, 64)
			continue
		}
		if m := opRe.FindStringSubmatch(line); m != nil {
			g := &gate{
				in1: m[1],
				in2: m[3],
				op:  m[2],
				out: m[4],
			}
			gates = append(gates, g)
			continue
		}
		log.Printf("unknown line: %q\n", line)
	}
	if err := sc.Err(); err != nil {
		return nil, nil, err
	}

	return wires, gates, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	wires, roots, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(wires, roots)
	} else {
		part1(wires, roots)
	}
}
