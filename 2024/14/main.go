package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)

const (
	width  = 101
	height = 103
)

type pt [2]int

type robot struct {
	pos pt
	vel pt
}

func (r *robot) move() {
	r.pos = r.pos.add(r.vel).clip()
}

func (p pt) add(q pt) pt {
	return pt{p[0] + q[0], p[1] + q[1]}
}

func (p pt) clip() pt {
	for p[0] < 0 {
		p[0] += width
	}
	for p[0] >= width {
		p[0] -= width
	}
	for p[1] < 0 {
		p[1] += height
	}
	for p[1] >= height {
		p[1] -= height
	}
	return p
}

func part1(robots []robot) {

	const seconds = 100

	for range seconds {
		for i := range robots {
			robots[i].move()
		}
	}
	var quads [4]int
	for i := range robots {
		r := &robots[i]
		if r.pos[0] == width/2 || r.pos[1] == height/2 {
			continue
		}
		var code int
		if r.pos[0] > width/2 {
			code = 1
		}
		if r.pos[1] > height/2 {
			code |= 2
		}
		quads[code]++
	}
	prod := 1
	for _, v := range quads {
		prod *= v
	}
	fmt.Println(prod)
}

func displayTree(robots []robot) bool {
	raster := make([]byte, width*height)
	for i := range raster {
		raster[i] = ' '
	}
	for i := range robots {
		r := &robots[i]
		raster[r.pos[1]*width+r.pos[0]] = '*'
	}
	found := false
	marker := []byte(`*******************************`)
	for rest := raster; len(rest) > 0; rest = rest[width:] {
		if bytes.Contains(rest[:width], marker) {
			found = true
		}
	}
	if found {
		for rest := raster; len(rest) > 0; rest = rest[width:] {
			fmt.Println(string(rest[:width]))
		}
	}
	return found
}

func part2(robots []robot) {
	for j := 1; ; j++ {
		for i := range robots {
			robots[i].move()
		}
		if displayTree(robots) {
			fmt.Println(j)
			break
		}
	}
}

var robotRe = regexp.MustCompile(`p=(\d+),(\d+) v=(-?\d+),(-?\d+)`)

func load() ([]robot, error) {
	var robots []robot
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		line := sc.Text()
		if m := robotRe.FindStringSubmatch(line); m != nil {
			x, _ := strconv.Atoi(m[1])
			y, _ := strconv.Atoi(m[2])
			vx, _ := strconv.Atoi(m[3])
			vy, _ := strconv.Atoi(m[4])
			robots = append(robots, robot{
				pos: pt{x, y},
				vel: pt{vx, vy},
			})
		}
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return robots, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	robots, err := load()
	if err != nil {
		log.Fatalln(robots)
	}
	if *second {
		part2(robots)
	} else {
		part1(robots)
	}
}
