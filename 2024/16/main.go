package main

import (
	"bufio"
	"bytes"
	"container/heap"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

type raster [][]byte

type pt struct {
	x int
	y int
}

type dir int

const (
	east dir = iota
	south
	west
	north
)

type position struct {
	pos  pt
	cost int
	dir  dir
}

type positions []*position

var deltas = []pt{
	east:  {+1, 0},
	south: {0, +1},
	west:  {-1, 0},
	north: {0, -1},
}

func (p *positions) Push(x any) {
	pos := x.(*position)
	*p = append(*p, pos)
}

func (p *positions) Pop() any {
	last := len(*p) - 1
	x := (*p)[last]
	(*p)[last] = nil
	*p = (*p)[:last]
	return x
}

func (p *positions) Len() int {
	return len(*p)
}

func (p *positions) Less(i, j int) bool {
	return (*p)[i].cost < (*p)[j].cost
}

func (p *positions) Swap(i, j int) {
	(*p)[i], (*p)[j] = (*p)[j], (*p)[i]
}

func (p pt) add(q pt) pt {
	return pt{p.x + q.x, p.y + q.y}
}

func (r raster) find(v byte) (pt, bool) {
	for y, row := range r {
		if x := slices.Index(row, v); x != -1 {
			return pt{x, y}, true
		}
	}
	return pt{}, false
}

func (r raster) valid(p pt) bool {
	return p.y >= 0 && p.y <= len(r) && p.x >= 0 && p.x <= len(r[0])
}

func (r raster) get(p pt) byte {
	return r[p.y][p.x]
}

func (r raster) succs(p *position, fn func(pt, dir, int)) {
	cont := p.dir
	prev := (cont + 3) % 4
	next := (cont + 1) % 4
	contPt := p.pos.add(deltas[cont])
	prevPt := p.pos.add(deltas[prev])
	nextPt := p.pos.add(deltas[next])
	if r.valid(contPt) && r.get(contPt) != '#' {
		fn(contPt, cont, 1)
	}
	if r.valid(prevPt) && r.get(prevPt) != '#' {
		fn(prevPt, prev, 1001)
	}
	if r.valid(nextPt) && r.get(nextPt) != '#' {
		fn(nextPt, next, 1001)
	}
}

func part1(r raster) {
	start, ok := r.find('S')
	if !ok {
		log.Println("cannot find start")
		return
	}
	end, ok := r.find('E')
	if !ok {
		log.Println("cannot find end")
		return
	}

	visited := map[pt]bool{}

	open := &positions{&position{
		pos: start,
		dir: east,
	}}

	heap.Init(open)

	for open.Len() > 0 {
		current := heap.Pop(open).(*position)
		if current.pos == end {
			fmt.Println(current.cost)
			break
		}
		visited[current.pos] = true
		r.succs(current, func(succ pt, dir dir, cost int) {
			if visited[succ] {
				return
			}
			ncost := current.cost + cost
			if idx := slices.IndexFunc(*open, func(p *position) bool {
				return p.pos == succ
			}); idx != -1 {
				if p := (*open)[idx]; p.cost > ncost {
					p.cost = ncost
					p.dir = dir
					heap.Fix(open, idx)
				}
				return
			}
			heap.Push(open, &position{
				pos:  succ,
				cost: ncost,
				dir:  dir,
			})
		})
	}
}

func part2(r raster) {
}

func load() (raster, error) {
	var r raster
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		r = append(r, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return r, nil
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	r, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(r)
	} else {
		part1(r)
	}
}
