package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
)

func load() ([][]byte, error) {
	sc := bufio.NewScanner(os.Stdin)
	var raster [][]byte
	for sc.Scan() {
		raster = append(raster, bytes.Clone(sc.Bytes()))
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return raster, nil
}

func part1(raster [][]byte) {
	/*
	   \|/
	   - -
	   /|\
	*/
	var dirs [][]int
	for dx := -1; dx <= +1; dx++ {
		for dy := -1; dy <= +1; dy++ {
			if dx == 0 && dy == 0 {
				continue
			}
			dirs = append(dirs, []int{dx, dy})
		}
	}

	xmas := []byte("XMAS")
	found := make(map[[4]int]bool)

	for y, line := range raster {
		for x, v := range line {
			if v != 'X' {
				continue
			}
		nextDir:
			for _, dir := range dirs {
				px, py := x, y
				var trace [4]int
				for i, c := range xmas {
					if px < 0 || px >= len(line) || py < 0 || py >= len(raster) ||
						c != raster[py][px] {
						continue nextDir
					}
					trace[i] = py*len(line) + px
					px += dir[0]
					py += dir[1]
				}
				slices.Sort(trace[:])
				found[trace] = true
			}
		}
	}
	fmt.Println(len(found))
}

func part2(raster [][]byte) {
	ofs := [2][2][2]int{
		{
			{-1, -1},
			{+1, +1},
		},
		{
			{+1, -1},
			{-1, +1},
		},
	}
	ms := []byte(`MS`)
	sum := 0
	for y := 1; y < len(raster)-1; y++ {
		line := raster[y]
		for x := 1; x < len(line)-1; x++ {
			if line[x] != 'A' {
				continue
			}
			count := 0
			for _, pairs := range ofs {
				p1, p2 := pairs[0], pairs[1]
				for i := 0; i < 2; i++ {
					m, s := ms[i], ms[(i+1)%2]
					if raster[y+p1[1]][x+p1[0]] == m &&
						raster[y+p2[1]][x+p2[0]] == s {
						count++
					}
				}
			}
			if count > 1 {
				sum++
			}
		}
	}
	fmt.Println(sum)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	raster, err := load()
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
	if *second {
		part2(raster)
	} else {
		part1(raster)
	}
}
