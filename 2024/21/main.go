package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
	"unicode"
)

var numKeys = map[byte][2]int{
	'7': {0, 0},
	'8': {1, 0},
	'9': {2, 0},
	'4': {0, 1},
	'5': {1, 1},
	'6': {2, 1},
	'1': {0, 2},
	'2': {1, 2},
	'3': {2, 2},
	' ': {0, 3},
	'0': {1, 3},
	'A': {2, 3},
}

var dirKeys = map[byte][2]int{
	' ': {0, 0},
	'^': {1, 0},
	'A': {2, 0},
	'<': {0, 1},
	'v': {1, 1},
	'>': {2, 1},
}

type keypadFn func(curr, target byte) string

func keypad(keys map[byte][2]int) keypadFn {
	return func(curr, target byte) string {
		cp := keys[curr]
		tp := keys[target]
		rd := tp[1] - cp[1]
		cd := tp[0] - cp[0]

		var cm string
		if cd > 0 {
			cm = strings.Repeat(">", cd)
		} else {
			cm = strings.Repeat("<", -cd)
		}
		var rm string
		if rd > 0 {
			rm = strings.Repeat("v", rd)
		} else {
			rm = strings.Repeat("^", -rd)
		}

		switch hole := keys[' ']; {
		case tp[1] == hole[1] && cp[0] == hole[0]:
			return cm + rm
		case cp[1] == hole[1] && tp[0] == hole[0]:
			return rm + cm
		case strings.ContainsRune(cm, '<'):
			return cm + rm
		default:
			return rm + cm
		}
	}
}

var (
	numericPad = keypad(numKeys)
	dirPad     = keypad(dirKeys)
)

func memoize(fn func(a, b byte, c int) int) func(a, b byte, c int) int {
	type key struct {
		a, b byte
		c    int
	}
	cache := map[key]int{}
	return func(a, b byte, c int) int {
		k := key{a, b, c}
		if v, ok := cache[k]; ok {
			return v
		}
		v := fn(a, b, c)
		cache[k] = v
		return v
	}
}

func keypads(code string, pads []keypadFn) int {

	var presses func(curr, target byte, level int) int
	presses = func(curr, target byte, level int) int {
		seq := pads[level](curr, target) + "A"
		if level == len(pads)-1 {
			return len(seq)
		}
		l := 0
		c := byte('A')
		for i := 0; i < len(seq); i++ {
			t := seq[i]
			l += presses(c, t, level+1)
			c = t
		}
		return l
	}
	presses = memoize(presses)
	l := 0
	c := byte('A')
	for i := 0; i < len(code); i++ {
		l += presses(c, code[i], 0)
		c = code[i]
	}
	return l
}

func load() ([]string, error) {
	var codes []string
	sc := bufio.NewScanner(os.Stdin)
	for sc.Scan() {
		codes = append(codes, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return codes, nil
}

func openDoors(codes []string, inner int) {
	pads := []keypadFn{numericPad}
	pads = append(pads, slices.Repeat([]keypadFn{dirPad}, inner)...)
	complexity := 0
	for _, code := range codes {
		length := keypads(code, pads)
		num := strings.Map(func(r rune) rune {
			if unicode.IsDigit(r) {
				return r
			}
			return -1
		}, code)
		ncode, _ := strconv.Atoi(num)
		//log.Printf("%d * %d\n", length, ncode)
		complexity += length * ncode
	}
	fmt.Println(complexity)
}

func part1(codes []string) {
	openDoors(codes, 2)
}

func part2(codes []string) {
	openDoors(codes, 25)
}

func main() {
	second := flag.Bool("second", false, "part 2")
	flag.Parse()
	codes, err := load()
	if err != nil {
		log.Fatalln(err)
	}
	if *second {
		part2(codes)
	} else {
		part1(codes)
	}
}
